# BannedFileEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_filename** | **str** |  | [optional] 
**m_size** | **int** |  | [optional] 
**m_ban_time_stamp** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


