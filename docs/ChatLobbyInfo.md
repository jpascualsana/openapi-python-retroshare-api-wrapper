# ChatLobbyInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobby_id** | **int** |  | [optional] 
**lobby_name** | **str** |  | [optional] 
**lobby_topic** | **str** |  | [optional] 
**participating_friends** | **list[str]** |  | [optional] 
**gxs_id** | **str** |  | [optional] 
**lobby_flags** | **int** |  | [optional] 
**gxs_ids** | [**list[ChatLobbyInfoGxsIds]**](ChatLobbyInfoGxsIds.md) |  | [optional] 
**last_activity** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


