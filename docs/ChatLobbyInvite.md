# ChatLobbyInvite

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobby_id** | **int** |  | [optional] 
**peer_id** | **str** |  | [optional] 
**lobby_name** | **str** |  | [optional] 
**lobby_topic** | **str** |  | [optional] 
**lobby_flags** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


