# openapi_client.DefaultApi

All URIs are relative to *http://127.0.0.1:9092*

Method | HTTP request | Description
------------- | ------------- | -------------
[**json_api_server_authorize_token**](DefaultApi.md#json_api_server_authorize_token) | **POST** /jsonApiServer/authorizeToken | Add new auth token to the authorized set
[**json_api_server_decode_token**](DefaultApi.md#json_api_server_decode_token) | **POST** /jsonApiServer/decodeToken | Get decoded version of the given encoded token
[**json_api_server_enconde_token**](DefaultApi.md#json_api_server_enconde_token) | **POST** /jsonApiServer/encondeToken | Get encoded version of the given decoded token
[**json_api_server_get_authorized_tokens**](DefaultApi.md#json_api_server_get_authorized_tokens) | **POST** /jsonApiServer/getAuthorizedTokens | Get uthorized tokens
[**json_api_server_is_auth_token_valid**](DefaultApi.md#json_api_server_is_auth_token_valid) | **POST** /jsonApiServer/isAuthTokenValid | Check if given JSON API auth token is authorized
[**json_api_server_request_new_token_autorization**](DefaultApi.md#json_api_server_request_new_token_autorization) | **POST** /jsonApiServer/requestNewTokenAutorization | This function should be used by JSON API clients that aren&#39;t authenticated yet, to ask their token to be authorized, the success or failure will depend on mNewAccessRequestCallback return value, and it will likely need human user interaction in the process.
[**json_api_server_revoke_auth_token**](DefaultApi.md#json_api_server_revoke_auth_token) | **POST** /jsonApiServer/revokeAuthToken | Revoke given auth token
[**json_api_server_version**](DefaultApi.md#json_api_server_version) | **POST** /jsonApiServer/version | Write version information to given paramethers
[**rs_accounts_export_identity**](DefaultApi.md#rs_accounts_export_identity) | **POST** /rsAccounts/ExportIdentity | Export full encrypted PGP identity to file
[**rs_accounts_export_identity_to_string**](DefaultApi.md#rs_accounts_export_identity_to_string) | **POST** /rsAccounts/exportIdentityToString | Export full encrypted PGP identity to string
[**rs_accounts_get_current_account_id**](DefaultApi.md#rs_accounts_get_current_account_id) | **POST** /rsAccounts/getCurrentAccountId | Get current account id. Beware that an account may be selected without actually logging in.
[**rs_accounts_get_pgp_logins**](DefaultApi.md#rs_accounts_get_pgp_logins) | **POST** /rsAccounts/GetPGPLogins | Get available PGP identities id list
[**rs_accounts_import_identity**](DefaultApi.md#rs_accounts_import_identity) | **POST** /rsAccounts/ImportIdentity | Import full encrypted PGP identity from file
[**rs_accounts_import_identity_from_string**](DefaultApi.md#rs_accounts_import_identity_from_string) | **POST** /rsAccounts/importIdentityFromString | Import full encrypted PGP identity from string
[**rs_ban_list_enable_ip_filtering**](DefaultApi.md#rs_ban_list_enable_ip_filtering) | **POST** /rsBanList/enableIPFiltering | Enable or disable IP filtering service
[**rs_ban_list_ip_filtering_enabled**](DefaultApi.md#rs_ban_list_ip_filtering_enabled) | **POST** /rsBanList/ipFilteringEnabled | Get ip filtering service status
[**rs_broadcast_discovery_get_discovered_peers**](DefaultApi.md#rs_broadcast_discovery_get_discovered_peers) | **POST** /rsBroadcastDiscovery/getDiscoveredPeers | Get potential peers that have been discovered up until now
[**rs_config_get_all_bandwidth_rates**](DefaultApi.md#rs_config_get_all_bandwidth_rates) | **POST** /rsConfig/getAllBandwidthRates | getAllBandwidthRates get the bandwidth rates for all peers
[**rs_config_get_config_net_status**](DefaultApi.md#rs_config_get_config_net_status) | **POST** /rsConfig/getConfigNetStatus | getConfigNetStatus return the net status
[**rs_config_get_current_data_rates**](DefaultApi.md#rs_config_get_current_data_rates) | **POST** /rsConfig/GetCurrentDataRates | GetCurrentDataRates get current upload and download rates
[**rs_config_get_max_data_rates**](DefaultApi.md#rs_config_get_max_data_rates) | **POST** /rsConfig/GetMaxDataRates | GetMaxDataRates get maximum upload and download rates
[**rs_config_get_operating_mode**](DefaultApi.md#rs_config_get_operating_mode) | **POST** /rsConfig/getOperatingMode | getOperatingMode get current operating mode
[**rs_config_get_total_bandwidth_rates**](DefaultApi.md#rs_config_get_total_bandwidth_rates) | **POST** /rsConfig/getTotalBandwidthRates | getTotalBandwidthRates returns the current bandwidths rates
[**rs_config_get_traffic_info**](DefaultApi.md#rs_config_get_traffic_info) | **POST** /rsConfig/getTrafficInfo | getTrafficInfo returns a list of all tracked traffic clues
[**rs_config_set_max_data_rates**](DefaultApi.md#rs_config_set_max_data_rates) | **POST** /rsConfig/SetMaxDataRates | SetMaxDataRates set maximum upload and download rates
[**rs_config_set_operating_mode**](DefaultApi.md#rs_config_set_operating_mode) | **POST** /rsConfig/setOperatingMode | setOperatingMode set the current oprating mode
[**rs_control_is_ready**](DefaultApi.md#rs_control_is_ready) | **POST** /rsControl/isReady | Check if core is fully ready, true only after
[**rs_control_rs_global_shut_down**](DefaultApi.md#rs_control_rs_global_shut_down) | **POST** /rsControl/rsGlobalShutDown | Turn off RetroShare
[**rs_events_register_events_handler**](DefaultApi.md#rs_events_register_events_handler) | **POST** /rsEvents/registerEventsHandler | This method is asynchronous. Register events handler Every time an event is dispatced the registered events handlers will get their method handleEvent called with the event passed as paramether.
[**rs_files_add_shared_directory**](DefaultApi.md#rs_files_add_shared_directory) | **POST** /rsFiles/addSharedDirectory | Add shared directory
[**rs_files_already_have_file**](DefaultApi.md#rs_files_already_have_file) | **POST** /rsFiles/alreadyHaveFile | Check if we already have a file
[**rs_files_ban_file**](DefaultApi.md#rs_files_ban_file) | **POST** /rsFiles/banFile | Ban unwanted file from being, searched and forwarded by this node
[**rs_files_default_chunk_strategy**](DefaultApi.md#rs_files_default_chunk_strategy) | **POST** /rsFiles/defaultChunkStrategy | Get default chunk strategy
[**rs_files_extra_file_hash**](DefaultApi.md#rs_files_extra_file_hash) | **POST** /rsFiles/ExtraFileHash | Add file to extra shared file list
[**rs_files_extra_file_remove**](DefaultApi.md#rs_files_extra_file_remove) | **POST** /rsFiles/ExtraFileRemove | Remove file from extra fila shared list
[**rs_files_extra_file_status**](DefaultApi.md#rs_files_extra_file_status) | **POST** /rsFiles/ExtraFileStatus | Get extra file information
[**rs_files_file_cancel**](DefaultApi.md#rs_files_file_cancel) | **POST** /rsFiles/FileCancel | Cancel file downloading
[**rs_files_file_clear_completed**](DefaultApi.md#rs_files_file_clear_completed) | **POST** /rsFiles/FileClearCompleted | Clear completed downloaded files list
[**rs_files_file_control**](DefaultApi.md#rs_files_file_control) | **POST** /rsFiles/FileControl | Controls file transfer
[**rs_files_file_details**](DefaultApi.md#rs_files_file_details) | **POST** /rsFiles/FileDetails | Get file details
[**rs_files_file_download_chunks_details**](DefaultApi.md#rs_files_file_download_chunks_details) | **POST** /rsFiles/FileDownloadChunksDetails | Get chunk details about the downloaded file with given hash.
[**rs_files_file_downloads**](DefaultApi.md#rs_files_file_downloads) | **POST** /rsFiles/FileDownloads | Get incoming files list
[**rs_files_file_request**](DefaultApi.md#rs_files_file_request) | **POST** /rsFiles/FileRequest | Initiate downloading of a file
[**rs_files_file_upload_chunks_details**](DefaultApi.md#rs_files_file_upload_chunks_details) | **POST** /rsFiles/FileUploadChunksDetails | Get details about the upload with given hash
[**rs_files_file_uploads**](DefaultApi.md#rs_files_file_uploads) | **POST** /rsFiles/FileUploads | Get outgoing files list
[**rs_files_force_directory_check**](DefaultApi.md#rs_files_force_directory_check) | **POST** /rsFiles/ForceDirectoryCheck | Force shared directories check
[**rs_files_free_disk_space_limit**](DefaultApi.md#rs_files_free_disk_space_limit) | **POST** /rsFiles/freeDiskSpaceLimit | Get free disk space limit
[**rs_files_get_download_directory**](DefaultApi.md#rs_files_get_download_directory) | **POST** /rsFiles/getDownloadDirectory | Get default complete downloads directory
[**rs_files_get_file_data**](DefaultApi.md#rs_files_get_file_data) | **POST** /rsFiles/getFileData | Provides file data for the gui, media streaming or rpc clients. It may return unverified chunks. This allows streaming without having to wait for hashes or completion of the file. This function returns an unspecified amount of bytes. Either as much data as available or a sensible maximum. Expect a block size of around 1MiB. To get more data, call this function repeatedly with different offsets.
[**rs_files_get_partials_directory**](DefaultApi.md#rs_files_get_partials_directory) | **POST** /rsFiles/getPartialsDirectory | Get partial downloads directory
[**rs_files_get_primary_banned_files_list**](DefaultApi.md#rs_files_get_primary_banned_files_list) | **POST** /rsFiles/getPrimaryBannedFilesList | Get list of banned files
[**rs_files_get_shared_directories**](DefaultApi.md#rs_files_get_shared_directories) | **POST** /rsFiles/getSharedDirectories | Get list of current shared directories
[**rs_files_is_hash_banned**](DefaultApi.md#rs_files_is_hash_banned) | **POST** /rsFiles/isHashBanned | Check if a file is on banned list
[**rs_files_remove_shared_directory**](DefaultApi.md#rs_files_remove_shared_directory) | **POST** /rsFiles/removeSharedDirectory | Remove directory from shared list
[**rs_files_request_dir_details**](DefaultApi.md#rs_files_request_dir_details) | **POST** /rsFiles/requestDirDetails | Request directory details, subsequent multiple call may be used to explore a whole directory tree.
[**rs_files_set_chunk_strategy**](DefaultApi.md#rs_files_set_chunk_strategy) | **POST** /rsFiles/setChunkStrategy | Set chunk strategy for file, useful to set streaming mode to be able of see video or other media preview while it is still downloading
[**rs_files_set_default_chunk_strategy**](DefaultApi.md#rs_files_set_default_chunk_strategy) | **POST** /rsFiles/setDefaultChunkStrategy | Set default chunk strategy
[**rs_files_set_destination_directory**](DefaultApi.md#rs_files_set_destination_directory) | **POST** /rsFiles/setDestinationDirectory | Set destination directory for given file
[**rs_files_set_destination_name**](DefaultApi.md#rs_files_set_destination_name) | **POST** /rsFiles/setDestinationName | Set name for dowloaded file
[**rs_files_set_download_directory**](DefaultApi.md#rs_files_set_download_directory) | **POST** /rsFiles/setDownloadDirectory | Set default complete downloads directory
[**rs_files_set_free_disk_space_limit**](DefaultApi.md#rs_files_set_free_disk_space_limit) | **POST** /rsFiles/setFreeDiskSpaceLimit | Set minimum free disk space limit
[**rs_files_set_partials_directory**](DefaultApi.md#rs_files_set_partials_directory) | **POST** /rsFiles/setPartialsDirectory | Set partial downloads directory
[**rs_files_set_shared_directories**](DefaultApi.md#rs_files_set_shared_directories) | **POST** /rsFiles/setSharedDirectories | Set shared directories
[**rs_files_turtle_search_request**](DefaultApi.md#rs_files_turtle_search_request) | **POST** /rsFiles/turtleSearchRequest | This method is asynchronous. Request remote files search
[**rs_files_unban_file**](DefaultApi.md#rs_files_unban_file) | **POST** /rsFiles/unbanFile | Remove file from unwanted list
[**rs_files_update_share_flags**](DefaultApi.md#rs_files_update_share_flags) | **POST** /rsFiles/updateShareFlags | Updates shared directory sharing flags. The directory should be already shared!
[**rs_gossip_discovery_get_disc_friends**](DefaultApi.md#rs_gossip_discovery_get_disc_friends) | **POST** /rsGossipDiscovery/getDiscFriends | getDiscFriends get a list of all friends of a given friend
[**rs_gossip_discovery_get_disc_pgp_friends**](DefaultApi.md#rs_gossip_discovery_get_disc_pgp_friends) | **POST** /rsGossipDiscovery/getDiscPgpFriends | getDiscPgpFriends get a list of all friends of a given friend
[**rs_gossip_discovery_get_peer_version**](DefaultApi.md#rs_gossip_discovery_get_peer_version) | **POST** /rsGossipDiscovery/getPeerVersion | getPeerVersion get the version string of a peer.
[**rs_gossip_discovery_get_waiting_disc_count**](DefaultApi.md#rs_gossip_discovery_get_waiting_disc_count) | **POST** /rsGossipDiscovery/getWaitingDiscCount | getWaitingDiscCount get the number of queued discovery packets.
[**rs_gossip_discovery_request_invite**](DefaultApi.md#rs_gossip_discovery_request_invite) | **POST** /rsGossipDiscovery/requestInvite | Request RetroShare certificate to given peer
[**rs_gossip_discovery_send_invite**](DefaultApi.md#rs_gossip_discovery_send_invite) | **POST** /rsGossipDiscovery/sendInvite | Send RetroShare invite to given peer
[**rs_gxs_channels_create_channel**](DefaultApi.md#rs_gxs_channels_create_channel) | **POST** /rsGxsChannels/createChannel | Deprecated{ substituted by createChannelV2 }
[**rs_gxs_channels_create_channel_v2**](DefaultApi.md#rs_gxs_channels_create_channel_v2) | **POST** /rsGxsChannels/createChannelV2 | Create channel. Blocking API.
[**rs_gxs_channels_create_comment**](DefaultApi.md#rs_gxs_channels_create_comment) | **POST** /rsGxsChannels/createComment | Deprecated
[**rs_gxs_channels_create_comment_v2**](DefaultApi.md#rs_gxs_channels_create_comment_v2) | **POST** /rsGxsChannels/createCommentV2 | Add a comment on a post or on another comment. Blocking API.
[**rs_gxs_channels_create_post**](DefaultApi.md#rs_gxs_channels_create_post) | **POST** /rsGxsChannels/createPost | Deprecated
[**rs_gxs_channels_create_post_v2**](DefaultApi.md#rs_gxs_channels_create_post_v2) | **POST** /rsGxsChannels/createPostV2 | Create channel post. Blocking API.
[**rs_gxs_channels_create_vote**](DefaultApi.md#rs_gxs_channels_create_vote) | **POST** /rsGxsChannels/createVote | Deprecated
[**rs_gxs_channels_create_vote_v2**](DefaultApi.md#rs_gxs_channels_create_vote_v2) | **POST** /rsGxsChannels/createVoteV2 | Create a vote
[**rs_gxs_channels_edit_channel**](DefaultApi.md#rs_gxs_channels_edit_channel) | **POST** /rsGxsChannels/editChannel | Edit channel details.
[**rs_gxs_channels_extra_file_hash**](DefaultApi.md#rs_gxs_channels_extra_file_hash) | **POST** /rsGxsChannels/ExtraFileHash | Share extra file Can be used to share extra file attached to a channel post
[**rs_gxs_channels_extra_file_remove**](DefaultApi.md#rs_gxs_channels_extra_file_remove) | **POST** /rsGxsChannels/ExtraFileRemove | Remove extra file from shared files
[**rs_gxs_channels_get_channel_auto_download**](DefaultApi.md#rs_gxs_channels_get_channel_auto_download) | **POST** /rsGxsChannels/getChannelAutoDownload | Get auto-download option value for given channel
[**rs_gxs_channels_get_channel_content**](DefaultApi.md#rs_gxs_channels_get_channel_content) | **POST** /rsGxsChannels/getChannelContent | Get channel contents
[**rs_gxs_channels_get_channel_download_directory**](DefaultApi.md#rs_gxs_channels_get_channel_download_directory) | **POST** /rsGxsChannels/getChannelDownloadDirectory | Get download directory for the given channel
[**rs_gxs_channels_get_channels_info**](DefaultApi.md#rs_gxs_channels_get_channels_info) | **POST** /rsGxsChannels/getChannelsInfo | Get channels information (description, thumbnail...). Blocking API.
[**rs_gxs_channels_get_channels_summaries**](DefaultApi.md#rs_gxs_channels_get_channels_summaries) | **POST** /rsGxsChannels/getChannelsSummaries | Get channels summaries list. Blocking API.
[**rs_gxs_channels_get_content_summaries**](DefaultApi.md#rs_gxs_channels_get_content_summaries) | **POST** /rsGxsChannels/getContentSummaries | Get channel content summaries
[**rs_gxs_channels_local_search_request**](DefaultApi.md#rs_gxs_channels_local_search_request) | **POST** /rsGxsChannels/localSearchRequest | This method is asynchronous. Search local channels
[**rs_gxs_channels_mark_read**](DefaultApi.md#rs_gxs_channels_mark_read) | **POST** /rsGxsChannels/markRead | Toggle post read status. Blocking API.
[**rs_gxs_channels_request_status**](DefaultApi.md#rs_gxs_channels_request_status) | **POST** /rsGxsChannels/requestStatus | null
[**rs_gxs_channels_set_channel_auto_download**](DefaultApi.md#rs_gxs_channels_set_channel_auto_download) | **POST** /rsGxsChannels/setChannelAutoDownload | Enable or disable auto-download for given channel. Blocking API
[**rs_gxs_channels_set_channel_download_directory**](DefaultApi.md#rs_gxs_channels_set_channel_download_directory) | **POST** /rsGxsChannels/setChannelDownloadDirectory | Set download directory for the given channel. Blocking API.
[**rs_gxs_channels_share_channel_keys**](DefaultApi.md#rs_gxs_channels_share_channel_keys) | **POST** /rsGxsChannels/shareChannelKeys | Share channel publishing key This can be used to authorize other peers to post on the channel
[**rs_gxs_channels_subscribe_to_channel**](DefaultApi.md#rs_gxs_channels_subscribe_to_channel) | **POST** /rsGxsChannels/subscribeToChannel | Subscrbe to a channel. Blocking API
[**rs_gxs_channels_turtle_channel_request**](DefaultApi.md#rs_gxs_channels_turtle_channel_request) | **POST** /rsGxsChannels/turtleChannelRequest | This method is asynchronous. Request remote channel
[**rs_gxs_channels_turtle_search_request**](DefaultApi.md#rs_gxs_channels_turtle_search_request) | **POST** /rsGxsChannels/turtleSearchRequest | This method is asynchronous. Request remote channels search
[**rs_gxs_circles_cancel_circle_membership**](DefaultApi.md#rs_gxs_circles_cancel_circle_membership) | **POST** /rsGxsCircles/cancelCircleMembership | Leave given circle
[**rs_gxs_circles_create_circle**](DefaultApi.md#rs_gxs_circles_create_circle) | **POST** /rsGxsCircles/createCircle | Create new circle
[**rs_gxs_circles_edit_circle**](DefaultApi.md#rs_gxs_circles_edit_circle) | **POST** /rsGxsCircles/editCircle | Edit own existing circle
[**rs_gxs_circles_get_circle_details**](DefaultApi.md#rs_gxs_circles_get_circle_details) | **POST** /rsGxsCircles/getCircleDetails | Get circle details. Memory cached
[**rs_gxs_circles_get_circle_external_id_list**](DefaultApi.md#rs_gxs_circles_get_circle_external_id_list) | **POST** /rsGxsCircles/getCircleExternalIdList | Get list of known external circles ids. Memory cached
[**rs_gxs_circles_get_circle_requests**](DefaultApi.md#rs_gxs_circles_get_circle_requests) | **POST** /rsGxsCircles/getCircleRequests | Get circle requests
[**rs_gxs_circles_get_circles_info**](DefaultApi.md#rs_gxs_circles_get_circles_info) | **POST** /rsGxsCircles/getCirclesInfo | Get circles information
[**rs_gxs_circles_get_circles_summaries**](DefaultApi.md#rs_gxs_circles_get_circles_summaries) | **POST** /rsGxsCircles/getCirclesSummaries | Get circles summaries list.
[**rs_gxs_circles_invite_ids_to_circle**](DefaultApi.md#rs_gxs_circles_invite_ids_to_circle) | **POST** /rsGxsCircles/inviteIdsToCircle | Invite identities to circle
[**rs_gxs_circles_request_circle_membership**](DefaultApi.md#rs_gxs_circles_request_circle_membership) | **POST** /rsGxsCircles/requestCircleMembership | Request circle membership, or accept circle invitation
[**rs_gxs_circles_request_status**](DefaultApi.md#rs_gxs_circles_request_status) | **POST** /rsGxsCircles/requestStatus | null
[**rs_gxs_forums_create_forum**](DefaultApi.md#rs_gxs_forums_create_forum) | **POST** /rsGxsForums/createForum | Deprecated
[**rs_gxs_forums_create_forum_v2**](DefaultApi.md#rs_gxs_forums_create_forum_v2) | **POST** /rsGxsForums/createForumV2 | Create forum.
[**rs_gxs_forums_create_message**](DefaultApi.md#rs_gxs_forums_create_message) | **POST** /rsGxsForums/createMessage | Deprecated
[**rs_gxs_forums_create_post**](DefaultApi.md#rs_gxs_forums_create_post) | **POST** /rsGxsForums/createPost | Create a post on the given forum.
[**rs_gxs_forums_edit_forum**](DefaultApi.md#rs_gxs_forums_edit_forum) | **POST** /rsGxsForums/editForum | Edit forum details.
[**rs_gxs_forums_get_forum_content**](DefaultApi.md#rs_gxs_forums_get_forum_content) | **POST** /rsGxsForums/getForumContent | Get specific list of messages from a single forums. Blocking API
[**rs_gxs_forums_get_forum_msg_meta_data**](DefaultApi.md#rs_gxs_forums_get_forum_msg_meta_data) | **POST** /rsGxsForums/getForumMsgMetaData | Get message metadatas for a specific forum. Blocking API
[**rs_gxs_forums_get_forums_info**](DefaultApi.md#rs_gxs_forums_get_forums_info) | **POST** /rsGxsForums/getForumsInfo | Get forums information (description, thumbnail...). Blocking API.
[**rs_gxs_forums_get_forums_summaries**](DefaultApi.md#rs_gxs_forums_get_forums_summaries) | **POST** /rsGxsForums/getForumsSummaries | Get forums summaries list. Blocking API.
[**rs_gxs_forums_mark_read**](DefaultApi.md#rs_gxs_forums_mark_read) | **POST** /rsGxsForums/markRead | Toggle message read status. Blocking API.
[**rs_gxs_forums_request_status**](DefaultApi.md#rs_gxs_forums_request_status) | **POST** /rsGxsForums/requestStatus | null
[**rs_gxs_forums_subscribe_to_forum**](DefaultApi.md#rs_gxs_forums_subscribe_to_forum) | **POST** /rsGxsForums/subscribeToForum | Subscrbe to a forum. Blocking API
[**rs_identity_auto_add_friend_ids_as_contact**](DefaultApi.md#rs_identity_auto_add_friend_ids_as_contact) | **POST** /rsIdentity/autoAddFriendIdsAsContact | Check if automatic signed by friend identity contact flagging is enabled
[**rs_identity_create_identity**](DefaultApi.md#rs_identity_create_identity) | **POST** /rsIdentity/createIdentity | Create a new identity
[**rs_identity_delete_banned_nodes_threshold**](DefaultApi.md#rs_identity_delete_banned_nodes_threshold) | **POST** /rsIdentity/deleteBannedNodesThreshold | Get number of days after which delete a banned identities
[**rs_identity_delete_identity**](DefaultApi.md#rs_identity_delete_identity) | **POST** /rsIdentity/deleteIdentity | Locally delete given identity
[**rs_identity_get_id_details**](DefaultApi.md#rs_identity_get_id_details) | **POST** /rsIdentity/getIdDetails | Get identity details, from the cache
[**rs_identity_get_identities_info**](DefaultApi.md#rs_identity_get_identities_info) | **POST** /rsIdentity/getIdentitiesInfo | Get identities information (name, avatar...). Blocking API.
[**rs_identity_get_identities_summaries**](DefaultApi.md#rs_identity_get_identities_summaries) | **POST** /rsIdentity/getIdentitiesSummaries | Get identities summaries list.
[**rs_identity_get_last_usage_ts**](DefaultApi.md#rs_identity_get_last_usage_ts) | **POST** /rsIdentity/getLastUsageTS | Get last seen usage time of given identity
[**rs_identity_get_own_pseudonimous_ids**](DefaultApi.md#rs_identity_get_own_pseudonimous_ids) | **POST** /rsIdentity/getOwnPseudonimousIds | Get own pseudonimous (unsigned) ids
[**rs_identity_get_own_signed_ids**](DefaultApi.md#rs_identity_get_own_signed_ids) | **POST** /rsIdentity/getOwnSignedIds | Get own signed ids
[**rs_identity_identity_from_base64**](DefaultApi.md#rs_identity_identity_from_base64) | **POST** /rsIdentity/identityFromBase64 | Import identity from base64 representation
[**rs_identity_identity_to_base64**](DefaultApi.md#rs_identity_identity_to_base64) | **POST** /rsIdentity/identityToBase64 | Get base64 representation of an identity
[**rs_identity_is_a_regular_contact**](DefaultApi.md#rs_identity_is_a_regular_contact) | **POST** /rsIdentity/isARegularContact | Check if an identity is contact
[**rs_identity_is_own_id**](DefaultApi.md#rs_identity_is_own_id) | **POST** /rsIdentity/isOwnId | Check if an id is own
[**rs_identity_request_identity**](DefaultApi.md#rs_identity_request_identity) | **POST** /rsIdentity/requestIdentity | request details of a not yet known identity to the network
[**rs_identity_request_status**](DefaultApi.md#rs_identity_request_status) | **POST** /rsIdentity/requestStatus | null
[**rs_identity_set_as_regular_contact**](DefaultApi.md#rs_identity_set_as_regular_contact) | **POST** /rsIdentity/setAsRegularContact | Set/unset identity as contact
[**rs_identity_set_auto_add_friend_ids_as_contact**](DefaultApi.md#rs_identity_set_auto_add_friend_ids_as_contact) | **POST** /rsIdentity/setAutoAddFriendIdsAsContact | Toggle automatic flagging signed by friends identity as contact
[**rs_identity_set_delete_banned_nodes_threshold**](DefaultApi.md#rs_identity_set_delete_banned_nodes_threshold) | **POST** /rsIdentity/setDeleteBannedNodesThreshold | Set number of days after which delete a banned identities
[**rs_identity_update_identity**](DefaultApi.md#rs_identity_update_identity) | **POST** /rsIdentity/updateIdentity | Update identity data (name, avatar...)
[**rs_login_helper_attempt_login**](DefaultApi.md#rs_login_helper_attempt_login) | **POST** /rsLoginHelper/attemptLogin | Normal way to attempt login
[**rs_login_helper_collect_entropy**](DefaultApi.md#rs_login_helper_collect_entropy) | **POST** /rsLoginHelper/collectEntropy | Feed extra entropy to the crypto libraries
[**rs_login_helper_create_location**](DefaultApi.md#rs_login_helper_create_location) | **POST** /rsLoginHelper/createLocation | Creates a new RetroShare location, and log in once is created
[**rs_login_helper_get_locations**](DefaultApi.md#rs_login_helper_get_locations) | **POST** /rsLoginHelper/getLocations | Get locations and associated information
[**rs_login_helper_is_logged_in**](DefaultApi.md#rs_login_helper_is_logged_in) | **POST** /rsLoginHelper/isLoggedIn | Check if RetroShare is already logged in, this usually return true after a successfull
[**rs_msgs_accept_lobby_invite**](DefaultApi.md#rs_msgs_accept_lobby_invite) | **POST** /rsMsgs/acceptLobbyInvite | acceptLobbyInvite accept a chat invite
[**rs_msgs_clear_chat_lobby**](DefaultApi.md#rs_msgs_clear_chat_lobby) | **POST** /rsMsgs/clearChatLobby | clearChatLobby clear a chat lobby
[**rs_msgs_create_chat_lobby**](DefaultApi.md#rs_msgs_create_chat_lobby) | **POST** /rsMsgs/createChatLobby | createChatLobby create a new chat lobby
[**rs_msgs_deny_lobby_invite**](DefaultApi.md#rs_msgs_deny_lobby_invite) | **POST** /rsMsgs/denyLobbyInvite | denyLobbyInvite deny a chat lobby invite
[**rs_msgs_get_chat_lobby_info**](DefaultApi.md#rs_msgs_get_chat_lobby_info) | **POST** /rsMsgs/getChatLobbyInfo | getChatLobbyInfo get lobby info of a subscribed chat lobby. Returns true if lobby id is valid.
[**rs_msgs_get_chat_lobby_list**](DefaultApi.md#rs_msgs_get_chat_lobby_list) | **POST** /rsMsgs/getChatLobbyList | getChatLobbyList get ids of subscribed lobbies
[**rs_msgs_get_custom_state_string**](DefaultApi.md#rs_msgs_get_custom_state_string) | **POST** /rsMsgs/getCustomStateString | getCustomStateString get the custom status message from a peer
[**rs_msgs_get_default_identity_for_chat_lobby**](DefaultApi.md#rs_msgs_get_default_identity_for_chat_lobby) | **POST** /rsMsgs/getDefaultIdentityForChatLobby | getDefaultIdentityForChatLobby get the default identity used for chat lobbies
[**rs_msgs_get_identity_for_chat_lobby**](DefaultApi.md#rs_msgs_get_identity_for_chat_lobby) | **POST** /rsMsgs/getIdentityForChatLobby | getIdentityForChatLobby
[**rs_msgs_get_list_of_nearby_chat_lobbies**](DefaultApi.md#rs_msgs_get_list_of_nearby_chat_lobbies) | **POST** /rsMsgs/getListOfNearbyChatLobbies | getListOfNearbyChatLobbies get info about all lobbies, subscribed and unsubscribed
[**rs_msgs_get_lobby_auto_subscribe**](DefaultApi.md#rs_msgs_get_lobby_auto_subscribe) | **POST** /rsMsgs/getLobbyAutoSubscribe | getLobbyAutoSubscribe get current value of auto subscribe
[**rs_msgs_get_max_message_security_size**](DefaultApi.md#rs_msgs_get_max_message_security_size) | **POST** /rsMsgs/getMaxMessageSecuritySize | getMaxMessageSecuritySize get the maximum size of a chta message
[**rs_msgs_get_message**](DefaultApi.md#rs_msgs_get_message) | **POST** /rsMsgs/getMessage | getMessage
[**rs_msgs_get_message_count**](DefaultApi.md#rs_msgs_get_message_count) | **POST** /rsMsgs/getMessageCount | getMessageCount
[**rs_msgs_get_message_summaries**](DefaultApi.md#rs_msgs_get_message_summaries) | **POST** /rsMsgs/getMessageSummaries | getMessageSummaries
[**rs_msgs_get_message_tag**](DefaultApi.md#rs_msgs_get_message_tag) | **POST** /rsMsgs/getMessageTag | getMessageTag
[**rs_msgs_get_message_tag_types**](DefaultApi.md#rs_msgs_get_message_tag_types) | **POST** /rsMsgs/getMessageTagTypes | getMessageTagTypes
[**rs_msgs_get_msg_parent_id**](DefaultApi.md#rs_msgs_get_msg_parent_id) | **POST** /rsMsgs/getMsgParentId | getMsgParentId
[**rs_msgs_get_pending_chat_lobby_invites**](DefaultApi.md#rs_msgs_get_pending_chat_lobby_invites) | **POST** /rsMsgs/getPendingChatLobbyInvites | getPendingChatLobbyInvites get a list of all pending chat lobby invites
[**rs_msgs_invite_peer_to_lobby**](DefaultApi.md#rs_msgs_invite_peer_to_lobby) | **POST** /rsMsgs/invitePeerToLobby | invitePeerToLobby invite a peer to join a lobby
[**rs_msgs_join_visible_chat_lobby**](DefaultApi.md#rs_msgs_join_visible_chat_lobby) | **POST** /rsMsgs/joinVisibleChatLobby | joinVisibleChatLobby join a lobby that is visible
[**rs_msgs_message_delete**](DefaultApi.md#rs_msgs_message_delete) | **POST** /rsMsgs/MessageDelete | MessageDelete
[**rs_msgs_message_forwarded**](DefaultApi.md#rs_msgs_message_forwarded) | **POST** /rsMsgs/MessageForwarded | MessageForwarded
[**rs_msgs_message_load_embedded_images**](DefaultApi.md#rs_msgs_message_load_embedded_images) | **POST** /rsMsgs/MessageLoadEmbeddedImages | MessageLoadEmbeddedImages
[**rs_msgs_message_read**](DefaultApi.md#rs_msgs_message_read) | **POST** /rsMsgs/MessageRead | MessageRead
[**rs_msgs_message_replied**](DefaultApi.md#rs_msgs_message_replied) | **POST** /rsMsgs/MessageReplied | MessageReplied
[**rs_msgs_message_send**](DefaultApi.md#rs_msgs_message_send) | **POST** /rsMsgs/MessageSend | MessageSend
[**rs_msgs_message_star**](DefaultApi.md#rs_msgs_message_star) | **POST** /rsMsgs/MessageStar | MessageStar
[**rs_msgs_message_to_draft**](DefaultApi.md#rs_msgs_message_to_draft) | **POST** /rsMsgs/MessageToDraft | MessageToDraft
[**rs_msgs_message_to_trash**](DefaultApi.md#rs_msgs_message_to_trash) | **POST** /rsMsgs/MessageToTrash | MessageToTrash
[**rs_msgs_remove_message_tag_type**](DefaultApi.md#rs_msgs_remove_message_tag_type) | **POST** /rsMsgs/removeMessageTagType | removeMessageTagType
[**rs_msgs_reset_message_standard_tag_types**](DefaultApi.md#rs_msgs_reset_message_standard_tag_types) | **POST** /rsMsgs/resetMessageStandardTagTypes | resetMessageStandardTagTypes
[**rs_msgs_send_chat**](DefaultApi.md#rs_msgs_send_chat) | **POST** /rsMsgs/sendChat | sendChat send a chat message to a given id
[**rs_msgs_send_lobby_status_peer_leaving**](DefaultApi.md#rs_msgs_send_lobby_status_peer_leaving) | **POST** /rsMsgs/sendLobbyStatusPeerLeaving | sendLobbyStatusPeerLeaving notify friend nodes that we&#39;re leaving a subscribed lobby
[**rs_msgs_send_status_string**](DefaultApi.md#rs_msgs_send_status_string) | **POST** /rsMsgs/sendStatusString | sendStatusString send a status string
[**rs_msgs_set_custom_state_string**](DefaultApi.md#rs_msgs_set_custom_state_string) | **POST** /rsMsgs/setCustomStateString | setCustomStateString set your custom status message
[**rs_msgs_set_default_identity_for_chat_lobby**](DefaultApi.md#rs_msgs_set_default_identity_for_chat_lobby) | **POST** /rsMsgs/setDefaultIdentityForChatLobby | setDefaultIdentityForChatLobby set the default identity used for chat lobbies
[**rs_msgs_set_identity_for_chat_lobby**](DefaultApi.md#rs_msgs_set_identity_for_chat_lobby) | **POST** /rsMsgs/setIdentityForChatLobby | setIdentityForChatLobby set the chat identit
[**rs_msgs_set_lobby_auto_subscribe**](DefaultApi.md#rs_msgs_set_lobby_auto_subscribe) | **POST** /rsMsgs/setLobbyAutoSubscribe | setLobbyAutoSubscribe enable or disable auto subscribe for a chat lobby
[**rs_msgs_set_message_tag**](DefaultApi.md#rs_msgs_set_message_tag) | **POST** /rsMsgs/setMessageTag | setMessageTag set &#x3D;&#x3D; false &amp;&amp; tagId &#x3D;&#x3D; 0
[**rs_msgs_set_message_tag_type**](DefaultApi.md#rs_msgs_set_message_tag_type) | **POST** /rsMsgs/setMessageTagType | setMessageTagType
[**rs_msgs_system_message**](DefaultApi.md#rs_msgs_system_message) | **POST** /rsMsgs/SystemMessage | SystemMessage
[**rs_msgs_unsubscribe_chat_lobby**](DefaultApi.md#rs_msgs_unsubscribe_chat_lobby) | **POST** /rsMsgs/unsubscribeChatLobby | unsubscribeChatLobby leave a chat lobby
[**rs_peers_accept_invite**](DefaultApi.md#rs_peers_accept_invite) | **POST** /rsPeers/acceptInvite | Add trusted node from invite
[**rs_peers_add_friend**](DefaultApi.md#rs_peers_add_friend) | **POST** /rsPeers/addFriend | Add trusted node
[**rs_peers_add_group**](DefaultApi.md#rs_peers_add_group) | **POST** /rsPeers/addGroup | addGroup create a new group
[**rs_peers_add_peer_locator**](DefaultApi.md#rs_peers_add_peer_locator) | **POST** /rsPeers/addPeerLocator | Add URL locator for given peer
[**rs_peers_assign_peer_to_group**](DefaultApi.md#rs_peers_assign_peer_to_group) | **POST** /rsPeers/assignPeerToGroup | assignPeerToGroup add a peer to a group
[**rs_peers_assign_peers_to_group**](DefaultApi.md#rs_peers_assign_peers_to_group) | **POST** /rsPeers/assignPeersToGroup | assignPeersToGroup add a list of peers to a group
[**rs_peers_connect_attempt**](DefaultApi.md#rs_peers_connect_attempt) | **POST** /rsPeers/connectAttempt | Trigger connection attempt to given node
[**rs_peers_edit_group**](DefaultApi.md#rs_peers_edit_group) | **POST** /rsPeers/editGroup | editGroup edit an existing group
[**rs_peers_get_friend_list**](DefaultApi.md#rs_peers_get_friend_list) | **POST** /rsPeers/getFriendList | Get trusted peers list
[**rs_peers_get_gpg_id**](DefaultApi.md#rs_peers_get_gpg_id) | **POST** /rsPeers/getGPGId | Get PGP id for the given peer
[**rs_peers_get_group_info**](DefaultApi.md#rs_peers_get_group_info) | **POST** /rsPeers/getGroupInfo | getGroupInfo get group information to one group
[**rs_peers_get_group_info_by_name**](DefaultApi.md#rs_peers_get_group_info_by_name) | **POST** /rsPeers/getGroupInfoByName | getGroupInfoByName get group information by group name
[**rs_peers_get_group_info_list**](DefaultApi.md#rs_peers_get_group_info_list) | **POST** /rsPeers/getGroupInfoList | getGroupInfoList get list of all groups
[**rs_peers_get_online_list**](DefaultApi.md#rs_peers_get_online_list) | **POST** /rsPeers/getOnlineList | Get connected peers list
[**rs_peers_get_peer_details**](DefaultApi.md#rs_peers_get_peer_details) | **POST** /rsPeers/getPeerDetails | Get details details of the given peer
[**rs_peers_get_peers_count**](DefaultApi.md#rs_peers_get_peers_count) | **POST** /rsPeers/getPeersCount | Get peers count
[**rs_peers_get_retroshare_invite**](DefaultApi.md#rs_peers_get_retroshare_invite) | **POST** /rsPeers/GetRetroshareInvite | Get RetroShare invite of the given peer
[**rs_peers_is_friend**](DefaultApi.md#rs_peers_is_friend) | **POST** /rsPeers/isFriend | Check if given peer is a trusted node
[**rs_peers_is_online**](DefaultApi.md#rs_peers_is_online) | **POST** /rsPeers/isOnline | Check if there is an established connection to the given peer
[**rs_peers_is_pgp_friend**](DefaultApi.md#rs_peers_is_pgp_friend) | **POST** /rsPeers/isPgpFriend | Check if given PGP id is trusted
[**rs_peers_load_certificate_from_string**](DefaultApi.md#rs_peers_load_certificate_from_string) | **POST** /rsPeers/loadCertificateFromString | Import certificate into the keyring
[**rs_peers_load_details_from_string_cert**](DefaultApi.md#rs_peers_load_details_from_string_cert) | **POST** /rsPeers/loadDetailsFromStringCert | Examine certificate and get details without importing into the keyring
[**rs_peers_remove_friend**](DefaultApi.md#rs_peers_remove_friend) | **POST** /rsPeers/removeFriend | Revoke connection trust from to node
[**rs_peers_remove_friend_location**](DefaultApi.md#rs_peers_remove_friend_location) | **POST** /rsPeers/removeFriendLocation | Remove location of a trusted node, useful to prune old unused locations of a trusted peer without revoking trust
[**rs_peers_remove_group**](DefaultApi.md#rs_peers_remove_group) | **POST** /rsPeers/removeGroup | removeGroup remove a group
[**rs_peers_set_dyn_dns**](DefaultApi.md#rs_peers_set_dyn_dns) | **POST** /rsPeers/setDynDNS | Set (dynamical) domain name associated to the given peer
[**rs_peers_set_ext_address**](DefaultApi.md#rs_peers_set_ext_address) | **POST** /rsPeers/setExtAddress | Set external IPv4 address for given peer
[**rs_peers_set_local_address**](DefaultApi.md#rs_peers_set_local_address) | **POST** /rsPeers/setLocalAddress | Set local IPv4 address for the given peer
[**rs_peers_set_network_mode**](DefaultApi.md#rs_peers_set_network_mode) | **POST** /rsPeers/setNetworkMode | Set network mode of the given peer
[**rs_peers_set_vis_state**](DefaultApi.md#rs_peers_set_vis_state) | **POST** /rsPeers/setVisState | set DHT and discovery modes
[**rs_reputations_auto_positive_opinion_for_contacts**](DefaultApi.md#rs_reputations_auto_positive_opinion_for_contacts) | **POST** /rsReputations/autoPositiveOpinionForContacts | check if giving automatic positive opinion when flagging as contact is enbaled
[**rs_reputations_ban_node**](DefaultApi.md#rs_reputations_ban_node) | **POST** /rsReputations/banNode | Enable automatic banning of all identities signed by the given node
[**rs_reputations_get_own_opinion**](DefaultApi.md#rs_reputations_get_own_opinion) | **POST** /rsReputations/getOwnOpinion | Get own opition about the given identity
[**rs_reputations_get_reputation_info**](DefaultApi.md#rs_reputations_get_reputation_info) | **POST** /rsReputations/getReputationInfo | Get reputation data of given identity
[**rs_reputations_is_identity_banned**](DefaultApi.md#rs_reputations_is_identity_banned) | **POST** /rsReputations/isIdentityBanned | This method allow fast checking if a GXS identity is banned.
[**rs_reputations_is_node_banned**](DefaultApi.md#rs_reputations_is_node_banned) | **POST** /rsReputations/isNodeBanned | Check if automatic banning of all identities signed by the given node is enabled
[**rs_reputations_overall_reputation_level**](DefaultApi.md#rs_reputations_overall_reputation_level) | **POST** /rsReputations/overallReputationLevel | Get overall reputation level of given identity
[**rs_reputations_remember_banned_id_threshold**](DefaultApi.md#rs_reputations_remember_banned_id_threshold) | **POST** /rsReputations/rememberBannedIdThreshold | Get number of days to wait before deleting a banned identity from local storage
[**rs_reputations_set_auto_positive_opinion_for_contacts**](DefaultApi.md#rs_reputations_set_auto_positive_opinion_for_contacts) | **POST** /rsReputations/setAutoPositiveOpinionForContacts | Enable giving automatic positive opinion when flagging as contact
[**rs_reputations_set_own_opinion**](DefaultApi.md#rs_reputations_set_own_opinion) | **POST** /rsReputations/setOwnOpinion | Set own opinion about the given identity
[**rs_reputations_set_remember_banned_id_threshold**](DefaultApi.md#rs_reputations_set_remember_banned_id_threshold) | **POST** /rsReputations/setRememberBannedIdThreshold | Set number of days to wait before deleting a banned identity from local storage
[**rs_reputations_set_threshold_for_remotely_negative_reputation**](DefaultApi.md#rs_reputations_set_threshold_for_remotely_negative_reputation) | **POST** /rsReputations/setThresholdForRemotelyNegativeReputation | Set threshold on remote reputation to consider it remotely negative
[**rs_reputations_set_threshold_for_remotely_positive_reputation**](DefaultApi.md#rs_reputations_set_threshold_for_remotely_positive_reputation) | **POST** /rsReputations/setThresholdForRemotelyPositiveReputation | Set threshold on remote reputation to consider it remotely positive
[**rs_reputations_threshold_for_remotely_negative_reputation**](DefaultApi.md#rs_reputations_threshold_for_remotely_negative_reputation) | **POST** /rsReputations/thresholdForRemotelyNegativeReputation | Get threshold on remote reputation to consider it remotely negative
[**rs_reputations_threshold_for_remotely_positive_reputation**](DefaultApi.md#rs_reputations_threshold_for_remotely_positive_reputation) | **POST** /rsReputations/thresholdForRemotelyPositiveReputation | Get threshold on remote reputation to consider it remotely negative
[**rs_service_control_get_own_services**](DefaultApi.md#rs_service_control_get_own_services) | **POST** /rsServiceControl/getOwnServices | get a map off all services.
[**rs_service_control_get_peers_connected**](DefaultApi.md#rs_service_control_get_peers_connected) | **POST** /rsServiceControl/getPeersConnected | getPeersConnected return peers using a service.
[**rs_service_control_get_service_item_names**](DefaultApi.md#rs_service_control_get_service_item_names) | **POST** /rsServiceControl/getServiceItemNames | getServiceItemNames return a map of service item names.
[**rs_service_control_get_service_name**](DefaultApi.md#rs_service_control_get_service_name) | **POST** /rsServiceControl/getServiceName | getServiceName lookup the name of a service.
[**rs_service_control_get_service_permissions**](DefaultApi.md#rs_service_control_get_service_permissions) | **POST** /rsServiceControl/getServicePermissions | getServicePermissions return permissions of one service.
[**rs_service_control_get_services_allowed**](DefaultApi.md#rs_service_control_get_services_allowed) | **POST** /rsServiceControl/getServicesAllowed | getServicesAllowed return a mpa with allowed service information.
[**rs_service_control_get_services_provided**](DefaultApi.md#rs_service_control_get_services_provided) | **POST** /rsServiceControl/getServicesProvided | getServicesProvided return services provided by a peer.
[**rs_service_control_update_service_permissions**](DefaultApi.md#rs_service_control_update_service_permissions) | **POST** /rsServiceControl/updateServicePermissions | updateServicePermissions update service permissions of one service.


# **json_api_server_authorize_token**
> RespJsonApiServerAuthorizeToken json_api_server_authorize_token(req_json_api_server_authorize_token=req_json_api_server_authorize_token)

Add new auth token to the authorized set

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_json_api_server_authorize_token = {"token":"( string)toke to autorize decoded "} # ReqJsonApiServerAuthorizeToken | token: \"( string)toke to autorize decoded \"  (optional)

try:
    # Add new auth token to the authorized set
    api_response = api_instance.json_api_server_authorize_token(req_json_api_server_authorize_token=req_json_api_server_authorize_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->json_api_server_authorize_token: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_json_api_server_authorize_token** | [**ReqJsonApiServerAuthorizeToken**](ReqJsonApiServerAuthorizeToken.md)| token: \&quot;( string)toke to autorize decoded \&quot;  | [optional] 

### Return type

[**RespJsonApiServerAuthorizeToken**](RespJsonApiServerAuthorizeToken.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if the token has been added to authorized, false if error occurred or if the token was already authorized   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **json_api_server_decode_token**
> RespJsonApiServerDecodeToken json_api_server_decode_token(req_json_api_server_decode_token=req_json_api_server_decode_token)

Get decoded version of the given encoded token

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()
req_json_api_server_decode_token = {"token":"( string)encoded "} # ReqJsonApiServerDecodeToken | token: \"( string)encoded \"  (optional)

try:
    # Get decoded version of the given encoded token
    api_response = api_instance.json_api_server_decode_token(req_json_api_server_decode_token=req_json_api_server_decode_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->json_api_server_decode_token: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_json_api_server_decode_token** | [**ReqJsonApiServerDecodeToken**](ReqJsonApiServerDecodeToken.md)| token: \&quot;( string)encoded \&quot;  | [optional] 

### Return type

[**RespJsonApiServerDecodeToken**](RespJsonApiServerDecodeToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: token decoded   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **json_api_server_enconde_token**
> RespJsonApiServerEncondeToken json_api_server_enconde_token(req_json_api_server_enconde_token=req_json_api_server_enconde_token)

Get encoded version of the given decoded token

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()
req_json_api_server_enconde_token = {"token":"( string)decoded "} # ReqJsonApiServerEncondeToken | token: \"( string)decoded \"  (optional)

try:
    # Get encoded version of the given decoded token
    api_response = api_instance.json_api_server_enconde_token(req_json_api_server_enconde_token=req_json_api_server_enconde_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->json_api_server_enconde_token: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_json_api_server_enconde_token** | [**ReqJsonApiServerEncondeToken**](ReqJsonApiServerEncondeToken.md)| token: \&quot;( string)decoded \&quot;  | [optional] 

### Return type

[**RespJsonApiServerEncondeToken**](RespJsonApiServerEncondeToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: token encoded   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **json_api_server_get_authorized_tokens**
> RespJsonApiServerGetAuthorizedTokens json_api_server_get_authorized_tokens()

Get uthorized tokens

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get uthorized tokens
    api_response = api_instance.json_api_server_get_authorized_tokens()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->json_api_server_get_authorized_tokens: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespJsonApiServerGetAuthorizedTokens**](RespJsonApiServerGetAuthorizedTokens.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: the set of authorized encoded tokens   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **json_api_server_is_auth_token_valid**
> RespJsonApiServerIsAuthTokenValid json_api_server_is_auth_token_valid(req_json_api_server_is_auth_token_valid=req_json_api_server_is_auth_token_valid)

Check if given JSON API auth token is authorized

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_json_api_server_is_auth_token_valid = {"token":"( string)decoded "} # ReqJsonApiServerIsAuthTokenValid | token: \"( string)decoded \"  (optional)

try:
    # Check if given JSON API auth token is authorized
    api_response = api_instance.json_api_server_is_auth_token_valid(req_json_api_server_is_auth_token_valid=req_json_api_server_is_auth_token_valid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->json_api_server_is_auth_token_valid: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_json_api_server_is_auth_token_valid** | [**ReqJsonApiServerIsAuthTokenValid**](ReqJsonApiServerIsAuthTokenValid.md)| token: \&quot;( string)decoded \&quot;  | [optional] 

### Return type

[**RespJsonApiServerIsAuthTokenValid**](RespJsonApiServerIsAuthTokenValid.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: tru if authorized, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **json_api_server_request_new_token_autorization**
> RespJsonApiServerRequestNewTokenAutorization json_api_server_request_new_token_autorization(req_json_api_server_request_new_token_autorization=req_json_api_server_request_new_token_autorization)

This function should be used by JSON API clients that aren't authenticated yet, to ask their token to be authorized, the success or failure will depend on mNewAccessRequestCallback return value, and it will likely need human user interaction in the process.

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()
req_json_api_server_request_new_token_autorization = {"token":"( string)token to autorize "} # ReqJsonApiServerRequestNewTokenAutorization | token: \"( string)token to autorize \"  (optional)

try:
    # This function should be used by JSON API clients that aren't authenticated yet, to ask their token to be authorized, the success or failure will depend on mNewAccessRequestCallback return value, and it will likely need human user interaction in the process.
    api_response = api_instance.json_api_server_request_new_token_autorization(req_json_api_server_request_new_token_autorization=req_json_api_server_request_new_token_autorization)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->json_api_server_request_new_token_autorization: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_json_api_server_request_new_token_autorization** | [**ReqJsonApiServerRequestNewTokenAutorization**](ReqJsonApiServerRequestNewTokenAutorization.md)| token: \&quot;( string)token to autorize \&quot;  | [optional] 

### Return type

[**RespJsonApiServerRequestNewTokenAutorization**](RespJsonApiServerRequestNewTokenAutorization.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if authorization succeded, false otherwise.   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **json_api_server_revoke_auth_token**
> RespJsonApiServerRevokeAuthToken json_api_server_revoke_auth_token(req_json_api_server_revoke_auth_token=req_json_api_server_revoke_auth_token)

Revoke given auth token

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_json_api_server_revoke_auth_token = {"token":"( string)decoded "} # ReqJsonApiServerRevokeAuthToken | token: \"( string)decoded \"  (optional)

try:
    # Revoke given auth token
    api_response = api_instance.json_api_server_revoke_auth_token(req_json_api_server_revoke_auth_token=req_json_api_server_revoke_auth_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->json_api_server_revoke_auth_token: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_json_api_server_revoke_auth_token** | [**ReqJsonApiServerRevokeAuthToken**](ReqJsonApiServerRevokeAuthToken.md)| token: \&quot;( string)decoded \&quot;  | [optional] 

### Return type

[**RespJsonApiServerRevokeAuthToken**](RespJsonApiServerRevokeAuthToken.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if the token has been revoked, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **json_api_server_version**
> RespJsonApiServerVersion json_api_server_version()

Write version information to given paramethers

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    # Write version information to given paramethers
    api_response = api_instance.json_api_server_version()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->json_api_server_version: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespJsonApiServerVersion**](RespJsonApiServerVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:  [out] major(integer)storage  [out] minor(integer)storage  [out] mini(integer)storage  [out] extra(string)storage  [out] human(string)storage   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_accounts_export_identity**
> RespRsAccountsExportIdentity rs_accounts_export_identity(req_rs_accounts_export_identity=req_rs_accounts_export_identity)

Export full encrypted PGP identity to file

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_accounts_export_identity = {"filePath":"( string)path of certificate file ","pgpId":"( RsPgpId)PGP id to export "} # ReqRsAccountsExportIdentity | filePath: \"( string)path of certificate file \"         pgpId: \"( RsPgpId)PGP id to export \"  (optional)

try:
    # Export full encrypted PGP identity to file
    api_response = api_instance.rs_accounts_export_identity(req_rs_accounts_export_identity=req_rs_accounts_export_identity)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_accounts_export_identity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_accounts_export_identity** | [**ReqRsAccountsExportIdentity**](ReqRsAccountsExportIdentity.md)| filePath: \&quot;( string)path of certificate file \&quot;         pgpId: \&quot;( RsPgpId)PGP id to export \&quot;  | [optional] 

### Return type

[**RespRsAccountsExportIdentity**](RespRsAccountsExportIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_accounts_export_identity_to_string**
> RespRsAccountsExportIdentityToString rs_accounts_export_identity_to_string(req_rs_accounts_export_identity_to_string=req_rs_accounts_export_identity_to_string)

Export full encrypted PGP identity to string

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_accounts_export_identity_to_string = {"pgpId":"( RsPgpId)PGP id to export ","includeSignatures":"( boolean)true to include signatures "} # ReqRsAccountsExportIdentityToString | pgpId: \"( RsPgpId)PGP id to export \"         includeSignatures: \"( boolean)true to include signatures \"  (optional)

try:
    # Export full encrypted PGP identity to string
    api_response = api_instance.rs_accounts_export_identity_to_string(req_rs_accounts_export_identity_to_string=req_rs_accounts_export_identity_to_string)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_accounts_export_identity_to_string: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_accounts_export_identity_to_string** | [**ReqRsAccountsExportIdentityToString**](ReqRsAccountsExportIdentityToString.md)| pgpId: \&quot;( RsPgpId)PGP id to export \&quot;         includeSignatures: \&quot;( boolean)true to include signatures \&quot;  | [optional] 

### Return type

[**RespRsAccountsExportIdentityToString**](RespRsAccountsExportIdentityToString.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success, false otherwise  [out] data(string)storage for certificate string  [out] errorMsg(string)storage for eventual human readable error message   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_accounts_get_current_account_id**
> RespRsAccountsGetCurrentAccountId rs_accounts_get_current_account_id()

Get current account id. Beware that an account may be selected without actually logging in.

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    # Get current account id. Beware that an account may be selected without actually logging in.
    api_response = api_instance.rs_accounts_get_current_account_id()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_accounts_get_current_account_id: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsAccountsGetCurrentAccountId**](RespRsAccountsGetCurrentAccountId.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if account hasn&#39;t been selected yet, true otherwise  [out] id(RsPeerId)storage for current account id   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_accounts_get_pgp_logins**
> RespRsAccountsGetPGPLogins rs_accounts_get_pgp_logins()

Get available PGP identities id list

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    # Get available PGP identities id list
    api_response = api_instance.rs_accounts_get_pgp_logins()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_accounts_get_pgp_logins: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsAccountsGetPGPLogins**](RespRsAccountsGetPGPLogins.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success, false otherwise  [out] pgpIds(list&lt;RsPgpId&gt;)storage for PGP id list   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_accounts_import_identity**
> RespRsAccountsImportIdentity rs_accounts_import_identity(req_rs_accounts_import_identity=req_rs_accounts_import_identity)

Import full encrypted PGP identity from file

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()
req_rs_accounts_import_identity = {"filePath":"( string)path of certificate file "} # ReqRsAccountsImportIdentity | filePath: \"( string)path of certificate file \"  (optional)

try:
    # Import full encrypted PGP identity from file
    api_response = api_instance.rs_accounts_import_identity(req_rs_accounts_import_identity=req_rs_accounts_import_identity)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_accounts_import_identity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_accounts_import_identity** | [**ReqRsAccountsImportIdentity**](ReqRsAccountsImportIdentity.md)| filePath: \&quot;( string)path of certificate file \&quot;  | [optional] 

### Return type

[**RespRsAccountsImportIdentity**](RespRsAccountsImportIdentity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success, false otherwise  [out] pgpId(RsPgpId)storage for the PGP fingerprint of the imported key  [out] errorMsg(string)storage for eventual human readable error message   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_accounts_import_identity_from_string**
> RespRsAccountsImportIdentityFromString rs_accounts_import_identity_from_string(req_rs_accounts_import_identity_from_string=req_rs_accounts_import_identity_from_string)

Import full encrypted PGP identity from string

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()
req_rs_accounts_import_identity_from_string = {"data":"( string)certificate string "} # ReqRsAccountsImportIdentityFromString | data: \"( string)certificate string \"  (optional)

try:
    # Import full encrypted PGP identity from string
    api_response = api_instance.rs_accounts_import_identity_from_string(req_rs_accounts_import_identity_from_string=req_rs_accounts_import_identity_from_string)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_accounts_import_identity_from_string: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_accounts_import_identity_from_string** | [**ReqRsAccountsImportIdentityFromString**](ReqRsAccountsImportIdentityFromString.md)| data: \&quot;( string)certificate string \&quot;  | [optional] 

### Return type

[**RespRsAccountsImportIdentityFromString**](RespRsAccountsImportIdentityFromString.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success, false otherwise  [out] pgpId(RsPgpId)storage for the PGP fingerprint of the imported key  [out] errorMsg(string)storage for eventual human readable error message   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_ban_list_enable_ip_filtering**
> object rs_ban_list_enable_ip_filtering(req_rs_ban_list_enable_ip_filtering=req_rs_ban_list_enable_ip_filtering)

Enable or disable IP filtering service

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_ban_list_enable_ip_filtering = {"enable":"( boolean)pass true to enable, false to disable "} # ReqRsBanListEnableIPFiltering | enable: \"( boolean)pass true to enable, false to disable \"  (optional)

try:
    # Enable or disable IP filtering service
    api_response = api_instance.rs_ban_list_enable_ip_filtering(req_rs_ban_list_enable_ip_filtering=req_rs_ban_list_enable_ip_filtering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_ban_list_enable_ip_filtering: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_ban_list_enable_ip_filtering** | [**ReqRsBanListEnableIPFiltering**](ReqRsBanListEnableIPFiltering.md)| enable: \&quot;( boolean)pass true to enable, false to disable \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_ban_list_ip_filtering_enabled**
> RespRsBanListIpFilteringEnabled rs_ban_list_ip_filtering_enabled()

Get ip filtering service status

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get ip filtering service status
    api_response = api_instance.rs_ban_list_ip_filtering_enabled()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_ban_list_ip_filtering_enabled: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsBanListIpFilteringEnabled**](RespRsBanListIpFilteringEnabled.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if enabled, false if disabled   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_broadcast_discovery_get_discovered_peers**
> RespRsBroadcastDiscoveryGetDiscoveredPeers rs_broadcast_discovery_get_discovered_peers()

Get potential peers that have been discovered up until now

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get potential peers that have been discovered up until now
    api_response = api_instance.rs_broadcast_discovery_get_discovered_peers()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_broadcast_discovery_get_discovered_peers: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsBroadcastDiscoveryGetDiscoveredPeers**](RespRsBroadcastDiscoveryGetDiscoveredPeers.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: vector containing discovered peers, may be empty.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_config_get_all_bandwidth_rates**
> RespRsConfigGetAllBandwidthRates rs_config_get_all_bandwidth_rates()

getAllBandwidthRates get the bandwidth rates for all peers

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getAllBandwidthRates get the bandwidth rates for all peers
    api_response = api_instance.rs_config_get_all_bandwidth_rates()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_config_get_all_bandwidth_rates: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetAllBandwidthRates**](RespRsConfigGetAllBandwidthRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: returns 1 on succes and 0 otherwise  [out] ratemap(map&lt;RsPeerId,RsConfigDataRates&gt;)map with peers-&gt;rates   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_config_get_config_net_status**
> RespRsConfigGetConfigNetStatus rs_config_get_config_net_status()

getConfigNetStatus return the net status

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getConfigNetStatus return the net status
    api_response = api_instance.rs_config_get_config_net_status()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_config_get_config_net_status: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetConfigNetStatus**](RespRsConfigGetConfigNetStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: returns 1 on succes and 0 otherwise  [out] status(RsConfigNetStatus)network status   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_config_get_current_data_rates**
> RespRsConfigGetCurrentDataRates rs_config_get_current_data_rates()

GetCurrentDataRates get current upload and download rates

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # GetCurrentDataRates get current upload and download rates
    api_response = api_instance.rs_config_get_current_data_rates()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_config_get_current_data_rates: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetCurrentDataRates**](RespRsConfigGetCurrentDataRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: returns 1 on succes and 0 otherwise  [out] inKb(number)download rate in kB  [out] outKb(number)upload rate in kB   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_config_get_max_data_rates**
> RespRsConfigGetMaxDataRates rs_config_get_max_data_rates()

GetMaxDataRates get maximum upload and download rates

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # GetMaxDataRates get maximum upload and download rates
    api_response = api_instance.rs_config_get_max_data_rates()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_config_get_max_data_rates: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetMaxDataRates**](RespRsConfigGetMaxDataRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: returns 1 on succes and 0 otherwise  [out] inKb(integer)download rate in kB  [out] outKb(integer)upload rate in kB   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_config_get_operating_mode**
> RespRsConfigGetOperatingMode rs_config_get_operating_mode()

getOperatingMode get current operating mode

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getOperatingMode get current operating mode
    api_response = api_instance.rs_config_get_operating_mode()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_config_get_operating_mode: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetOperatingMode**](RespRsConfigGetOperatingMode.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: return the current operating mode   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_config_get_total_bandwidth_rates**
> RespRsConfigGetTotalBandwidthRates rs_config_get_total_bandwidth_rates()

getTotalBandwidthRates returns the current bandwidths rates

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getTotalBandwidthRates returns the current bandwidths rates
    api_response = api_instance.rs_config_get_total_bandwidth_rates()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_config_get_total_bandwidth_rates: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetTotalBandwidthRates**](RespRsConfigGetTotalBandwidthRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: returns 1 on succes and 0 otherwise  [out] rates(RsConfigDataRates)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_config_get_traffic_info**
> RespRsConfigGetTrafficInfo rs_config_get_traffic_info()

getTrafficInfo returns a list of all tracked traffic clues

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getTrafficInfo returns a list of all tracked traffic clues
    api_response = api_instance.rs_config_get_traffic_info()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_config_get_traffic_info: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetTrafficInfo**](RespRsConfigGetTrafficInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: returns 1 on succes and 0 otherwise  [out] out_lst(list&lt;RSTrafficClue&gt;)outgoing traffic clues  [out] in_lst(list&lt;RSTrafficClue&gt;)incomming traffic clues   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_config_set_max_data_rates**
> RespRsConfigSetMaxDataRates rs_config_set_max_data_rates(req_rs_config_set_max_data_rates=req_rs_config_set_max_data_rates)

SetMaxDataRates set maximum upload and download rates

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_config_set_max_data_rates = {"downKb":"( integer)download rate in kB ","upKb":"( integer)upload rate in kB "} # ReqRsConfigSetMaxDataRates | downKb: \"( integer)download rate in kB \"         upKb: \"( integer)upload rate in kB \"  (optional)

try:
    # SetMaxDataRates set maximum upload and download rates
    api_response = api_instance.rs_config_set_max_data_rates(req_rs_config_set_max_data_rates=req_rs_config_set_max_data_rates)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_config_set_max_data_rates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_config_set_max_data_rates** | [**ReqRsConfigSetMaxDataRates**](ReqRsConfigSetMaxDataRates.md)| downKb: \&quot;( integer)download rate in kB \&quot;         upKb: \&quot;( integer)upload rate in kB \&quot;  | [optional] 

### Return type

[**RespRsConfigSetMaxDataRates**](RespRsConfigSetMaxDataRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: returns 1 on succes and 0 otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_config_set_operating_mode**
> RespRsConfigSetOperatingMode rs_config_set_operating_mode(req_rs_config_set_operating_mode=req_rs_config_set_operating_mode)

setOperatingMode set the current oprating mode

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_config_set_operating_mode = {"opMode":"( integer)new opearting mode "} # ReqRsConfigSetOperatingMode | opMode: \"( integer)new opearting mode \"  (optional)

try:
    # setOperatingMode set the current oprating mode
    api_response = api_instance.rs_config_set_operating_mode(req_rs_config_set_operating_mode=req_rs_config_set_operating_mode)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_config_set_operating_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_config_set_operating_mode** | [**ReqRsConfigSetOperatingMode**](ReqRsConfigSetOperatingMode.md)| opMode: \&quot;( integer)new opearting mode \&quot;  | [optional] 

### Return type

[**RespRsConfigSetOperatingMode**](RespRsConfigSetOperatingMode.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_control_is_ready**
> RespRsControlIsReady rs_control_is_ready()

Check if core is fully ready, true only after

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Check if core is fully ready, true only after
    api_response = api_instance.rs_control_is_ready()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_control_is_ready: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsControlIsReady**](RespRsControlIsReady.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_control_rs_global_shut_down**
> object rs_control_rs_global_shut_down()

Turn off RetroShare

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Turn off RetroShare
    api_response = api_instance.rs_control_rs_global_shut_down()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_control_rs_global_shut_down: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_events_register_events_handler**
> RespRsEventsRegisterEventsHandler rs_events_register_events_handler(req_rs_events_register_events_handler=req_rs_events_register_events_handler)

This method is asynchronous. Register events handler Every time an event is dispatced the registered events handlers will get their method handleEvent called with the event passed as paramether.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_events_register_events_handler = {"hId":"( RsEventsHandlerId_t)Optional storage for handler id, useful to eventually unregister the handler later. The value may be provided to the function call but must habe been generated with "} # ReqRsEventsRegisterEventsHandler | hId: \"( RsEventsHandlerId_t)Optional storage for handler id, useful to eventually unregister the handler later. The value may be provided to the function call but must habe been generated with \"  (optional)

try:
    # This method is asynchronous. Register events handler Every time an event is dispatced the registered events handlers will get their method handleEvent called with the event passed as paramether.
    api_response = api_instance.rs_events_register_events_handler(req_rs_events_register_events_handler=req_rs_events_register_events_handler)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_events_register_events_handler: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_events_register_events_handler** | [**ReqRsEventsRegisterEventsHandler**](ReqRsEventsRegisterEventsHandler.md)| hId: \&quot;( RsEventsHandlerId_t)Optional storage for handler id, useful to eventually unregister the handler later. The value may be provided to the function call but must habe been generated with \&quot;  | [optional] 

### Return type

[**RespRsEventsRegisterEventsHandler**](RespRsEventsRegisterEventsHandler.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: False on error, true otherwise.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_add_shared_directory**
> RespRsFilesAddSharedDirectory rs_files_add_shared_directory(req_rs_files_add_shared_directory=req_rs_files_add_shared_directory)

Add shared directory

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_add_shared_directory = {"dir":"( SharedDirInfo)directory to share with sharing options "} # ReqRsFilesAddSharedDirectory | dir: \"( SharedDirInfo)directory to share with sharing options \"  (optional)

try:
    # Add shared directory
    api_response = api_instance.rs_files_add_shared_directory(req_rs_files_add_shared_directory=req_rs_files_add_shared_directory)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_add_shared_directory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_add_shared_directory** | [**ReqRsFilesAddSharedDirectory**](ReqRsFilesAddSharedDirectory.md)| dir: \&quot;( SharedDirInfo)directory to share with sharing options \&quot;  | [optional] 

### Return type

[**RespRsFilesAddSharedDirectory**](RespRsFilesAddSharedDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_already_have_file**
> RespRsFilesAlreadyHaveFile rs_files_already_have_file(req_rs_files_already_have_file=req_rs_files_already_have_file)

Check if we already have a file

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_already_have_file = {"hash":"( RsFileHash)file identifier "} # ReqRsFilesAlreadyHaveFile | hash: \"( RsFileHash)file identifier \"  (optional)

try:
    # Check if we already have a file
    api_response = api_instance.rs_files_already_have_file(req_rs_files_already_have_file=req_rs_files_already_have_file)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_already_have_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_already_have_file** | [**ReqRsFilesAlreadyHaveFile**](ReqRsFilesAlreadyHaveFile.md)| hash: \&quot;( RsFileHash)file identifier \&quot;  | [optional] 

### Return type

[**RespRsFilesAlreadyHaveFile**](RespRsFilesAlreadyHaveFile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if the file is already present, false otherwise  [out] info(FileInfo)storage for the possibly found file information   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_ban_file**
> RespRsFilesBanFile rs_files_ban_file(req_rs_files_ban_file=req_rs_files_ban_file)

Ban unwanted file from being, searched and forwarded by this node

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_ban_file = {"realFileHash":"( RsFileHash)this is what will really enforce banning ","filename":"( string)expected name of the file, for the user to read ","fileSize":"( integer)expected file size, for the user to read "} # ReqRsFilesBanFile | realFileHash: \"( RsFileHash)this is what will really enforce banning \"         filename: \"( string)expected name of the file, for the user to read \"         fileSize: \"( integer)expected file size, for the user to read \"  (optional)

try:
    # Ban unwanted file from being, searched and forwarded by this node
    api_response = api_instance.rs_files_ban_file(req_rs_files_ban_file=req_rs_files_ban_file)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_ban_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_ban_file** | [**ReqRsFilesBanFile**](ReqRsFilesBanFile.md)| realFileHash: \&quot;( RsFileHash)this is what will really enforce banning \&quot;         filename: \&quot;( string)expected name of the file, for the user to read \&quot;         fileSize: \&quot;( integer)expected file size, for the user to read \&quot;  | [optional] 

### Return type

[**RespRsFilesBanFile**](RespRsFilesBanFile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: meaningless value   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_default_chunk_strategy**
> RespRsFilesDefaultChunkStrategy rs_files_default_chunk_strategy()

Get default chunk strategy

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get default chunk strategy
    api_response = api_instance.rs_files_default_chunk_strategy()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_default_chunk_strategy: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsFilesDefaultChunkStrategy**](RespRsFilesDefaultChunkStrategy.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: current default chunck strategy   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_extra_file_hash**
> RespRsFilesExtraFileHash rs_files_extra_file_hash(req_rs_files_extra_file_hash=req_rs_files_extra_file_hash)

Add file to extra shared file list

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_extra_file_hash = {"localpath":"( string)path of the file ","period":"( rstime_t)how much time the file will be kept in extra list in seconds ","flags":"( TransferRequestFlags)sharing policy flags ex: RS_FILE_REQ_ANONYMOUS_ROUTING "} # ReqRsFilesExtraFileHash | localpath: \"( string)path of the file \"         period: \"( rstime_t)how much time the file will be kept in extra list in seconds \"         flags: \"( TransferRequestFlags)sharing policy flags ex: RS_FILE_REQ_ANONYMOUS_ROUTING \"  (optional)

try:
    # Add file to extra shared file list
    api_response = api_instance.rs_files_extra_file_hash(req_rs_files_extra_file_hash=req_rs_files_extra_file_hash)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_extra_file_hash: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_extra_file_hash** | [**ReqRsFilesExtraFileHash**](ReqRsFilesExtraFileHash.md)| localpath: \&quot;( string)path of the file \&quot;         period: \&quot;( rstime_t)how much time the file will be kept in extra list in seconds \&quot;         flags: \&quot;( TransferRequestFlags)sharing policy flags ex: RS_FILE_REQ_ANONYMOUS_ROUTING \&quot;  | [optional] 

### Return type

[**RespRsFilesExtraFileHash**](RespRsFilesExtraFileHash.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_extra_file_remove**
> RespRsFilesExtraFileRemove rs_files_extra_file_remove(req_rs_files_extra_file_remove=req_rs_files_extra_file_remove)

Remove file from extra fila shared list

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_extra_file_remove = {"hash":"( RsFileHash)hash of the file to remove "} # ReqRsFilesExtraFileRemove | hash: \"( RsFileHash)hash of the file to remove \"  (optional)

try:
    # Remove file from extra fila shared list
    api_response = api_instance.rs_files_extra_file_remove(req_rs_files_extra_file_remove=req_rs_files_extra_file_remove)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_extra_file_remove: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_extra_file_remove** | [**ReqRsFilesExtraFileRemove**](ReqRsFilesExtraFileRemove.md)| hash: \&quot;( RsFileHash)hash of the file to remove \&quot;  | [optional] 

### Return type

[**RespRsFilesExtraFileRemove**](RespRsFilesExtraFileRemove.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: return false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_extra_file_status**
> RespRsFilesExtraFileStatus rs_files_extra_file_status(req_rs_files_extra_file_status=req_rs_files_extra_file_status)

Get extra file information

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_extra_file_status = {"localpath":"( string)path of the file "} # ReqRsFilesExtraFileStatus | localpath: \"( string)path of the file \"  (optional)

try:
    # Get extra file information
    api_response = api_instance.rs_files_extra_file_status(req_rs_files_extra_file_status=req_rs_files_extra_file_status)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_extra_file_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_extra_file_status** | [**ReqRsFilesExtraFileStatus**](ReqRsFilesExtraFileStatus.md)| localpath: \&quot;( string)path of the file \&quot;  | [optional] 

### Return type

[**RespRsFilesExtraFileStatus**](RespRsFilesExtraFileStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] info(FileInfo)storage for the file information   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_file_cancel**
> RespRsFilesFileCancel rs_files_file_cancel(req_rs_files_file_cancel=req_rs_files_file_cancel)

Cancel file downloading

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_file_cancel = {"hash":"( RsFileHash)None"} # ReqRsFilesFileCancel | hash: \"( RsFileHash)None\"  (optional)

try:
    # Cancel file downloading
    api_response = api_instance.rs_files_file_cancel(req_rs_files_file_cancel=req_rs_files_file_cancel)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_file_cancel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_file_cancel** | [**ReqRsFilesFileCancel**](ReqRsFilesFileCancel.md)| hash: \&quot;( RsFileHash)None\&quot;  | [optional] 

### Return type

[**RespRsFilesFileCancel**](RespRsFilesFileCancel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if the file is not in the download queue, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_file_clear_completed**
> RespRsFilesFileClearCompleted rs_files_file_clear_completed()

Clear completed downloaded files list

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Clear completed downloaded files list
    api_response = api_instance.rs_files_file_clear_completed()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_file_clear_completed: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsFilesFileClearCompleted**](RespRsFilesFileClearCompleted.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_file_control**
> RespRsFilesFileControl rs_files_file_control(req_rs_files_file_control=req_rs_files_file_control)

Controls file transfer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_file_control = {"hash":"( RsFileHash)file identifier ","flags":"( integer)action to perform. Pict into { RS_FILE_CTRL_PAUSE, RS_FILE_CTRL_START, RS_FILE_CTRL_FORCE_CHECK } } "} # ReqRsFilesFileControl | hash: \"( RsFileHash)file identifier \"         flags: \"( integer)action to perform. Pict into { RS_FILE_CTRL_PAUSE, RS_FILE_CTRL_START, RS_FILE_CTRL_FORCE_CHECK } } \"  (optional)

try:
    # Controls file transfer
    api_response = api_instance.rs_files_file_control(req_rs_files_file_control=req_rs_files_file_control)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_file_control: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_file_control** | [**ReqRsFilesFileControl**](ReqRsFilesFileControl.md)| hash: \&quot;( RsFileHash)file identifier \&quot;         flags: \&quot;( integer)action to perform. Pict into { RS_FILE_CTRL_PAUSE, RS_FILE_CTRL_START, RS_FILE_CTRL_FORCE_CHECK } } \&quot;  | [optional] 

### Return type

[**RespRsFilesFileControl**](RespRsFilesFileControl.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occured such as unknown hash.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_file_details**
> RespRsFilesFileDetails rs_files_file_details(req_rs_files_file_details=req_rs_files_file_details)

Get file details

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_file_details = {"hash":"( RsFileHash)file identifier ","hintflags":"( FileSearchFlags)filtering hint (RS_FILE_HINTS_EXTRA|...|RS_FILE_HINTS_LOCAL) "} # ReqRsFilesFileDetails | hash: \"( RsFileHash)file identifier \"         hintflags: \"( FileSearchFlags)filtering hint (RS_FILE_HINTS_EXTRA|...|RS_FILE_HINTS_LOCAL) \"  (optional)

try:
    # Get file details
    api_response = api_instance.rs_files_file_details(req_rs_files_file_details=req_rs_files_file_details)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_file_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_file_details** | [**ReqRsFilesFileDetails**](ReqRsFilesFileDetails.md)| hash: \&quot;( RsFileHash)file identifier \&quot;         hintflags: \&quot;( FileSearchFlags)filtering hint (RS_FILE_HINTS_EXTRA|...|RS_FILE_HINTS_LOCAL) \&quot;  | [optional] 

### Return type

[**RespRsFilesFileDetails**](RespRsFilesFileDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if file found, false otherwise  [out] info(FileInfo)storage for file information   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_file_download_chunks_details**
> RespRsFilesFileDownloadChunksDetails rs_files_file_download_chunks_details(req_rs_files_file_download_chunks_details=req_rs_files_file_download_chunks_details)

Get chunk details about the downloaded file with given hash.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_file_download_chunks_details = {"hash":"( RsFileHash)file identifier "} # ReqRsFilesFileDownloadChunksDetails | hash: \"( RsFileHash)file identifier \"  (optional)

try:
    # Get chunk details about the downloaded file with given hash.
    api_response = api_instance.rs_files_file_download_chunks_details(req_rs_files_file_download_chunks_details=req_rs_files_file_download_chunks_details)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_file_download_chunks_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_file_download_chunks_details** | [**ReqRsFilesFileDownloadChunksDetails**](ReqRsFilesFileDownloadChunksDetails.md)| hash: \&quot;( RsFileHash)file identifier \&quot;  | [optional] 

### Return type

[**RespRsFilesFileDownloadChunksDetails**](RespRsFilesFileDownloadChunksDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if file found, false otherwise  [out] info(FileChunksInfo)storage for file information   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_file_downloads**
> RespRsFilesFileDownloads rs_files_file_downloads()

Get incoming files list

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get incoming files list
    api_response = api_instance.rs_files_file_downloads()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_file_downloads: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsFilesFileDownloads**](RespRsFilesFileDownloads.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:  [out] hashs(list&lt;RsFileHash&gt;)storage for files identifiers list   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_file_request**
> RespRsFilesFileRequest rs_files_file_request(req_rs_files_file_request=req_rs_files_file_request)

Initiate downloading of a file

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_file_request = {"fileName":"( string)None","hash":"( RsFileHash)None","size":"( integer)None","destPath":"( string)in not empty specify a destination path ","flags":"( TransferRequestFlags)you usually want RS_FILE_REQ_ANONYMOUS_ROUTING ","srcIds":"( list<RsPeerId>)eventually specify known sources "} # ReqRsFilesFileRequest | fileName: \"( string)None\"         hash: \"( RsFileHash)None\"         size: \"( integer)None\"         destPath: \"( string)in not empty specify a destination path \"         flags: \"( TransferRequestFlags)you usually want RS_FILE_REQ_ANONYMOUS_ROUTING \"         srcIds: \"( list<RsPeerId>)eventually specify known sources \"  (optional)

try:
    # Initiate downloading of a file
    api_response = api_instance.rs_files_file_request(req_rs_files_file_request=req_rs_files_file_request)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_file_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_file_request** | [**ReqRsFilesFileRequest**](ReqRsFilesFileRequest.md)| fileName: \&quot;( string)None\&quot;         hash: \&quot;( RsFileHash)None\&quot;         size: \&quot;( integer)None\&quot;         destPath: \&quot;( string)in not empty specify a destination path \&quot;         flags: \&quot;( TransferRequestFlags)you usually want RS_FILE_REQ_ANONYMOUS_ROUTING \&quot;         srcIds: \&quot;( list&lt;RsPeerId&gt;)eventually specify known sources \&quot;  | [optional] 

### Return type

[**RespRsFilesFileRequest**](RespRsFilesFileRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if we already have the file, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_file_upload_chunks_details**
> RespRsFilesFileUploadChunksDetails rs_files_file_upload_chunks_details(req_rs_files_file_upload_chunks_details=req_rs_files_file_upload_chunks_details)

Get details about the upload with given hash

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_file_upload_chunks_details = {"hash":"( RsFileHash)file identifier ","peerId":"( RsPeerId)peer identifier "} # ReqRsFilesFileUploadChunksDetails | hash: \"( RsFileHash)file identifier \"         peerId: \"( RsPeerId)peer identifier \"  (optional)

try:
    # Get details about the upload with given hash
    api_response = api_instance.rs_files_file_upload_chunks_details(req_rs_files_file_upload_chunks_details=req_rs_files_file_upload_chunks_details)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_file_upload_chunks_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_file_upload_chunks_details** | [**ReqRsFilesFileUploadChunksDetails**](ReqRsFilesFileUploadChunksDetails.md)| hash: \&quot;( RsFileHash)file identifier \&quot;         peerId: \&quot;( RsPeerId)peer identifier \&quot;  | [optional] 

### Return type

[**RespRsFilesFileUploadChunksDetails**](RespRsFilesFileUploadChunksDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if file found, false otherwise  [out] map(CompressedChunkMap)storage for chunk info   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_file_uploads**
> RespRsFilesFileUploads rs_files_file_uploads()

Get outgoing files list

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get outgoing files list
    api_response = api_instance.rs_files_file_uploads()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_file_uploads: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsFilesFileUploads**](RespRsFilesFileUploads.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if some error occurred, true otherwise  [out] hashs(list&lt;RsFileHash&gt;)storage for files identifiers list   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_force_directory_check**
> object rs_files_force_directory_check()

Force shared directories check

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Force shared directories check
    api_response = api_instance.rs_files_force_directory_check()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_force_directory_check: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_free_disk_space_limit**
> RespRsFilesFreeDiskSpaceLimit rs_files_free_disk_space_limit()

Get free disk space limit

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get free disk space limit
    api_response = api_instance.rs_files_free_disk_space_limit()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_free_disk_space_limit: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsFilesFreeDiskSpaceLimit**](RespRsFilesFreeDiskSpaceLimit.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: current minimum free space on disk in MB   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_get_download_directory**
> RespRsFilesGetDownloadDirectory rs_files_get_download_directory()

Get default complete downloads directory

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get default complete downloads directory
    api_response = api_instance.rs_files_get_download_directory()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_get_download_directory: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsFilesGetDownloadDirectory**](RespRsFilesGetDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: default completed downloads directory path   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_get_file_data**
> RespRsFilesGetFileData rs_files_get_file_data(req_rs_files_get_file_data=req_rs_files_get_file_data)

Provides file data for the gui, media streaming or rpc clients. It may return unverified chunks. This allows streaming without having to wait for hashes or completion of the file. This function returns an unspecified amount of bytes. Either as much data as available or a sensible maximum. Expect a block size of around 1MiB. To get more data, call this function repeatedly with different offsets.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_get_file_data = {"hash":"( RsFileHash)hash of the file. The file has to be available on this node or it has to be in downloading state. ","offset":"( integer)where the desired block starts ","requested_size":"( integer)size of pre-allocated data. Will be updated by the function. "} # ReqRsFilesGetFileData | hash: \"( RsFileHash)hash of the file. The file has to be available on this node or it has to be in downloading state. \"         offset: \"( integer)where the desired block starts \"         requested_size: \"( integer)size of pre-allocated data. Will be updated by the function. \"  (optional)

try:
    # Provides file data for the gui, media streaming or rpc clients. It may return unverified chunks. This allows streaming without having to wait for hashes or completion of the file. This function returns an unspecified amount of bytes. Either as much data as available or a sensible maximum. Expect a block size of around 1MiB. To get more data, call this function repeatedly with different offsets.
    api_response = api_instance.rs_files_get_file_data(req_rs_files_get_file_data=req_rs_files_get_file_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_get_file_data: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_get_file_data** | [**ReqRsFilesGetFileData**](ReqRsFilesGetFileData.md)| hash: \&quot;( RsFileHash)hash of the file. The file has to be available on this node or it has to be in downloading state. \&quot;         offset: \&quot;( integer)where the desired block starts \&quot;         requested_size: \&quot;( integer)size of pre-allocated data. Will be updated by the function. \&quot;  | [optional] 

### Return type

[**RespRsFilesGetFileData**](RespRsFilesGetFileData.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: Returns false in case [out] data(integer)pre-allocated memory chunk of size &#39;requested_size&#39; by the client   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_get_partials_directory**
> RespRsFilesGetPartialsDirectory rs_files_get_partials_directory()

Get partial downloads directory

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get partial downloads directory
    api_response = api_instance.rs_files_get_partials_directory()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_get_partials_directory: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsFilesGetPartialsDirectory**](RespRsFilesGetPartialsDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: partials downloads directory path   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_get_primary_banned_files_list**
> RespRsFilesGetPrimaryBannedFilesList rs_files_get_primary_banned_files_list()

Get list of banned files

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get list of banned files
    api_response = api_instance.rs_files_get_primary_banned_files_list()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_get_primary_banned_files_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsFilesGetPrimaryBannedFilesList**](RespRsFilesGetPrimaryBannedFilesList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: meaningless value  [out] bannedFiles(map&lt;RsFileHash,BannedFileEntry&gt;)storage for banned files information   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_get_shared_directories**
> RespRsFilesGetSharedDirectories rs_files_get_shared_directories()

Get list of current shared directories

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get list of current shared directories
    api_response = api_instance.rs_files_get_shared_directories()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_get_shared_directories: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsFilesGetSharedDirectories**](RespRsFilesGetSharedDirectories.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] dirs(list&lt;SharedDirInfo&gt;)storage for the list of share directories   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_is_hash_banned**
> RespRsFilesIsHashBanned rs_files_is_hash_banned(req_rs_files_is_hash_banned=req_rs_files_is_hash_banned)

Check if a file is on banned list

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_is_hash_banned = {"hash":"( RsFileHash)hash of the file "} # ReqRsFilesIsHashBanned | hash: \"( RsFileHash)hash of the file \"  (optional)

try:
    # Check if a file is on banned list
    api_response = api_instance.rs_files_is_hash_banned(req_rs_files_is_hash_banned=req_rs_files_is_hash_banned)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_is_hash_banned: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_is_hash_banned** | [**ReqRsFilesIsHashBanned**](ReqRsFilesIsHashBanned.md)| hash: \&quot;( RsFileHash)hash of the file \&quot;  | [optional] 

### Return type

[**RespRsFilesIsHashBanned**](RespRsFilesIsHashBanned.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if the hash is on the list, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_remove_shared_directory**
> RespRsFilesRemoveSharedDirectory rs_files_remove_shared_directory(req_rs_files_remove_shared_directory=req_rs_files_remove_shared_directory)

Remove directory from shared list

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_remove_shared_directory = {"dir":"( string)Path of the directory to remove from shared list "} # ReqRsFilesRemoveSharedDirectory | dir: \"( string)Path of the directory to remove from shared list \"  (optional)

try:
    # Remove directory from shared list
    api_response = api_instance.rs_files_remove_shared_directory(req_rs_files_remove_shared_directory=req_rs_files_remove_shared_directory)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_remove_shared_directory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_remove_shared_directory** | [**ReqRsFilesRemoveSharedDirectory**](ReqRsFilesRemoveSharedDirectory.md)| dir: \&quot;( string)Path of the directory to remove from shared list \&quot;  | [optional] 

### Return type

[**RespRsFilesRemoveSharedDirectory**](RespRsFilesRemoveSharedDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_request_dir_details**
> RespRsFilesRequestDirDetails rs_files_request_dir_details(req_rs_files_request_dir_details=req_rs_files_request_dir_details)

Request directory details, subsequent multiple call may be used to explore a whole directory tree.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_request_dir_details = {"handle":"( integer)element handle 0 for root, pass the content of DirDetails::child[x].ref after first call to explore deeper, be aware that is not a real pointer but an index used internally by RetroShare. ","flags":"( FileSearchFlags)file search flags RS_FILE_HINTS_* "} # ReqRsFilesRequestDirDetails | handle: \"( integer)element handle 0 for root, pass the content of DirDetails::child[x].ref after first call to explore deeper, be aware that is not a real pointer but an index used internally by RetroShare. \"         flags: \"( FileSearchFlags)file search flags RS_FILE_HINTS_* \"  (optional)

try:
    # Request directory details, subsequent multiple call may be used to explore a whole directory tree.
    api_response = api_instance.rs_files_request_dir_details(req_rs_files_request_dir_details=req_rs_files_request_dir_details)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_request_dir_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_request_dir_details** | [**ReqRsFilesRequestDirDetails**](ReqRsFilesRequestDirDetails.md)| handle: \&quot;( integer)element handle 0 for root, pass the content of DirDetails::child[x].ref after first call to explore deeper, be aware that is not a real pointer but an index used internally by RetroShare. \&quot;         flags: \&quot;( FileSearchFlags)file search flags RS_FILE_HINTS_* \&quot;  | [optional] 

### Return type

[**RespRsFilesRequestDirDetails**](RespRsFilesRequestDirDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise  [out] details(DirDetails)Storage for directory details   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_set_chunk_strategy**
> RespRsFilesSetChunkStrategy rs_files_set_chunk_strategy(req_rs_files_set_chunk_strategy=req_rs_files_set_chunk_strategy)

Set chunk strategy for file, useful to set streaming mode to be able of see video or other media preview while it is still downloading

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_set_chunk_strategy = {"hash":"( RsFileHash)file identifier ","newStrategy":"( FileChunksInfo_ChunkStrategy)None"} # ReqRsFilesSetChunkStrategy | hash: \"( RsFileHash)file identifier \"         newStrategy: \"( FileChunksInfo_ChunkStrategy)None\"  (optional)

try:
    # Set chunk strategy for file, useful to set streaming mode to be able of see video or other media preview while it is still downloading
    api_response = api_instance.rs_files_set_chunk_strategy(req_rs_files_set_chunk_strategy=req_rs_files_set_chunk_strategy)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_set_chunk_strategy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_set_chunk_strategy** | [**ReqRsFilesSetChunkStrategy**](ReqRsFilesSetChunkStrategy.md)| hash: \&quot;( RsFileHash)file identifier \&quot;         newStrategy: \&quot;( FileChunksInfo_ChunkStrategy)None\&quot;  | [optional] 

### Return type

[**RespRsFilesSetChunkStrategy**](RespRsFilesSetChunkStrategy.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if some error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_set_default_chunk_strategy**
> object rs_files_set_default_chunk_strategy(req_rs_files_set_default_chunk_strategy=req_rs_files_set_default_chunk_strategy)

Set default chunk strategy

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_set_default_chunk_strategy = {"strategy":"( FileChunksInfo_ChunkStrategy)None"} # ReqRsFilesSetDefaultChunkStrategy | strategy: \"( FileChunksInfo_ChunkStrategy)None\"  (optional)

try:
    # Set default chunk strategy
    api_response = api_instance.rs_files_set_default_chunk_strategy(req_rs_files_set_default_chunk_strategy=req_rs_files_set_default_chunk_strategy)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_set_default_chunk_strategy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_set_default_chunk_strategy** | [**ReqRsFilesSetDefaultChunkStrategy**](ReqRsFilesSetDefaultChunkStrategy.md)| strategy: \&quot;( FileChunksInfo_ChunkStrategy)None\&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_set_destination_directory**
> RespRsFilesSetDestinationDirectory rs_files_set_destination_directory(req_rs_files_set_destination_directory=req_rs_files_set_destination_directory)

Set destination directory for given file

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_set_destination_directory = {"hash":"( RsFileHash)file identifier ","newPath":"( string)None"} # ReqRsFilesSetDestinationDirectory | hash: \"( RsFileHash)file identifier \"         newPath: \"( string)None\"  (optional)

try:
    # Set destination directory for given file
    api_response = api_instance.rs_files_set_destination_directory(req_rs_files_set_destination_directory=req_rs_files_set_destination_directory)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_set_destination_directory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_set_destination_directory** | [**ReqRsFilesSetDestinationDirectory**](ReqRsFilesSetDestinationDirectory.md)| hash: \&quot;( RsFileHash)file identifier \&quot;         newPath: \&quot;( string)None\&quot;  | [optional] 

### Return type

[**RespRsFilesSetDestinationDirectory**](RespRsFilesSetDestinationDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if some error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_set_destination_name**
> RespRsFilesSetDestinationName rs_files_set_destination_name(req_rs_files_set_destination_name=req_rs_files_set_destination_name)

Set name for dowloaded file

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_set_destination_name = {"hash":"( RsFileHash)file identifier ","newName":"( string)None"} # ReqRsFilesSetDestinationName | hash: \"( RsFileHash)file identifier \"         newName: \"( string)None\"  (optional)

try:
    # Set name for dowloaded file
    api_response = api_instance.rs_files_set_destination_name(req_rs_files_set_destination_name=req_rs_files_set_destination_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_set_destination_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_set_destination_name** | [**ReqRsFilesSetDestinationName**](ReqRsFilesSetDestinationName.md)| hash: \&quot;( RsFileHash)file identifier \&quot;         newName: \&quot;( string)None\&quot;  | [optional] 

### Return type

[**RespRsFilesSetDestinationName**](RespRsFilesSetDestinationName.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if some error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_set_download_directory**
> RespRsFilesSetDownloadDirectory rs_files_set_download_directory(req_rs_files_set_download_directory=req_rs_files_set_download_directory)

Set default complete downloads directory

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_set_download_directory = {"path":"( string)directory path "} # ReqRsFilesSetDownloadDirectory | path: \"( string)directory path \"  (optional)

try:
    # Set default complete downloads directory
    api_response = api_instance.rs_files_set_download_directory(req_rs_files_set_download_directory=req_rs_files_set_download_directory)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_set_download_directory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_set_download_directory** | [**ReqRsFilesSetDownloadDirectory**](ReqRsFilesSetDownloadDirectory.md)| path: \&quot;( string)directory path \&quot;  | [optional] 

### Return type

[**RespRsFilesSetDownloadDirectory**](RespRsFilesSetDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_set_free_disk_space_limit**
> object rs_files_set_free_disk_space_limit(req_rs_files_set_free_disk_space_limit=req_rs_files_set_free_disk_space_limit)

Set minimum free disk space limit

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_set_free_disk_space_limit = {"minimumFreeMB":"( integer)minimum free space in MB "} # ReqRsFilesSetFreeDiskSpaceLimit | minimumFreeMB: \"( integer)minimum free space in MB \"  (optional)

try:
    # Set minimum free disk space limit
    api_response = api_instance.rs_files_set_free_disk_space_limit(req_rs_files_set_free_disk_space_limit=req_rs_files_set_free_disk_space_limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_set_free_disk_space_limit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_set_free_disk_space_limit** | [**ReqRsFilesSetFreeDiskSpaceLimit**](ReqRsFilesSetFreeDiskSpaceLimit.md)| minimumFreeMB: \&quot;( integer)minimum free space in MB \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_set_partials_directory**
> RespRsFilesSetPartialsDirectory rs_files_set_partials_directory(req_rs_files_set_partials_directory=req_rs_files_set_partials_directory)

Set partial downloads directory

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_set_partials_directory = {"path":"( string)directory path "} # ReqRsFilesSetPartialsDirectory | path: \"( string)directory path \"  (optional)

try:
    # Set partial downloads directory
    api_response = api_instance.rs_files_set_partials_directory(req_rs_files_set_partials_directory=req_rs_files_set_partials_directory)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_set_partials_directory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_set_partials_directory** | [**ReqRsFilesSetPartialsDirectory**](ReqRsFilesSetPartialsDirectory.md)| path: \&quot;( string)directory path \&quot;  | [optional] 

### Return type

[**RespRsFilesSetPartialsDirectory**](RespRsFilesSetPartialsDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_set_shared_directories**
> RespRsFilesSetSharedDirectories rs_files_set_shared_directories(req_rs_files_set_shared_directories=req_rs_files_set_shared_directories)

Set shared directories

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_set_shared_directories = {"dirs":"( list<SharedDirInfo>)list of shared directories with share options "} # ReqRsFilesSetSharedDirectories | dirs: \"( list<SharedDirInfo>)list of shared directories with share options \"  (optional)

try:
    # Set shared directories
    api_response = api_instance.rs_files_set_shared_directories(req_rs_files_set_shared_directories=req_rs_files_set_shared_directories)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_set_shared_directories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_set_shared_directories** | [**ReqRsFilesSetSharedDirectories**](ReqRsFilesSetSharedDirectories.md)| dirs: \&quot;( list&lt;SharedDirInfo&gt;)list of shared directories with share options \&quot;  | [optional] 

### Return type

[**RespRsFilesSetSharedDirectories**](RespRsFilesSetSharedDirectories.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_turtle_search_request**
> RespRsFilesTurtleSearchRequest rs_files_turtle_search_request(req_rs_files_turtle_search_request=req_rs_files_turtle_search_request)

This method is asynchronous. Request remote files search

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_turtle_search_request = {"matchString":"( string)string to look for in the search ","maxWait":"( rstime_t)maximum wait time in seconds for search results "} # ReqRsFilesTurtleSearchRequest | matchString: \"( string)string to look for in the search \"         maxWait: \"( rstime_t)maximum wait time in seconds for search results \"  (optional)

try:
    # This method is asynchronous. Request remote files search
    api_response = api_instance.rs_files_turtle_search_request(req_rs_files_turtle_search_request=req_rs_files_turtle_search_request)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_turtle_search_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_turtle_search_request** | [**ReqRsFilesTurtleSearchRequest**](ReqRsFilesTurtleSearchRequest.md)| matchString: \&quot;( string)string to look for in the search \&quot;         maxWait: \&quot;( rstime_t)maximum wait time in seconds for search results \&quot;  | [optional] 

### Return type

[**RespRsFilesTurtleSearchRequest**](RespRsFilesTurtleSearchRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_unban_file**
> RespRsFilesUnbanFile rs_files_unban_file(req_rs_files_unban_file=req_rs_files_unban_file)

Remove file from unwanted list

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_unban_file = {"realFileHash":"( RsFileHash)hash of the file "} # ReqRsFilesUnbanFile | realFileHash: \"( RsFileHash)hash of the file \"  (optional)

try:
    # Remove file from unwanted list
    api_response = api_instance.rs_files_unban_file(req_rs_files_unban_file=req_rs_files_unban_file)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_unban_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_unban_file** | [**ReqRsFilesUnbanFile**](ReqRsFilesUnbanFile.md)| realFileHash: \&quot;( RsFileHash)hash of the file \&quot;  | [optional] 

### Return type

[**RespRsFilesUnbanFile**](RespRsFilesUnbanFile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: meaningless value   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_files_update_share_flags**
> RespRsFilesUpdateShareFlags rs_files_update_share_flags(req_rs_files_update_share_flags=req_rs_files_update_share_flags)

Updates shared directory sharing flags. The directory should be already shared!

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_files_update_share_flags = {"dir":"( SharedDirInfo)Shared directory with updated sharing options "} # ReqRsFilesUpdateShareFlags | dir: \"( SharedDirInfo)Shared directory with updated sharing options \"  (optional)

try:
    # Updates shared directory sharing flags. The directory should be already shared!
    api_response = api_instance.rs_files_update_share_flags(req_rs_files_update_share_flags=req_rs_files_update_share_flags)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_files_update_share_flags: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_files_update_share_flags** | [**ReqRsFilesUpdateShareFlags**](ReqRsFilesUpdateShareFlags.md)| dir: \&quot;( SharedDirInfo)Shared directory with updated sharing options \&quot;  | [optional] 

### Return type

[**RespRsFilesUpdateShareFlags**](RespRsFilesUpdateShareFlags.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gossip_discovery_get_disc_friends**
> RespRsGossipDiscoveryGetDiscFriends rs_gossip_discovery_get_disc_friends(req_rs_gossip_discovery_get_disc_friends=req_rs_gossip_discovery_get_disc_friends)

getDiscFriends get a list of all friends of a given friend

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gossip_discovery_get_disc_friends = {"id":"( RsPeerId)peer to get the friends of "} # ReqRsGossipDiscoveryGetDiscFriends | id: \"( RsPeerId)peer to get the friends of \"  (optional)

try:
    # getDiscFriends get a list of all friends of a given friend
    api_response = api_instance.rs_gossip_discovery_get_disc_friends(req_rs_gossip_discovery_get_disc_friends=req_rs_gossip_discovery_get_disc_friends)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gossip_discovery_get_disc_friends: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gossip_discovery_get_disc_friends** | [**ReqRsGossipDiscoveryGetDiscFriends**](ReqRsGossipDiscoveryGetDiscFriends.md)| id: \&quot;( RsPeerId)peer to get the friends of \&quot;  | [optional] 

### Return type

[**RespRsGossipDiscoveryGetDiscFriends**](RespRsGossipDiscoveryGetDiscFriends.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success false otherwise  [out] friends(list&lt;RsPeerId&gt;)list of friends (ssl id)   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gossip_discovery_get_disc_pgp_friends**
> RespRsGossipDiscoveryGetDiscPgpFriends rs_gossip_discovery_get_disc_pgp_friends(req_rs_gossip_discovery_get_disc_pgp_friends=req_rs_gossip_discovery_get_disc_pgp_friends)

getDiscPgpFriends get a list of all friends of a given friend

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gossip_discovery_get_disc_pgp_friends = {"pgpid":"( RsPgpId)peer to get the friends of "} # ReqRsGossipDiscoveryGetDiscPgpFriends | pgpid: \"( RsPgpId)peer to get the friends of \"  (optional)

try:
    # getDiscPgpFriends get a list of all friends of a given friend
    api_response = api_instance.rs_gossip_discovery_get_disc_pgp_friends(req_rs_gossip_discovery_get_disc_pgp_friends=req_rs_gossip_discovery_get_disc_pgp_friends)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gossip_discovery_get_disc_pgp_friends: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gossip_discovery_get_disc_pgp_friends** | [**ReqRsGossipDiscoveryGetDiscPgpFriends**](ReqRsGossipDiscoveryGetDiscPgpFriends.md)| pgpid: \&quot;( RsPgpId)peer to get the friends of \&quot;  | [optional] 

### Return type

[**RespRsGossipDiscoveryGetDiscPgpFriends**](RespRsGossipDiscoveryGetDiscPgpFriends.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success false otherwise  [out] gpg_friends(list&lt;RsPgpId&gt;)list of friends (gpg id)   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gossip_discovery_get_peer_version**
> RespRsGossipDiscoveryGetPeerVersion rs_gossip_discovery_get_peer_version(req_rs_gossip_discovery_get_peer_version=req_rs_gossip_discovery_get_peer_version)

getPeerVersion get the version string of a peer.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gossip_discovery_get_peer_version = {"id":"( RsPeerId)peer to get the version string of "} # ReqRsGossipDiscoveryGetPeerVersion | id: \"( RsPeerId)peer to get the version string of \"  (optional)

try:
    # getPeerVersion get the version string of a peer.
    api_response = api_instance.rs_gossip_discovery_get_peer_version(req_rs_gossip_discovery_get_peer_version=req_rs_gossip_discovery_get_peer_version)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gossip_discovery_get_peer_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gossip_discovery_get_peer_version** | [**ReqRsGossipDiscoveryGetPeerVersion**](ReqRsGossipDiscoveryGetPeerVersion.md)| id: \&quot;( RsPeerId)peer to get the version string of \&quot;  | [optional] 

### Return type

[**RespRsGossipDiscoveryGetPeerVersion**](RespRsGossipDiscoveryGetPeerVersion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success false otherwise  [out] version(string)version string sent by the peer   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gossip_discovery_get_waiting_disc_count**
> RespRsGossipDiscoveryGetWaitingDiscCount rs_gossip_discovery_get_waiting_disc_count()

getWaitingDiscCount get the number of queued discovery packets.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getWaitingDiscCount get the number of queued discovery packets.
    api_response = api_instance.rs_gossip_discovery_get_waiting_disc_count()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gossip_discovery_get_waiting_disc_count: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsGossipDiscoveryGetWaitingDiscCount**](RespRsGossipDiscoveryGetWaitingDiscCount.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success false otherwise  [out] sendCount(integer)number of queued outgoing packets  [out] recvCount(integer)number of queued incoming packets   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gossip_discovery_request_invite**
> RespRsGossipDiscoveryRequestInvite rs_gossip_discovery_request_invite(req_rs_gossip_discovery_request_invite=req_rs_gossip_discovery_request_invite)

Request RetroShare certificate to given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gossip_discovery_request_invite = {"inviteId":"( RsPeerId)id of the peer of which request the invite ","toSslId":"( RsPeerId)id of the destination of the request "} # ReqRsGossipDiscoveryRequestInvite | inviteId: \"( RsPeerId)id of the peer of which request the invite \"         toSslId: \"( RsPeerId)id of the destination of the request \"  (optional)

try:
    # Request RetroShare certificate to given peer
    api_response = api_instance.rs_gossip_discovery_request_invite(req_rs_gossip_discovery_request_invite=req_rs_gossip_discovery_request_invite)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gossip_discovery_request_invite: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gossip_discovery_request_invite** | [**ReqRsGossipDiscoveryRequestInvite**](ReqRsGossipDiscoveryRequestInvite.md)| inviteId: \&quot;( RsPeerId)id of the peer of which request the invite \&quot;         toSslId: \&quot;( RsPeerId)id of the destination of the request \&quot;  | [optional] 

### Return type

[**RespRsGossipDiscoveryRequestInvite**](RespRsGossipDiscoveryRequestInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success false otherwise  [out] errorMessage(string)Optional storage for the error message, meaningful only on failure.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gossip_discovery_send_invite**
> RespRsGossipDiscoverySendInvite rs_gossip_discovery_send_invite(req_rs_gossip_discovery_send_invite=req_rs_gossip_discovery_send_invite)

Send RetroShare invite to given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gossip_discovery_send_invite = {"inviteId":"( RsPeerId)id of peer of which send the invite ","toSslId":"( RsPeerId)ssl id of the destination peer "} # ReqRsGossipDiscoverySendInvite | inviteId: \"( RsPeerId)id of peer of which send the invite \"         toSslId: \"( RsPeerId)ssl id of the destination peer \"  (optional)

try:
    # Send RetroShare invite to given peer
    api_response = api_instance.rs_gossip_discovery_send_invite(req_rs_gossip_discovery_send_invite=req_rs_gossip_discovery_send_invite)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gossip_discovery_send_invite: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gossip_discovery_send_invite** | [**ReqRsGossipDiscoverySendInvite**](ReqRsGossipDiscoverySendInvite.md)| inviteId: \&quot;( RsPeerId)id of peer of which send the invite \&quot;         toSslId: \&quot;( RsPeerId)ssl id of the destination peer \&quot;  | [optional] 

### Return type

[**RespRsGossipDiscoverySendInvite**](RespRsGossipDiscoverySendInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success false otherwise  [out] errorMessage(string)Optional storage for the error message, meaningful only on failure.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_create_channel**
> RespRsGxsChannelsCreateChannel rs_gxs_channels_create_channel(req_rs_gxs_channels_create_channel=req_rs_gxs_channels_create_channel)

Deprecated{ substituted by createChannelV2 }

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_create_channel = {"channel":"( RsGxsChannelGroup)Channel data (name, description...) "} # ReqRsGxsChannelsCreateChannel | channel: \"( RsGxsChannelGroup)Channel data (name, description...) \"  (optional)

try:
    # Deprecated{ substituted by createChannelV2 }
    api_response = api_instance.rs_gxs_channels_create_channel(req_rs_gxs_channels_create_channel=req_rs_gxs_channels_create_channel)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_create_channel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_create_channel** | [**ReqRsGxsChannelsCreateChannel**](ReqRsGxsChannelsCreateChannel.md)| channel: \&quot;( RsGxsChannelGroup)Channel data (name, description...) \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsCreateChannel**](RespRsGxsChannelsCreateChannel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_create_channel_v2**
> RespRsGxsChannelsCreateChannelV2 rs_gxs_channels_create_channel_v2(req_rs_gxs_channels_create_channel_v2=req_rs_gxs_channels_create_channel_v2)

Create channel. Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_create_channel_v2 = {"name":"( string)Name of the channel ","description":"( string)Description of the channel ","thumbnail":"( RsGxsImage)Optional image to show as channel thumbnail. ","authorId":"( RsGxsId)Optional id of the author. Leave empty for an anonymous channel. ","circleType":"( RsGxsCircleType)Optional visibility rule, default public. ","circleId":"( RsGxsCircleId)If the channel is not public specify the id of the circle who can see the channel. Depending on the value you pass for circleType this should be be an external circle if EXTERNAL is passed, a local friend group id if NODES_GROUP is passed, empty otherwise. "} # ReqRsGxsChannelsCreateChannelV2 | name: \"( string)Name of the channel \"         description: \"( string)Description of the channel \"         thumbnail: \"( RsGxsImage)Optional image to show as channel thumbnail. \"         authorId: \"( RsGxsId)Optional id of the author. Leave empty for an anonymous channel. \"         circleType: \"( RsGxsCircleType)Optional visibility rule, default public. \"         circleId: \"( RsGxsCircleId)If the channel is not public specify the id of the circle who can see the channel. Depending on the value you pass for circleType this should be be an external circle if EXTERNAL is passed, a local friend group id if NODES_GROUP is passed, empty otherwise. \"  (optional)

try:
    # Create channel. Blocking API.
    api_response = api_instance.rs_gxs_channels_create_channel_v2(req_rs_gxs_channels_create_channel_v2=req_rs_gxs_channels_create_channel_v2)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_create_channel_v2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_create_channel_v2** | [**ReqRsGxsChannelsCreateChannelV2**](ReqRsGxsChannelsCreateChannelV2.md)| name: \&quot;( string)Name of the channel \&quot;         description: \&quot;( string)Description of the channel \&quot;         thumbnail: \&quot;( RsGxsImage)Optional image to show as channel thumbnail. \&quot;         authorId: \&quot;( RsGxsId)Optional id of the author. Leave empty for an anonymous channel. \&quot;         circleType: \&quot;( RsGxsCircleType)Optional visibility rule, default public. \&quot;         circleId: \&quot;( RsGxsCircleId)If the channel is not public specify the id of the circle who can see the channel. Depending on the value you pass for circleType this should be be an external circle if EXTERNAL is passed, a local friend group id if NODES_GROUP is passed, empty otherwise. \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsCreateChannelV2**](RespRsGxsChannelsCreateChannelV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: False on error, true otherwise.  [out] channelId(RsGxsGroupId)Optional storage for the id of the created channel, meaningful only if creations succeeds.  [out] errorMessage(string)Optional storage for error messsage, meaningful only if creation fail.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_create_comment**
> RespRsGxsChannelsCreateComment rs_gxs_channels_create_comment(req_rs_gxs_channels_create_comment=req_rs_gxs_channels_create_comment)

Deprecated

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_create_comment = {"comment":"( RsGxsComment)None"} # ReqRsGxsChannelsCreateComment | comment: \"( RsGxsComment)None\"  (optional)

try:
    # Deprecated
    api_response = api_instance.rs_gxs_channels_create_comment(req_rs_gxs_channels_create_comment=req_rs_gxs_channels_create_comment)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_create_comment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_create_comment** | [**ReqRsGxsChannelsCreateComment**](ReqRsGxsChannelsCreateComment.md)| comment: \&quot;( RsGxsComment)None\&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsCreateComment**](RespRsGxsChannelsCreateComment.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_create_comment_v2**
> RespRsGxsChannelsCreateCommentV2 rs_gxs_channels_create_comment_v2(req_rs_gxs_channels_create_comment_v2=req_rs_gxs_channels_create_comment_v2)

Add a comment on a post or on another comment. Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_create_comment_v2 = {"channelId":"( RsGxsGroupId)Id of the channel in which the comment is to be posted ","threadId":"( RsGxsMessageId)Id of the post (that is a thread) in the channel where the comment is placed ","comment":"( string)UTF-8 string containing the comment itself ","authorId":"( RsGxsId)Id of the author of the comment ","parentId":"( RsGxsMessageId)Id of the parent of the comment that is either a channel post Id or the Id of another comment. ","origCommentId":"( RsGxsMessageId)If this is supposed to replace an already existent comment, the id of the old post. If left blank a new post will be created. "} # ReqRsGxsChannelsCreateCommentV2 | channelId: \"( RsGxsGroupId)Id of the channel in which the comment is to be posted \"         threadId: \"( RsGxsMessageId)Id of the post (that is a thread) in the channel where the comment is placed \"         comment: \"( string)UTF-8 string containing the comment itself \"         authorId: \"( RsGxsId)Id of the author of the comment \"         parentId: \"( RsGxsMessageId)Id of the parent of the comment that is either a channel post Id or the Id of another comment. \"         origCommentId: \"( RsGxsMessageId)If this is supposed to replace an already existent comment, the id of the old post. If left blank a new post will be created. \"  (optional)

try:
    # Add a comment on a post or on another comment. Blocking API.
    api_response = api_instance.rs_gxs_channels_create_comment_v2(req_rs_gxs_channels_create_comment_v2=req_rs_gxs_channels_create_comment_v2)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_create_comment_v2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_create_comment_v2** | [**ReqRsGxsChannelsCreateCommentV2**](ReqRsGxsChannelsCreateCommentV2.md)| channelId: \&quot;( RsGxsGroupId)Id of the channel in which the comment is to be posted \&quot;         threadId: \&quot;( RsGxsMessageId)Id of the post (that is a thread) in the channel where the comment is placed \&quot;         comment: \&quot;( string)UTF-8 string containing the comment itself \&quot;         authorId: \&quot;( RsGxsId)Id of the author of the comment \&quot;         parentId: \&quot;( RsGxsMessageId)Id of the parent of the comment that is either a channel post Id or the Id of another comment. \&quot;         origCommentId: \&quot;( RsGxsMessageId)If this is supposed to replace an already existent comment, the id of the old post. If left blank a new post will be created. \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsCreateCommentV2**](RespRsGxsChannelsCreateCommentV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] commentMessageId(RsGxsMessageId)Optional storage for the id of the comment that was created, meaningful only on success.  [out] errorMessage(string)Optional storage for error message, meaningful only on failure.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_create_post**
> RespRsGxsChannelsCreatePost rs_gxs_channels_create_post(req_rs_gxs_channels_create_post=req_rs_gxs_channels_create_post)

Deprecated

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_create_post = {"post":"( RsGxsChannelPost)None"} # ReqRsGxsChannelsCreatePost | post: \"( RsGxsChannelPost)None\"  (optional)

try:
    # Deprecated
    api_response = api_instance.rs_gxs_channels_create_post(req_rs_gxs_channels_create_post=req_rs_gxs_channels_create_post)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_create_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_create_post** | [**ReqRsGxsChannelsCreatePost**](ReqRsGxsChannelsCreatePost.md)| post: \&quot;( RsGxsChannelPost)None\&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsCreatePost**](RespRsGxsChannelsCreatePost.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_create_post_v2**
> RespRsGxsChannelsCreatePostV2 rs_gxs_channels_create_post_v2(req_rs_gxs_channels_create_post_v2=req_rs_gxs_channels_create_post_v2)

Create channel post. Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_create_post_v2 = {"channelId":"( RsGxsGroupId)Id of the channel where to put the post. Beware you need publish rights on that channel to post. ","title":"( string)Title of the post ","mBody":"( string)Text content of the post ","files":"( list<RsGxsFile>)Optional list of attached files. These are supposed to be already shared, ","thumbnail":"( RsGxsImage)Optional thumbnail image for the post. ","origPostId":"( RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created. "} # ReqRsGxsChannelsCreatePostV2 | channelId: \"( RsGxsGroupId)Id of the channel where to put the post. Beware you need publish rights on that channel to post. \"         title: \"( string)Title of the post \"         mBody: \"( string)Text content of the post \"         files: \"( list<RsGxsFile>)Optional list of attached files. These are supposed to be already shared, \"         thumbnail: \"( RsGxsImage)Optional thumbnail image for the post. \"         origPostId: \"( RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created. \"  (optional)

try:
    # Create channel post. Blocking API.
    api_response = api_instance.rs_gxs_channels_create_post_v2(req_rs_gxs_channels_create_post_v2=req_rs_gxs_channels_create_post_v2)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_create_post_v2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_create_post_v2** | [**ReqRsGxsChannelsCreatePostV2**](ReqRsGxsChannelsCreatePostV2.md)| channelId: \&quot;( RsGxsGroupId)Id of the channel where to put the post. Beware you need publish rights on that channel to post. \&quot;         title: \&quot;( string)Title of the post \&quot;         mBody: \&quot;( string)Text content of the post \&quot;         files: \&quot;( list&lt;RsGxsFile&gt;)Optional list of attached files. These are supposed to be already shared, \&quot;         thumbnail: \&quot;( RsGxsImage)Optional thumbnail image for the post. \&quot;         origPostId: \&quot;( RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created. \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsCreatePostV2**](RespRsGxsChannelsCreatePostV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] postId(RsGxsMessageId)Optional storage for the id of the created post, meaningful only on success.  [out] errorMessage(string)Optional storage for the error message, meaningful only on failure.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_create_vote**
> RespRsGxsChannelsCreateVote rs_gxs_channels_create_vote(req_rs_gxs_channels_create_vote=req_rs_gxs_channels_create_vote)

Deprecated

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_create_vote = {"vote":"( RsGxsVote)None"} # ReqRsGxsChannelsCreateVote | vote: \"( RsGxsVote)None\"  (optional)

try:
    # Deprecated
    api_response = api_instance.rs_gxs_channels_create_vote(req_rs_gxs_channels_create_vote=req_rs_gxs_channels_create_vote)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_create_vote: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_create_vote** | [**ReqRsGxsChannelsCreateVote**](ReqRsGxsChannelsCreateVote.md)| vote: \&quot;( RsGxsVote)None\&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsCreateVote**](RespRsGxsChannelsCreateVote.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_create_vote_v2**
> RespRsGxsChannelsCreateVoteV2 rs_gxs_channels_create_vote_v2(req_rs_gxs_channels_create_vote_v2=req_rs_gxs_channels_create_vote_v2)

Create a vote

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_create_vote_v2 = {"channelId":"( RsGxsGroupId)Id of the channel where to vote ","postId":"( RsGxsMessageId)Id of the channel post of which a comment is voted. ","commentId":"( RsGxsMessageId)Id of the comment that is voted ","authorId":"( RsGxsId)Id of the author. Needs to be of an owned identity. ","vote":"( RsGxsVoteType)Vote value, either "} # ReqRsGxsChannelsCreateVoteV2 | channelId: \"( RsGxsGroupId)Id of the channel where to vote \"         postId: \"( RsGxsMessageId)Id of the channel post of which a comment is voted. \"         commentId: \"( RsGxsMessageId)Id of the comment that is voted \"         authorId: \"( RsGxsId)Id of the author. Needs to be of an owned identity. \"         vote: \"( RsGxsVoteType)Vote value, either \"  (optional)

try:
    # Create a vote
    api_response = api_instance.rs_gxs_channels_create_vote_v2(req_rs_gxs_channels_create_vote_v2=req_rs_gxs_channels_create_vote_v2)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_create_vote_v2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_create_vote_v2** | [**ReqRsGxsChannelsCreateVoteV2**](ReqRsGxsChannelsCreateVoteV2.md)| channelId: \&quot;( RsGxsGroupId)Id of the channel where to vote \&quot;         postId: \&quot;( RsGxsMessageId)Id of the channel post of which a comment is voted. \&quot;         commentId: \&quot;( RsGxsMessageId)Id of the comment that is voted \&quot;         authorId: \&quot;( RsGxsId)Id of the author. Needs to be of an owned identity. \&quot;         vote: \&quot;( RsGxsVoteType)Vote value, either \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsCreateVoteV2**](RespRsGxsChannelsCreateVoteV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] voteId(RsGxsMessageId)Optional storage for the id of the created vote, meaningful only on success.  [out] errorMessage(string)Optional storage for error message, meaningful only on failure.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_edit_channel**
> RespRsGxsChannelsEditChannel rs_gxs_channels_edit_channel(req_rs_gxs_channels_edit_channel=req_rs_gxs_channels_edit_channel)

Edit channel details.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_edit_channel = {"channel":"( RsGxsChannelGroup)Channel data (name, description...) with modifications "} # ReqRsGxsChannelsEditChannel | channel: \"( RsGxsChannelGroup)Channel data (name, description...) with modifications \"  (optional)

try:
    # Edit channel details.
    api_response = api_instance.rs_gxs_channels_edit_channel(req_rs_gxs_channels_edit_channel=req_rs_gxs_channels_edit_channel)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_edit_channel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_edit_channel** | [**ReqRsGxsChannelsEditChannel**](ReqRsGxsChannelsEditChannel.md)| channel: \&quot;( RsGxsChannelGroup)Channel data (name, description...) with modifications \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsEditChannel**](RespRsGxsChannelsEditChannel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_extra_file_hash**
> RespRsGxsChannelsExtraFileHash rs_gxs_channels_extra_file_hash(req_rs_gxs_channels_extra_file_hash=req_rs_gxs_channels_extra_file_hash)

Share extra file Can be used to share extra file attached to a channel post

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_extra_file_hash = {"path":"( string)file path "} # ReqRsGxsChannelsExtraFileHash | path: \"( string)file path \"  (optional)

try:
    # Share extra file Can be used to share extra file attached to a channel post
    api_response = api_instance.rs_gxs_channels_extra_file_hash(req_rs_gxs_channels_extra_file_hash=req_rs_gxs_channels_extra_file_hash)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_extra_file_hash: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_extra_file_hash** | [**ReqRsGxsChannelsExtraFileHash**](ReqRsGxsChannelsExtraFileHash.md)| path: \&quot;( string)file path \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsExtraFileHash**](RespRsGxsChannelsExtraFileHash.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_extra_file_remove**
> RespRsGxsChannelsExtraFileRemove rs_gxs_channels_extra_file_remove(req_rs_gxs_channels_extra_file_remove=req_rs_gxs_channels_extra_file_remove)

Remove extra file from shared files

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_extra_file_remove = {"hash":"( RsFileHash)hash of the file to remove "} # ReqRsGxsChannelsExtraFileRemove | hash: \"( RsFileHash)hash of the file to remove \"  (optional)

try:
    # Remove extra file from shared files
    api_response = api_instance.rs_gxs_channels_extra_file_remove(req_rs_gxs_channels_extra_file_remove=req_rs_gxs_channels_extra_file_remove)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_extra_file_remove: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_extra_file_remove** | [**ReqRsGxsChannelsExtraFileRemove**](ReqRsGxsChannelsExtraFileRemove.md)| hash: \&quot;( RsFileHash)hash of the file to remove \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsExtraFileRemove**](RespRsGxsChannelsExtraFileRemove.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_get_channel_auto_download**
> RespRsGxsChannelsGetChannelAutoDownload rs_gxs_channels_get_channel_auto_download(req_rs_gxs_channels_get_channel_auto_download=req_rs_gxs_channels_get_channel_auto_download)

Get auto-download option value for given channel

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_get_channel_auto_download = {"channelId":"( RsGxsGroupId)channel id "} # ReqRsGxsChannelsGetChannelAutoDownload | channelId: \"( RsGxsGroupId)channel id \"  (optional)

try:
    # Get auto-download option value for given channel
    api_response = api_instance.rs_gxs_channels_get_channel_auto_download(req_rs_gxs_channels_get_channel_auto_download=req_rs_gxs_channels_get_channel_auto_download)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_get_channel_auto_download: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_get_channel_auto_download** | [**ReqRsGxsChannelsGetChannelAutoDownload**](ReqRsGxsChannelsGetChannelAutoDownload.md)| channelId: \&quot;( RsGxsGroupId)channel id \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsGetChannelAutoDownload**](RespRsGxsChannelsGetChannelAutoDownload.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] enabled(boolean)storage for the auto-download option value   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_get_channel_content**
> RespRsGxsChannelsGetChannelContent rs_gxs_channels_get_channel_content(req_rs_gxs_channels_get_channel_content=req_rs_gxs_channels_get_channel_content)

Get channel contents

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_get_channel_content = {"channelId":"( RsGxsGroupId)id of the channel of which the content is requested ","contentsIds":"( set<RsGxsMessageId>)ids of requested contents "} # ReqRsGxsChannelsGetChannelContent | channelId: \"( RsGxsGroupId)id of the channel of which the content is requested \"         contentsIds: \"( set<RsGxsMessageId>)ids of requested contents \"  (optional)

try:
    # Get channel contents
    api_response = api_instance.rs_gxs_channels_get_channel_content(req_rs_gxs_channels_get_channel_content=req_rs_gxs_channels_get_channel_content)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_get_channel_content: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_get_channel_content** | [**ReqRsGxsChannelsGetChannelContent**](ReqRsGxsChannelsGetChannelContent.md)| channelId: \&quot;( RsGxsGroupId)id of the channel of which the content is requested \&quot;         contentsIds: \&quot;( set&lt;RsGxsMessageId&gt;)ids of requested contents \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsGetChannelContent**](RespRsGxsChannelsGetChannelContent.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] posts(vector&lt;RsGxsChannelPost&gt;)storage for posts  [out] comments(vector&lt;RsGxsComment&gt;)storage for the comments   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_get_channel_download_directory**
> RespRsGxsChannelsGetChannelDownloadDirectory rs_gxs_channels_get_channel_download_directory(req_rs_gxs_channels_get_channel_download_directory=req_rs_gxs_channels_get_channel_download_directory)

Get download directory for the given channel

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_get_channel_download_directory = {"channelId":"( RsGxsGroupId)id of the channel "} # ReqRsGxsChannelsGetChannelDownloadDirectory | channelId: \"( RsGxsGroupId)id of the channel \"  (optional)

try:
    # Get download directory for the given channel
    api_response = api_instance.rs_gxs_channels_get_channel_download_directory(req_rs_gxs_channels_get_channel_download_directory=req_rs_gxs_channels_get_channel_download_directory)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_get_channel_download_directory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_get_channel_download_directory** | [**ReqRsGxsChannelsGetChannelDownloadDirectory**](ReqRsGxsChannelsGetChannelDownloadDirectory.md)| channelId: \&quot;( RsGxsGroupId)id of the channel \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsGetChannelDownloadDirectory**](RespRsGxsChannelsGetChannelDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] directory(string)reference to string where to store the path   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_get_channels_info**
> RespRsGxsChannelsGetChannelsInfo rs_gxs_channels_get_channels_info(req_rs_gxs_channels_get_channels_info=req_rs_gxs_channels_get_channels_info)

Get channels information (description, thumbnail...). Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_get_channels_info = {"chanIds":"( list<RsGxsGroupId>)ids of the channels of which to get the informations "} # ReqRsGxsChannelsGetChannelsInfo | chanIds: \"( list<RsGxsGroupId>)ids of the channels of which to get the informations \"  (optional)

try:
    # Get channels information (description, thumbnail...). Blocking API.
    api_response = api_instance.rs_gxs_channels_get_channels_info(req_rs_gxs_channels_get_channels_info=req_rs_gxs_channels_get_channels_info)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_get_channels_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_get_channels_info** | [**ReqRsGxsChannelsGetChannelsInfo**](ReqRsGxsChannelsGetChannelsInfo.md)| chanIds: \&quot;( list&lt;RsGxsGroupId&gt;)ids of the channels of which to get the informations \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsGetChannelsInfo**](RespRsGxsChannelsGetChannelsInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] channelsInfo(vector&lt;RsGxsChannelGroup&gt;)storage for the channels informations   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_get_channels_summaries**
> RespRsGxsChannelsGetChannelsSummaries rs_gxs_channels_get_channels_summaries()

Get channels summaries list. Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get channels summaries list. Blocking API.
    api_response = api_instance.rs_gxs_channels_get_channels_summaries()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_get_channels_summaries: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsGxsChannelsGetChannelsSummaries**](RespRsGxsChannelsGetChannelsSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] channels(list&lt;RsGroupMetaData&gt;)list where to store the channels   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_get_content_summaries**
> RespRsGxsChannelsGetContentSummaries rs_gxs_channels_get_content_summaries(req_rs_gxs_channels_get_content_summaries=req_rs_gxs_channels_get_content_summaries)

Get channel content summaries

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_get_content_summaries = {"channelId":"( RsGxsGroupId)id of the channel of which the content is requested "} # ReqRsGxsChannelsGetContentSummaries | channelId: \"( RsGxsGroupId)id of the channel of which the content is requested \"  (optional)

try:
    # Get channel content summaries
    api_response = api_instance.rs_gxs_channels_get_content_summaries(req_rs_gxs_channels_get_content_summaries=req_rs_gxs_channels_get_content_summaries)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_get_content_summaries: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_get_content_summaries** | [**ReqRsGxsChannelsGetContentSummaries**](ReqRsGxsChannelsGetContentSummaries.md)| channelId: \&quot;( RsGxsGroupId)id of the channel of which the content is requested \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsGetContentSummaries**](RespRsGxsChannelsGetContentSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] summaries(vector&lt;RsMsgMetaData&gt;)storage for summaries   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_local_search_request**
> RespRsGxsChannelsLocalSearchRequest rs_gxs_channels_local_search_request(req_rs_gxs_channels_local_search_request=req_rs_gxs_channels_local_search_request)

This method is asynchronous. Search local channels

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_local_search_request = {"matchString":"( string)string to look for in the search ","maxWait":"( rstime_t)maximum wait time in seconds for search results "} # ReqRsGxsChannelsLocalSearchRequest | matchString: \"( string)string to look for in the search \"         maxWait: \"( rstime_t)maximum wait time in seconds for search results \"  (optional)

try:
    # This method is asynchronous. Search local channels
    api_response = api_instance.rs_gxs_channels_local_search_request(req_rs_gxs_channels_local_search_request=req_rs_gxs_channels_local_search_request)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_local_search_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_local_search_request** | [**ReqRsGxsChannelsLocalSearchRequest**](ReqRsGxsChannelsLocalSearchRequest.md)| matchString: \&quot;( string)string to look for in the search \&quot;         maxWait: \&quot;( rstime_t)maximum wait time in seconds for search results \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsLocalSearchRequest**](RespRsGxsChannelsLocalSearchRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_mark_read**
> RespRsGxsChannelsMarkRead rs_gxs_channels_mark_read(req_rs_gxs_channels_mark_read=req_rs_gxs_channels_mark_read)

Toggle post read status. Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_mark_read = {"postId":"( RsGxsGrpMsgIdPair)post identifier ","read":"( boolean)true to mark as read, false to mark as unread "} # ReqRsGxsChannelsMarkRead | postId: \"( RsGxsGrpMsgIdPair)post identifier \"         read: \"( boolean)true to mark as read, false to mark as unread \"  (optional)

try:
    # Toggle post read status. Blocking API.
    api_response = api_instance.rs_gxs_channels_mark_read(req_rs_gxs_channels_mark_read=req_rs_gxs_channels_mark_read)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_mark_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_mark_read** | [**ReqRsGxsChannelsMarkRead**](ReqRsGxsChannelsMarkRead.md)| postId: \&quot;( RsGxsGrpMsgIdPair)post identifier \&quot;         read: \&quot;( boolean)true to mark as read, false to mark as unread \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsMarkRead**](RespRsGxsChannelsMarkRead.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_request_status**
> RespRsGxsChannelsRequestStatus rs_gxs_channels_request_status(req_rs_gxs_channels_request_status=req_rs_gxs_channels_request_status)

null

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_request_status = {"token":"( integer)None"} # ReqRsGxsChannelsRequestStatus | token: \"( integer)None\"  (optional)

try:
    # null
    api_response = api_instance.rs_gxs_channels_request_status(req_rs_gxs_channels_request_status=req_rs_gxs_channels_request_status)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_request_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_request_status** | [**ReqRsGxsChannelsRequestStatus**](ReqRsGxsChannelsRequestStatus.md)| token: \&quot;( integer)None\&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsRequestStatus**](RespRsGxsChannelsRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_set_channel_auto_download**
> RespRsGxsChannelsSetChannelAutoDownload rs_gxs_channels_set_channel_auto_download(req_rs_gxs_channels_set_channel_auto_download=req_rs_gxs_channels_set_channel_auto_download)

Enable or disable auto-download for given channel. Blocking API

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_set_channel_auto_download = {"channelId":"( RsGxsGroupId)channel id ","enable":"( boolean)true to enable, false to disable "} # ReqRsGxsChannelsSetChannelAutoDownload | channelId: \"( RsGxsGroupId)channel id \"         enable: \"( boolean)true to enable, false to disable \"  (optional)

try:
    # Enable or disable auto-download for given channel. Blocking API
    api_response = api_instance.rs_gxs_channels_set_channel_auto_download(req_rs_gxs_channels_set_channel_auto_download=req_rs_gxs_channels_set_channel_auto_download)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_set_channel_auto_download: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_set_channel_auto_download** | [**ReqRsGxsChannelsSetChannelAutoDownload**](ReqRsGxsChannelsSetChannelAutoDownload.md)| channelId: \&quot;( RsGxsGroupId)channel id \&quot;         enable: \&quot;( boolean)true to enable, false to disable \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsSetChannelAutoDownload**](RespRsGxsChannelsSetChannelAutoDownload.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_set_channel_download_directory**
> RespRsGxsChannelsSetChannelDownloadDirectory rs_gxs_channels_set_channel_download_directory(req_rs_gxs_channels_set_channel_download_directory=req_rs_gxs_channels_set_channel_download_directory)

Set download directory for the given channel. Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_set_channel_download_directory = {"channelId":"( RsGxsGroupId)id of the channel ","directory":"( string)path "} # ReqRsGxsChannelsSetChannelDownloadDirectory | channelId: \"( RsGxsGroupId)id of the channel \"         directory: \"( string)path \"  (optional)

try:
    # Set download directory for the given channel. Blocking API.
    api_response = api_instance.rs_gxs_channels_set_channel_download_directory(req_rs_gxs_channels_set_channel_download_directory=req_rs_gxs_channels_set_channel_download_directory)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_set_channel_download_directory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_set_channel_download_directory** | [**ReqRsGxsChannelsSetChannelDownloadDirectory**](ReqRsGxsChannelsSetChannelDownloadDirectory.md)| channelId: \&quot;( RsGxsGroupId)id of the channel \&quot;         directory: \&quot;( string)path \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsSetChannelDownloadDirectory**](RespRsGxsChannelsSetChannelDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_share_channel_keys**
> RespRsGxsChannelsShareChannelKeys rs_gxs_channels_share_channel_keys(req_rs_gxs_channels_share_channel_keys=req_rs_gxs_channels_share_channel_keys)

Share channel publishing key This can be used to authorize other peers to post on the channel

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_share_channel_keys = {"channelId":"( RsGxsGroupId)id of the channel ","peers":"( set<RsPeerId>)peers to share the key with "} # ReqRsGxsChannelsShareChannelKeys | channelId: \"( RsGxsGroupId)id of the channel \"         peers: \"( set<RsPeerId>)peers to share the key with \"  (optional)

try:
    # Share channel publishing key This can be used to authorize other peers to post on the channel
    api_response = api_instance.rs_gxs_channels_share_channel_keys(req_rs_gxs_channels_share_channel_keys=req_rs_gxs_channels_share_channel_keys)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_share_channel_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_share_channel_keys** | [**ReqRsGxsChannelsShareChannelKeys**](ReqRsGxsChannelsShareChannelKeys.md)| channelId: \&quot;( RsGxsGroupId)id of the channel \&quot;         peers: \&quot;( set&lt;RsPeerId&gt;)peers to share the key with \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsShareChannelKeys**](RespRsGxsChannelsShareChannelKeys.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_subscribe_to_channel**
> RespRsGxsChannelsSubscribeToChannel rs_gxs_channels_subscribe_to_channel(req_rs_gxs_channels_subscribe_to_channel=req_rs_gxs_channels_subscribe_to_channel)

Subscrbe to a channel. Blocking API

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_subscribe_to_channel = {"channelId":"( RsGxsGroupId)Channel id ","subscribe":"( boolean)true to subscribe, false to unsubscribe "} # ReqRsGxsChannelsSubscribeToChannel | channelId: \"( RsGxsGroupId)Channel id \"         subscribe: \"( boolean)true to subscribe, false to unsubscribe \"  (optional)

try:
    # Subscrbe to a channel. Blocking API
    api_response = api_instance.rs_gxs_channels_subscribe_to_channel(req_rs_gxs_channels_subscribe_to_channel=req_rs_gxs_channels_subscribe_to_channel)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_subscribe_to_channel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_subscribe_to_channel** | [**ReqRsGxsChannelsSubscribeToChannel**](ReqRsGxsChannelsSubscribeToChannel.md)| channelId: \&quot;( RsGxsGroupId)Channel id \&quot;         subscribe: \&quot;( boolean)true to subscribe, false to unsubscribe \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsSubscribeToChannel**](RespRsGxsChannelsSubscribeToChannel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_turtle_channel_request**
> RespRsGxsChannelsTurtleChannelRequest rs_gxs_channels_turtle_channel_request(req_rs_gxs_channels_turtle_channel_request=req_rs_gxs_channels_turtle_channel_request)

This method is asynchronous. Request remote channel

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_turtle_channel_request = {"channelId":"( RsGxsGroupId)id of the channel to request to distants peers ","maxWait":"( rstime_t)maximum wait time in seconds for search results "} # ReqRsGxsChannelsTurtleChannelRequest | channelId: \"( RsGxsGroupId)id of the channel to request to distants peers \"         maxWait: \"( rstime_t)maximum wait time in seconds for search results \"  (optional)

try:
    # This method is asynchronous. Request remote channel
    api_response = api_instance.rs_gxs_channels_turtle_channel_request(req_rs_gxs_channels_turtle_channel_request=req_rs_gxs_channels_turtle_channel_request)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_turtle_channel_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_turtle_channel_request** | [**ReqRsGxsChannelsTurtleChannelRequest**](ReqRsGxsChannelsTurtleChannelRequest.md)| channelId: \&quot;( RsGxsGroupId)id of the channel to request to distants peers \&quot;         maxWait: \&quot;( rstime_t)maximum wait time in seconds for search results \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsTurtleChannelRequest**](RespRsGxsChannelsTurtleChannelRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_channels_turtle_search_request**
> RespRsGxsChannelsTurtleSearchRequest rs_gxs_channels_turtle_search_request(req_rs_gxs_channels_turtle_search_request=req_rs_gxs_channels_turtle_search_request)

This method is asynchronous. Request remote channels search

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_channels_turtle_search_request = {"matchString":"( string)string to look for in the search ","maxWait":"( rstime_t)maximum wait time in seconds for search results "} # ReqRsGxsChannelsTurtleSearchRequest | matchString: \"( string)string to look for in the search \"         maxWait: \"( rstime_t)maximum wait time in seconds for search results \"  (optional)

try:
    # This method is asynchronous. Request remote channels search
    api_response = api_instance.rs_gxs_channels_turtle_search_request(req_rs_gxs_channels_turtle_search_request=req_rs_gxs_channels_turtle_search_request)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_channels_turtle_search_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_channels_turtle_search_request** | [**ReqRsGxsChannelsTurtleSearchRequest**](ReqRsGxsChannelsTurtleSearchRequest.md)| matchString: \&quot;( string)string to look for in the search \&quot;         maxWait: \&quot;( rstime_t)maximum wait time in seconds for search results \&quot;  | [optional] 

### Return type

[**RespRsGxsChannelsTurtleSearchRequest**](RespRsGxsChannelsTurtleSearchRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_cancel_circle_membership**
> RespRsGxsCirclesCancelCircleMembership rs_gxs_circles_cancel_circle_membership(req_rs_gxs_circles_cancel_circle_membership=req_rs_gxs_circles_cancel_circle_membership)

Leave given circle

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_circles_cancel_circle_membership = {"ownGxsId":"( RsGxsId)Own id to remove from the circle ","circleId":"( RsGxsCircleId)Id of the circle to leave "} # ReqRsGxsCirclesCancelCircleMembership | ownGxsId: \"( RsGxsId)Own id to remove from the circle \"         circleId: \"( RsGxsCircleId)Id of the circle to leave \"  (optional)

try:
    # Leave given circle
    api_response = api_instance.rs_gxs_circles_cancel_circle_membership(req_rs_gxs_circles_cancel_circle_membership=req_rs_gxs_circles_cancel_circle_membership)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_cancel_circle_membership: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_circles_cancel_circle_membership** | [**ReqRsGxsCirclesCancelCircleMembership**](ReqRsGxsCirclesCancelCircleMembership.md)| ownGxsId: \&quot;( RsGxsId)Own id to remove from the circle \&quot;         circleId: \&quot;( RsGxsCircleId)Id of the circle to leave \&quot;  | [optional] 

### Return type

[**RespRsGxsCirclesCancelCircleMembership**](RespRsGxsCirclesCancelCircleMembership.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_create_circle**
> RespRsGxsCirclesCreateCircle rs_gxs_circles_create_circle(req_rs_gxs_circles_create_circle=req_rs_gxs_circles_create_circle)

Create new circle

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_circles_create_circle = {"cData":"( RsGxsCircleGroup)input name and flags of the circle, storage for generated circle data id etc. "} # ReqRsGxsCirclesCreateCircle | cData: \"( RsGxsCircleGroup)input name and flags of the circle, storage for generated circle data id etc. \"  (optional)

try:
    # Create new circle
    api_response = api_instance.rs_gxs_circles_create_circle(req_rs_gxs_circles_create_circle=req_rs_gxs_circles_create_circle)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_create_circle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_circles_create_circle** | [**ReqRsGxsCirclesCreateCircle**](ReqRsGxsCirclesCreateCircle.md)| cData: \&quot;( RsGxsCircleGroup)input name and flags of the circle, storage for generated circle data id etc. \&quot;  | [optional] 

### Return type

[**RespRsGxsCirclesCreateCircle**](RespRsGxsCirclesCreateCircle.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_edit_circle**
> RespRsGxsCirclesEditCircle rs_gxs_circles_edit_circle(req_rs_gxs_circles_edit_circle=req_rs_gxs_circles_edit_circle)

Edit own existing circle

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_circles_edit_circle = {"cData":"( RsGxsCircleGroup)Circle data with modifications, storage for data updatedad during the operation. "} # ReqRsGxsCirclesEditCircle | cData: \"( RsGxsCircleGroup)Circle data with modifications, storage for data updatedad during the operation. \"  (optional)

try:
    # Edit own existing circle
    api_response = api_instance.rs_gxs_circles_edit_circle(req_rs_gxs_circles_edit_circle=req_rs_gxs_circles_edit_circle)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_edit_circle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_circles_edit_circle** | [**ReqRsGxsCirclesEditCircle**](ReqRsGxsCirclesEditCircle.md)| cData: \&quot;( RsGxsCircleGroup)Circle data with modifications, storage for data updatedad during the operation. \&quot;  | [optional] 

### Return type

[**RespRsGxsCirclesEditCircle**](RespRsGxsCirclesEditCircle.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_get_circle_details**
> RespRsGxsCirclesGetCircleDetails rs_gxs_circles_get_circle_details(req_rs_gxs_circles_get_circle_details=req_rs_gxs_circles_get_circle_details)

Get circle details. Memory cached

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_circles_get_circle_details = {"id":"( RsGxsCircleId)Id of the circle "} # ReqRsGxsCirclesGetCircleDetails | id: \"( RsGxsCircleId)Id of the circle \"  (optional)

try:
    # Get circle details. Memory cached
    api_response = api_instance.rs_gxs_circles_get_circle_details(req_rs_gxs_circles_get_circle_details=req_rs_gxs_circles_get_circle_details)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_get_circle_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_circles_get_circle_details** | [**ReqRsGxsCirclesGetCircleDetails**](ReqRsGxsCirclesGetCircleDetails.md)| id: \&quot;( RsGxsCircleId)Id of the circle \&quot;  | [optional] 

### Return type

[**RespRsGxsCirclesGetCircleDetails**](RespRsGxsCirclesGetCircleDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] details(RsGxsCircleDetails)Storage for the circle details   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_get_circle_external_id_list**
> RespRsGxsCirclesGetCircleExternalIdList rs_gxs_circles_get_circle_external_id_list(req_rs_gxs_circles_get_circle_external_id_list=req_rs_gxs_circles_get_circle_external_id_list)

Get list of known external circles ids. Memory cached

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_circles_get_circle_external_id_list = {"circleIds":"( list<RsGxsCircleId>)Storage for circles id list "} # ReqRsGxsCirclesGetCircleExternalIdList | circleIds: \"( list<RsGxsCircleId>)Storage for circles id list \"  (optional)

try:
    # Get list of known external circles ids. Memory cached
    api_response = api_instance.rs_gxs_circles_get_circle_external_id_list(req_rs_gxs_circles_get_circle_external_id_list=req_rs_gxs_circles_get_circle_external_id_list)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_get_circle_external_id_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_circles_get_circle_external_id_list** | [**ReqRsGxsCirclesGetCircleExternalIdList**](ReqRsGxsCirclesGetCircleExternalIdList.md)| circleIds: \&quot;( list&lt;RsGxsCircleId&gt;)Storage for circles id list \&quot;  | [optional] 

### Return type

[**RespRsGxsCirclesGetCircleExternalIdList**](RespRsGxsCirclesGetCircleExternalIdList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_get_circle_requests**
> RespRsGxsCirclesGetCircleRequests rs_gxs_circles_get_circle_requests(req_rs_gxs_circles_get_circle_requests=req_rs_gxs_circles_get_circle_requests)

Get circle requests

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_circles_get_circle_requests = {"circleId":"( RsGxsGroupId)id of the circle of which the requests are requested "} # ReqRsGxsCirclesGetCircleRequests | circleId: \"( RsGxsGroupId)id of the circle of which the requests are requested \"  (optional)

try:
    # Get circle requests
    api_response = api_instance.rs_gxs_circles_get_circle_requests(req_rs_gxs_circles_get_circle_requests=req_rs_gxs_circles_get_circle_requests)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_get_circle_requests: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_circles_get_circle_requests** | [**ReqRsGxsCirclesGetCircleRequests**](ReqRsGxsCirclesGetCircleRequests.md)| circleId: \&quot;( RsGxsGroupId)id of the circle of which the requests are requested \&quot;  | [optional] 

### Return type

[**RespRsGxsCirclesGetCircleRequests**](RespRsGxsCirclesGetCircleRequests.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] requests(vector&lt;RsGxsCircleMsg&gt;)storage for the circle requests   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_get_circles_info**
> RespRsGxsCirclesGetCirclesInfo rs_gxs_circles_get_circles_info(req_rs_gxs_circles_get_circles_info=req_rs_gxs_circles_get_circles_info)

Get circles information

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_circles_get_circles_info = {"circlesIds":"( list<RsGxsGroupId>)ids of the circles of which to get the informations "} # ReqRsGxsCirclesGetCirclesInfo | circlesIds: \"( list<RsGxsGroupId>)ids of the circles of which to get the informations \"  (optional)

try:
    # Get circles information
    api_response = api_instance.rs_gxs_circles_get_circles_info(req_rs_gxs_circles_get_circles_info=req_rs_gxs_circles_get_circles_info)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_get_circles_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_circles_get_circles_info** | [**ReqRsGxsCirclesGetCirclesInfo**](ReqRsGxsCirclesGetCirclesInfo.md)| circlesIds: \&quot;( list&lt;RsGxsGroupId&gt;)ids of the circles of which to get the informations \&quot;  | [optional] 

### Return type

[**RespRsGxsCirclesGetCirclesInfo**](RespRsGxsCirclesGetCirclesInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] circlesInfo(vector&lt;RsGxsCircleGroup&gt;)storage for the circles informations   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_get_circles_summaries**
> RespRsGxsCirclesGetCirclesSummaries rs_gxs_circles_get_circles_summaries()

Get circles summaries list.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get circles summaries list.
    api_response = api_instance.rs_gxs_circles_get_circles_summaries()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_get_circles_summaries: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsGxsCirclesGetCirclesSummaries**](RespRsGxsCirclesGetCirclesSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] circles(list&lt;RsGroupMetaData&gt;)list where to store the circles summaries   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_invite_ids_to_circle**
> RespRsGxsCirclesInviteIdsToCircle rs_gxs_circles_invite_ids_to_circle(req_rs_gxs_circles_invite_ids_to_circle=req_rs_gxs_circles_invite_ids_to_circle)

Invite identities to circle

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_circles_invite_ids_to_circle = {"identities":"( set<RsGxsId>)ids of the identities to invite ","circleId":"( RsGxsCircleId)Id of the circle you own and want to invite ids in "} # ReqRsGxsCirclesInviteIdsToCircle | identities: \"( set<RsGxsId>)ids of the identities to invite \"         circleId: \"( RsGxsCircleId)Id of the circle you own and want to invite ids in \"  (optional)

try:
    # Invite identities to circle
    api_response = api_instance.rs_gxs_circles_invite_ids_to_circle(req_rs_gxs_circles_invite_ids_to_circle=req_rs_gxs_circles_invite_ids_to_circle)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_invite_ids_to_circle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_circles_invite_ids_to_circle** | [**ReqRsGxsCirclesInviteIdsToCircle**](ReqRsGxsCirclesInviteIdsToCircle.md)| identities: \&quot;( set&lt;RsGxsId&gt;)ids of the identities to invite \&quot;         circleId: \&quot;( RsGxsCircleId)Id of the circle you own and want to invite ids in \&quot;  | [optional] 

### Return type

[**RespRsGxsCirclesInviteIdsToCircle**](RespRsGxsCirclesInviteIdsToCircle.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_request_circle_membership**
> RespRsGxsCirclesRequestCircleMembership rs_gxs_circles_request_circle_membership(req_rs_gxs_circles_request_circle_membership=req_rs_gxs_circles_request_circle_membership)

Request circle membership, or accept circle invitation

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_circles_request_circle_membership = {"ownGxsId":"( RsGxsId)Id of own identity to introduce to the circle ","circleId":"( RsGxsCircleId)Id of the circle to which ask for inclusion "} # ReqRsGxsCirclesRequestCircleMembership | ownGxsId: \"( RsGxsId)Id of own identity to introduce to the circle \"         circleId: \"( RsGxsCircleId)Id of the circle to which ask for inclusion \"  (optional)

try:
    # Request circle membership, or accept circle invitation
    api_response = api_instance.rs_gxs_circles_request_circle_membership(req_rs_gxs_circles_request_circle_membership=req_rs_gxs_circles_request_circle_membership)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_request_circle_membership: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_circles_request_circle_membership** | [**ReqRsGxsCirclesRequestCircleMembership**](ReqRsGxsCirclesRequestCircleMembership.md)| ownGxsId: \&quot;( RsGxsId)Id of own identity to introduce to the circle \&quot;         circleId: \&quot;( RsGxsCircleId)Id of the circle to which ask for inclusion \&quot;  | [optional] 

### Return type

[**RespRsGxsCirclesRequestCircleMembership**](RespRsGxsCirclesRequestCircleMembership.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_circles_request_status**
> RespRsGxsCirclesRequestStatus rs_gxs_circles_request_status(req_rs_gxs_circles_request_status=req_rs_gxs_circles_request_status)

null

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_circles_request_status = {"token":"( integer)None"} # ReqRsGxsCirclesRequestStatus | token: \"( integer)None\"  (optional)

try:
    # null
    api_response = api_instance.rs_gxs_circles_request_status(req_rs_gxs_circles_request_status=req_rs_gxs_circles_request_status)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_circles_request_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_circles_request_status** | [**ReqRsGxsCirclesRequestStatus**](ReqRsGxsCirclesRequestStatus.md)| token: \&quot;( integer)None\&quot;  | [optional] 

### Return type

[**RespRsGxsCirclesRequestStatus**](RespRsGxsCirclesRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_create_forum**
> RespRsGxsForumsCreateForum rs_gxs_forums_create_forum(req_rs_gxs_forums_create_forum=req_rs_gxs_forums_create_forum)

Deprecated

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_create_forum = {"forum":"( RsGxsForumGroup)Forum data (name, description...) "} # ReqRsGxsForumsCreateForum | forum: \"( RsGxsForumGroup)Forum data (name, description...) \"  (optional)

try:
    # Deprecated
    api_response = api_instance.rs_gxs_forums_create_forum(req_rs_gxs_forums_create_forum=req_rs_gxs_forums_create_forum)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_create_forum: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_create_forum** | [**ReqRsGxsForumsCreateForum**](ReqRsGxsForumsCreateForum.md)| forum: \&quot;( RsGxsForumGroup)Forum data (name, description...) \&quot;  | [optional] 

### Return type

[**RespRsGxsForumsCreateForum**](RespRsGxsForumsCreateForum.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_create_forum_v2**
> RespRsGxsForumsCreateForumV2 rs_gxs_forums_create_forum_v2(req_rs_gxs_forums_create_forum_v2=req_rs_gxs_forums_create_forum_v2)

Create forum.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_create_forum_v2 = {"name":"( string)Name of the forum ","description":"( string)Optional description of the forum ","authorId":"( RsGxsId)Optional id of the froum owner author ","moderatorsIds":"( set<RsGxsId>)Optional list of forum moderators ","circleType":"( RsGxsCircleType)Optional visibility rule, default public. ","circleId":"( RsGxsCircleId)If the forum is not public specify the id of the circle who can see the forum. Depending on the value you pass for circleType this should be a circle if EXTERNAL is passed, a local friends group id if NODES_GROUP is passed, empty otherwise. "} # ReqRsGxsForumsCreateForumV2 | name: \"( string)Name of the forum \"         description: \"( string)Optional description of the forum \"         authorId: \"( RsGxsId)Optional id of the froum owner author \"         moderatorsIds: \"( set<RsGxsId>)Optional list of forum moderators \"         circleType: \"( RsGxsCircleType)Optional visibility rule, default public. \"         circleId: \"( RsGxsCircleId)If the forum is not public specify the id of the circle who can see the forum. Depending on the value you pass for circleType this should be a circle if EXTERNAL is passed, a local friends group id if NODES_GROUP is passed, empty otherwise. \"  (optional)

try:
    # Create forum.
    api_response = api_instance.rs_gxs_forums_create_forum_v2(req_rs_gxs_forums_create_forum_v2=req_rs_gxs_forums_create_forum_v2)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_create_forum_v2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_create_forum_v2** | [**ReqRsGxsForumsCreateForumV2**](ReqRsGxsForumsCreateForumV2.md)| name: \&quot;( string)Name of the forum \&quot;         description: \&quot;( string)Optional description of the forum \&quot;         authorId: \&quot;( RsGxsId)Optional id of the froum owner author \&quot;         moderatorsIds: \&quot;( set&lt;RsGxsId&gt;)Optional list of forum moderators \&quot;         circleType: \&quot;( RsGxsCircleType)Optional visibility rule, default public. \&quot;         circleId: \&quot;( RsGxsCircleId)If the forum is not public specify the id of the circle who can see the forum. Depending on the value you pass for circleType this should be a circle if EXTERNAL is passed, a local friends group id if NODES_GROUP is passed, empty otherwise. \&quot;  | [optional] 

### Return type

[**RespRsGxsForumsCreateForumV2**](RespRsGxsForumsCreateForumV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: False on error, true otherwise.  [out] forumId(RsGxsGroupId)Optional storage for the id of the created forum, meaningful only if creations succeeds.  [out] errorMessage(string)Optional storage for error messsage, meaningful only if creation fail.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_create_message**
> RespRsGxsForumsCreateMessage rs_gxs_forums_create_message(req_rs_gxs_forums_create_message=req_rs_gxs_forums_create_message)

Deprecated

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_create_message = {"message":"( RsGxsForumMsg)None"} # ReqRsGxsForumsCreateMessage | message: \"( RsGxsForumMsg)None\"  (optional)

try:
    # Deprecated
    api_response = api_instance.rs_gxs_forums_create_message(req_rs_gxs_forums_create_message=req_rs_gxs_forums_create_message)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_create_message: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_create_message** | [**ReqRsGxsForumsCreateMessage**](ReqRsGxsForumsCreateMessage.md)| message: \&quot;( RsGxsForumMsg)None\&quot;  | [optional] 

### Return type

[**RespRsGxsForumsCreateMessage**](RespRsGxsForumsCreateMessage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_create_post**
> RespRsGxsForumsCreatePost rs_gxs_forums_create_post(req_rs_gxs_forums_create_post=req_rs_gxs_forums_create_post)

Create a post on the given forum.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_create_post = {"forumId":"( RsGxsGroupId)Id of the forum in which the post is to be submitted ","title":"( string)UTF-8 string containing the title of the post ","mBody":"( string)UTF-8 string containing the text of the post ","authorId":"( RsGxsId)Id of the author of the comment ","parentId":"( RsGxsMessageId)Optional Id of the parent post if this post is a reply to another post, empty otherwise. ","origPostId":"( RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created. "} # ReqRsGxsForumsCreatePost | forumId: \"( RsGxsGroupId)Id of the forum in which the post is to be submitted \"         title: \"( string)UTF-8 string containing the title of the post \"         mBody: \"( string)UTF-8 string containing the text of the post \"         authorId: \"( RsGxsId)Id of the author of the comment \"         parentId: \"( RsGxsMessageId)Optional Id of the parent post if this post is a reply to another post, empty otherwise. \"         origPostId: \"( RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created. \"  (optional)

try:
    # Create a post on the given forum.
    api_response = api_instance.rs_gxs_forums_create_post(req_rs_gxs_forums_create_post=req_rs_gxs_forums_create_post)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_create_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_create_post** | [**ReqRsGxsForumsCreatePost**](ReqRsGxsForumsCreatePost.md)| forumId: \&quot;( RsGxsGroupId)Id of the forum in which the post is to be submitted \&quot;         title: \&quot;( string)UTF-8 string containing the title of the post \&quot;         mBody: \&quot;( string)UTF-8 string containing the text of the post \&quot;         authorId: \&quot;( RsGxsId)Id of the author of the comment \&quot;         parentId: \&quot;( RsGxsMessageId)Optional Id of the parent post if this post is a reply to another post, empty otherwise. \&quot;         origPostId: \&quot;( RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created. \&quot;  | [optional] 

### Return type

[**RespRsGxsForumsCreatePost**](RespRsGxsForumsCreatePost.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] postMsgId(RsGxsMessageId)Optional storage for the id of the created, meaningful only on success.  [out] errorMessage(string)Optional storage for error message, meaningful only on failure.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_edit_forum**
> RespRsGxsForumsEditForum rs_gxs_forums_edit_forum(req_rs_gxs_forums_edit_forum=req_rs_gxs_forums_edit_forum)

Edit forum details.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_edit_forum = {"forum":"( RsGxsForumGroup)Forum data (name, description...) with modifications "} # ReqRsGxsForumsEditForum | forum: \"( RsGxsForumGroup)Forum data (name, description...) with modifications \"  (optional)

try:
    # Edit forum details.
    api_response = api_instance.rs_gxs_forums_edit_forum(req_rs_gxs_forums_edit_forum=req_rs_gxs_forums_edit_forum)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_edit_forum: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_edit_forum** | [**ReqRsGxsForumsEditForum**](ReqRsGxsForumsEditForum.md)| forum: \&quot;( RsGxsForumGroup)Forum data (name, description...) with modifications \&quot;  | [optional] 

### Return type

[**RespRsGxsForumsEditForum**](RespRsGxsForumsEditForum.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_get_forum_content**
> RespRsGxsForumsGetForumContent rs_gxs_forums_get_forum_content(req_rs_gxs_forums_get_forum_content=req_rs_gxs_forums_get_forum_content)

Get specific list of messages from a single forums. Blocking API

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_get_forum_content = {"forumId":"( RsGxsGroupId)id of the forum of which the content is requested ","msgsIds":"( set<RsGxsMessageId>)list of message ids to request "} # ReqRsGxsForumsGetForumContent | forumId: \"( RsGxsGroupId)id of the forum of which the content is requested \"         msgsIds: \"( set<RsGxsMessageId>)list of message ids to request \"  (optional)

try:
    # Get specific list of messages from a single forums. Blocking API
    api_response = api_instance.rs_gxs_forums_get_forum_content(req_rs_gxs_forums_get_forum_content=req_rs_gxs_forums_get_forum_content)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_get_forum_content: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_get_forum_content** | [**ReqRsGxsForumsGetForumContent**](ReqRsGxsForumsGetForumContent.md)| forumId: \&quot;( RsGxsGroupId)id of the forum of which the content is requested \&quot;         msgsIds: \&quot;( set&lt;RsGxsMessageId&gt;)list of message ids to request \&quot;  | [optional] 

### Return type

[**RespRsGxsForumsGetForumContent**](RespRsGxsForumsGetForumContent.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] msgs(vector&lt;RsGxsForumMsg&gt;)storage for the forum messages   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_get_forum_msg_meta_data**
> RespRsGxsForumsGetForumMsgMetaData rs_gxs_forums_get_forum_msg_meta_data(req_rs_gxs_forums_get_forum_msg_meta_data=req_rs_gxs_forums_get_forum_msg_meta_data)

Get message metadatas for a specific forum. Blocking API

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_get_forum_msg_meta_data = {"forumId":"( RsGxsGroupId)id of the forum of which the content is requested "} # ReqRsGxsForumsGetForumMsgMetaData | forumId: \"( RsGxsGroupId)id of the forum of which the content is requested \"  (optional)

try:
    # Get message metadatas for a specific forum. Blocking API
    api_response = api_instance.rs_gxs_forums_get_forum_msg_meta_data(req_rs_gxs_forums_get_forum_msg_meta_data=req_rs_gxs_forums_get_forum_msg_meta_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_get_forum_msg_meta_data: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_get_forum_msg_meta_data** | [**ReqRsGxsForumsGetForumMsgMetaData**](ReqRsGxsForumsGetForumMsgMetaData.md)| forumId: \&quot;( RsGxsGroupId)id of the forum of which the content is requested \&quot;  | [optional] 

### Return type

[**RespRsGxsForumsGetForumMsgMetaData**](RespRsGxsForumsGetForumMsgMetaData.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] msgMetas(vector&lt;RsMsgMetaData&gt;)storage for the forum messages meta data   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_get_forums_info**
> RespRsGxsForumsGetForumsInfo rs_gxs_forums_get_forums_info(req_rs_gxs_forums_get_forums_info=req_rs_gxs_forums_get_forums_info)

Get forums information (description, thumbnail...). Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_get_forums_info = {"forumIds":"( list<RsGxsGroupId>)ids of the forums of which to get the informations "} # ReqRsGxsForumsGetForumsInfo | forumIds: \"( list<RsGxsGroupId>)ids of the forums of which to get the informations \"  (optional)

try:
    # Get forums information (description, thumbnail...). Blocking API.
    api_response = api_instance.rs_gxs_forums_get_forums_info(req_rs_gxs_forums_get_forums_info=req_rs_gxs_forums_get_forums_info)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_get_forums_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_get_forums_info** | [**ReqRsGxsForumsGetForumsInfo**](ReqRsGxsForumsGetForumsInfo.md)| forumIds: \&quot;( list&lt;RsGxsGroupId&gt;)ids of the forums of which to get the informations \&quot;  | [optional] 

### Return type

[**RespRsGxsForumsGetForumsInfo**](RespRsGxsForumsGetForumsInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] forumsInfo(vector&lt;RsGxsForumGroup&gt;)storage for the forums informations   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_get_forums_summaries**
> RespRsGxsForumsGetForumsSummaries rs_gxs_forums_get_forums_summaries()

Get forums summaries list. Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get forums summaries list. Blocking API.
    api_response = api_instance.rs_gxs_forums_get_forums_summaries()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_get_forums_summaries: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsGxsForumsGetForumsSummaries**](RespRsGxsForumsGetForumsSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] forums(list&lt;RsGroupMetaData&gt;)list where to store the forums summaries   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_mark_read**
> RespRsGxsForumsMarkRead rs_gxs_forums_mark_read(req_rs_gxs_forums_mark_read=req_rs_gxs_forums_mark_read)

Toggle message read status. Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_mark_read = {"messageId":"( RsGxsGrpMsgIdPair)post identifier ","read":"( boolean)true to mark as read, false to mark as unread "} # ReqRsGxsForumsMarkRead | messageId: \"( RsGxsGrpMsgIdPair)post identifier \"         read: \"( boolean)true to mark as read, false to mark as unread \"  (optional)

try:
    # Toggle message read status. Blocking API.
    api_response = api_instance.rs_gxs_forums_mark_read(req_rs_gxs_forums_mark_read=req_rs_gxs_forums_mark_read)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_mark_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_mark_read** | [**ReqRsGxsForumsMarkRead**](ReqRsGxsForumsMarkRead.md)| messageId: \&quot;( RsGxsGrpMsgIdPair)post identifier \&quot;         read: \&quot;( boolean)true to mark as read, false to mark as unread \&quot;  | [optional] 

### Return type

[**RespRsGxsForumsMarkRead**](RespRsGxsForumsMarkRead.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_request_status**
> RespRsGxsForumsRequestStatus rs_gxs_forums_request_status(req_rs_gxs_forums_request_status=req_rs_gxs_forums_request_status)

null

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_request_status = {"token":"( integer)None"} # ReqRsGxsForumsRequestStatus | token: \"( integer)None\"  (optional)

try:
    # null
    api_response = api_instance.rs_gxs_forums_request_status(req_rs_gxs_forums_request_status=req_rs_gxs_forums_request_status)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_request_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_request_status** | [**ReqRsGxsForumsRequestStatus**](ReqRsGxsForumsRequestStatus.md)| token: \&quot;( integer)None\&quot;  | [optional] 

### Return type

[**RespRsGxsForumsRequestStatus**](RespRsGxsForumsRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_gxs_forums_subscribe_to_forum**
> RespRsGxsForumsSubscribeToForum rs_gxs_forums_subscribe_to_forum(req_rs_gxs_forums_subscribe_to_forum=req_rs_gxs_forums_subscribe_to_forum)

Subscrbe to a forum. Blocking API

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_gxs_forums_subscribe_to_forum = {"forumId":"( RsGxsGroupId)Forum id ","subscribe":"( boolean)true to subscribe, false to unsubscribe "} # ReqRsGxsForumsSubscribeToForum | forumId: \"( RsGxsGroupId)Forum id \"         subscribe: \"( boolean)true to subscribe, false to unsubscribe \"  (optional)

try:
    # Subscrbe to a forum. Blocking API
    api_response = api_instance.rs_gxs_forums_subscribe_to_forum(req_rs_gxs_forums_subscribe_to_forum=req_rs_gxs_forums_subscribe_to_forum)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_gxs_forums_subscribe_to_forum: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_gxs_forums_subscribe_to_forum** | [**ReqRsGxsForumsSubscribeToForum**](ReqRsGxsForumsSubscribeToForum.md)| forumId: \&quot;( RsGxsGroupId)Forum id \&quot;         subscribe: \&quot;( boolean)true to subscribe, false to unsubscribe \&quot;  | [optional] 

### Return type

[**RespRsGxsForumsSubscribeToForum**](RespRsGxsForumsSubscribeToForum.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_auto_add_friend_ids_as_contact**
> RespRsIdentityAutoAddFriendIdsAsContact rs_identity_auto_add_friend_ids_as_contact()

Check if automatic signed by friend identity contact flagging is enabled

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Check if automatic signed by friend identity contact flagging is enabled
    api_response = api_instance.rs_identity_auto_add_friend_ids_as_contact()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_auto_add_friend_ids_as_contact: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsIdentityAutoAddFriendIdsAsContact**](RespRsIdentityAutoAddFriendIdsAsContact.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if enabled, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_create_identity**
> RespRsIdentityCreateIdentity rs_identity_create_identity(req_rs_identity_create_identity=req_rs_identity_create_identity)

Create a new identity

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_create_identity = {"name":"( string)Name of the identity ","avatar":"( RsGxsImage)Image associated to the identity ","pseudonimous":"( boolean)true for unsigned identity, false otherwise ","pgpPassword":"( string)password to unlock PGP to sign identity, not implemented yet "} # ReqRsIdentityCreateIdentity | name: \"( string)Name of the identity \"         avatar: \"( RsGxsImage)Image associated to the identity \"         pseudonimous: \"( boolean)true for unsigned identity, false otherwise \"         pgpPassword: \"( string)password to unlock PGP to sign identity, not implemented yet \"  (optional)

try:
    # Create a new identity
    api_response = api_instance.rs_identity_create_identity(req_rs_identity_create_identity=req_rs_identity_create_identity)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_create_identity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_create_identity** | [**ReqRsIdentityCreateIdentity**](ReqRsIdentityCreateIdentity.md)| name: \&quot;( string)Name of the identity \&quot;         avatar: \&quot;( RsGxsImage)Image associated to the identity \&quot;         pseudonimous: \&quot;( boolean)true for unsigned identity, false otherwise \&quot;         pgpPassword: \&quot;( string)password to unlock PGP to sign identity, not implemented yet \&quot;  | [optional] 

### Return type

[**RespRsIdentityCreateIdentity**](RespRsIdentityCreateIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] id(RsGxsId)storage for the created identity Id   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_delete_banned_nodes_threshold**
> RespRsIdentityDeleteBannedNodesThreshold rs_identity_delete_banned_nodes_threshold()

Get number of days after which delete a banned identities

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get number of days after which delete a banned identities
    api_response = api_instance.rs_identity_delete_banned_nodes_threshold()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_delete_banned_nodes_threshold: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsIdentityDeleteBannedNodesThreshold**](RespRsIdentityDeleteBannedNodesThreshold.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: number of days   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_delete_identity**
> RespRsIdentityDeleteIdentity rs_identity_delete_identity(req_rs_identity_delete_identity=req_rs_identity_delete_identity)

Locally delete given identity

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_delete_identity = {"id":"( RsGxsId)Id of the identity "} # ReqRsIdentityDeleteIdentity | id: \"( RsGxsId)Id of the identity \"  (optional)

try:
    # Locally delete given identity
    api_response = api_instance.rs_identity_delete_identity(req_rs_identity_delete_identity=req_rs_identity_delete_identity)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_delete_identity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_delete_identity** | [**ReqRsIdentityDeleteIdentity**](ReqRsIdentityDeleteIdentity.md)| id: \&quot;( RsGxsId)Id of the identity \&quot;  | [optional] 

### Return type

[**RespRsIdentityDeleteIdentity**](RespRsIdentityDeleteIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_get_id_details**
> RespRsIdentityGetIdDetails rs_identity_get_id_details(req_rs_identity_get_id_details=req_rs_identity_get_id_details)

Get identity details, from the cache

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_get_id_details = {"id":"( RsGxsId)Id of the identity "} # ReqRsIdentityGetIdDetails | id: \"( RsGxsId)Id of the identity \"  (optional)

try:
    # Get identity details, from the cache
    api_response = api_instance.rs_identity_get_id_details(req_rs_identity_get_id_details=req_rs_identity_get_id_details)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_get_id_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_get_id_details** | [**ReqRsIdentityGetIdDetails**](ReqRsIdentityGetIdDetails.md)| id: \&quot;( RsGxsId)Id of the identity \&quot;  | [optional] 

### Return type

[**RespRsIdentityGetIdDetails**](RespRsIdentityGetIdDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] details(RsIdentityDetails)Storage for the identity details   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_get_identities_info**
> RespRsIdentityGetIdentitiesInfo rs_identity_get_identities_info(req_rs_identity_get_identities_info=req_rs_identity_get_identities_info)

Get identities information (name, avatar...). Blocking API.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_get_identities_info = {"ids":"( set<RsGxsId>)ids of the channels of which to get the informations "} # ReqRsIdentityGetIdentitiesInfo | ids: \"( set<RsGxsId>)ids of the channels of which to get the informations \"  (optional)

try:
    # Get identities information (name, avatar...). Blocking API.
    api_response = api_instance.rs_identity_get_identities_info(req_rs_identity_get_identities_info=req_rs_identity_get_identities_info)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_get_identities_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_get_identities_info** | [**ReqRsIdentityGetIdentitiesInfo**](ReqRsIdentityGetIdentitiesInfo.md)| ids: \&quot;( set&lt;RsGxsId&gt;)ids of the channels of which to get the informations \&quot;  | [optional] 

### Return type

[**RespRsIdentityGetIdentitiesInfo**](RespRsIdentityGetIdentitiesInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] idsInfo(vector&lt;RsGxsIdGroup&gt;)storage for the identities informations   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_get_identities_summaries**
> RespRsIdentityGetIdentitiesSummaries rs_identity_get_identities_summaries()

Get identities summaries list.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get identities summaries list.
    api_response = api_instance.rs_identity_get_identities_summaries()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_get_identities_summaries: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsIdentityGetIdentitiesSummaries**](RespRsIdentityGetIdentitiesSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if something failed, true otherwhise  [out] ids(list&lt;RsGroupMetaData&gt;)list where to store the identities   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_get_last_usage_ts**
> RespRsIdentityGetLastUsageTS rs_identity_get_last_usage_ts(req_rs_identity_get_last_usage_ts=req_rs_identity_get_last_usage_ts)

Get last seen usage time of given identity

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_get_last_usage_ts = {"id":"( RsGxsId)Id of the identity "} # ReqRsIdentityGetLastUsageTS | id: \"( RsGxsId)Id of the identity \"  (optional)

try:
    # Get last seen usage time of given identity
    api_response = api_instance.rs_identity_get_last_usage_ts(req_rs_identity_get_last_usage_ts=req_rs_identity_get_last_usage_ts)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_get_last_usage_ts: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_get_last_usage_ts** | [**ReqRsIdentityGetLastUsageTS**](ReqRsIdentityGetLastUsageTS.md)| id: \&quot;( RsGxsId)Id of the identity \&quot;  | [optional] 

### Return type

[**RespRsIdentityGetLastUsageTS**](RespRsIdentityGetLastUsageTS.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: timestamp of last seen usage   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_get_own_pseudonimous_ids**
> RespRsIdentityGetOwnPseudonimousIds rs_identity_get_own_pseudonimous_ids()

Get own pseudonimous (unsigned) ids

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get own pseudonimous (unsigned) ids
    api_response = api_instance.rs_identity_get_own_pseudonimous_ids()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_get_own_pseudonimous_ids: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsIdentityGetOwnPseudonimousIds**](RespRsIdentityGetOwnPseudonimousIds.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] ids(vector&lt;RsGxsId&gt;)storage for the ids   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_get_own_signed_ids**
> RespRsIdentityGetOwnSignedIds rs_identity_get_own_signed_ids()

Get own signed ids

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get own signed ids
    api_response = api_instance.rs_identity_get_own_signed_ids()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_get_own_signed_ids: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsIdentityGetOwnSignedIds**](RespRsIdentityGetOwnSignedIds.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] ids(vector&lt;RsGxsId&gt;)storage for the ids   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_identity_from_base64**
> RespRsIdentityIdentityFromBase64 rs_identity_identity_from_base64(req_rs_identity_identity_from_base64=req_rs_identity_identity_from_base64)

Import identity from base64 representation

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_identity_from_base64 = {"base64String":"( string)base64 representation of the identity to import "} # ReqRsIdentityIdentityFromBase64 | base64String: \"( string)base64 representation of the identity to import \"  (optional)

try:
    # Import identity from base64 representation
    api_response = api_instance.rs_identity_identity_from_base64(req_rs_identity_identity_from_base64=req_rs_identity_identity_from_base64)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_identity_from_base64: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_identity_from_base64** | [**ReqRsIdentityIdentityFromBase64**](ReqRsIdentityIdentityFromBase64.md)| base64String: \&quot;( string)base64 representation of the identity to import \&quot;  | [optional] 

### Return type

[**RespRsIdentityIdentityFromBase64**](RespRsIdentityIdentityFromBase64.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] id(RsGxsId)storage for the identity id   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_identity_to_base64**
> RespRsIdentityIdentityToBase64 rs_identity_identity_to_base64(req_rs_identity_identity_to_base64=req_rs_identity_identity_to_base64)

Get base64 representation of an identity

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_identity_to_base64 = {"id":"( RsGxsId)Id of the identity "} # ReqRsIdentityIdentityToBase64 | id: \"( RsGxsId)Id of the identity \"  (optional)

try:
    # Get base64 representation of an identity
    api_response = api_instance.rs_identity_identity_to_base64(req_rs_identity_identity_to_base64=req_rs_identity_identity_to_base64)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_identity_to_base64: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_identity_to_base64** | [**ReqRsIdentityIdentityToBase64**](ReqRsIdentityIdentityToBase64.md)| id: \&quot;( RsGxsId)Id of the identity \&quot;  | [optional] 

### Return type

[**RespRsIdentityIdentityToBase64**](RespRsIdentityIdentityToBase64.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] base64String(string)storage for the identity base64   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_is_a_regular_contact**
> RespRsIdentityIsARegularContact rs_identity_is_a_regular_contact(req_rs_identity_is_a_regular_contact=req_rs_identity_is_a_regular_contact)

Check if an identity is contact

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_is_a_regular_contact = {"id":"( RsGxsId)Id of the identity "} # ReqRsIdentityIsARegularContact | id: \"( RsGxsId)Id of the identity \"  (optional)

try:
    # Check if an identity is contact
    api_response = api_instance.rs_identity_is_a_regular_contact(req_rs_identity_is_a_regular_contact=req_rs_identity_is_a_regular_contact)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_is_a_regular_contact: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_is_a_regular_contact** | [**ReqRsIdentityIsARegularContact**](ReqRsIdentityIsARegularContact.md)| id: \&quot;( RsGxsId)Id of the identity \&quot;  | [optional] 

### Return type

[**RespRsIdentityIsARegularContact**](RespRsIdentityIsARegularContact.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if it is a conctact, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_is_own_id**
> RespRsIdentityIsOwnId rs_identity_is_own_id(req_rs_identity_is_own_id=req_rs_identity_is_own_id)

Check if an id is own

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_is_own_id = {"id":"( RsGxsId)Id to check "} # ReqRsIdentityIsOwnId | id: \"( RsGxsId)Id to check \"  (optional)

try:
    # Check if an id is own
    api_response = api_instance.rs_identity_is_own_id(req_rs_identity_is_own_id=req_rs_identity_is_own_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_is_own_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_is_own_id** | [**ReqRsIdentityIsOwnId**](ReqRsIdentityIsOwnId.md)| id: \&quot;( RsGxsId)Id to check \&quot;  | [optional] 

### Return type

[**RespRsIdentityIsOwnId**](RespRsIdentityIsOwnId.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if the id is own, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_request_identity**
> RespRsIdentityRequestIdentity rs_identity_request_identity(req_rs_identity_request_identity=req_rs_identity_request_identity)

request details of a not yet known identity to the network

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_request_identity = {"id":"( RsGxsId)id of the identity to request "} # ReqRsIdentityRequestIdentity | id: \"( RsGxsId)id of the identity to request \"  (optional)

try:
    # request details of a not yet known identity to the network
    api_response = api_instance.rs_identity_request_identity(req_rs_identity_request_identity=req_rs_identity_request_identity)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_request_identity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_request_identity** | [**ReqRsIdentityRequestIdentity**](ReqRsIdentityRequestIdentity.md)| id: \&quot;( RsGxsId)id of the identity to request \&quot;  | [optional] 

### Return type

[**RespRsIdentityRequestIdentity**](RespRsIdentityRequestIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_request_status**
> RespRsIdentityRequestStatus rs_identity_request_status(req_rs_identity_request_status=req_rs_identity_request_status)

null

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_request_status = {"token":"( integer)None"} # ReqRsIdentityRequestStatus | token: \"( integer)None\"  (optional)

try:
    # null
    api_response = api_instance.rs_identity_request_status(req_rs_identity_request_status=req_rs_identity_request_status)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_request_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_request_status** | [**ReqRsIdentityRequestStatus**](ReqRsIdentityRequestStatus.md)| token: \&quot;( integer)None\&quot;  | [optional] 

### Return type

[**RespRsIdentityRequestStatus**](RespRsIdentityRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_set_as_regular_contact**
> RespRsIdentitySetAsRegularContact rs_identity_set_as_regular_contact(req_rs_identity_set_as_regular_contact=req_rs_identity_set_as_regular_contact)

Set/unset identity as contact

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_set_as_regular_contact = {"id":"( RsGxsId)Id of the identity ","isContact":"( boolean)true to set, false to unset "} # ReqRsIdentitySetAsRegularContact | id: \"( RsGxsId)Id of the identity \"         isContact: \"( boolean)true to set, false to unset \"  (optional)

try:
    # Set/unset identity as contact
    api_response = api_instance.rs_identity_set_as_regular_contact(req_rs_identity_set_as_regular_contact=req_rs_identity_set_as_regular_contact)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_set_as_regular_contact: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_set_as_regular_contact** | [**ReqRsIdentitySetAsRegularContact**](ReqRsIdentitySetAsRegularContact.md)| id: \&quot;( RsGxsId)Id of the identity \&quot;         isContact: \&quot;( boolean)true to set, false to unset \&quot;  | [optional] 

### Return type

[**RespRsIdentitySetAsRegularContact**](RespRsIdentitySetAsRegularContact.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_set_auto_add_friend_ids_as_contact**
> object rs_identity_set_auto_add_friend_ids_as_contact(req_rs_identity_set_auto_add_friend_ids_as_contact=req_rs_identity_set_auto_add_friend_ids_as_contact)

Toggle automatic flagging signed by friends identity as contact

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_set_auto_add_friend_ids_as_contact = {"enabled":"( boolean)true to enable, false to disable "} # ReqRsIdentitySetAutoAddFriendIdsAsContact | enabled: \"( boolean)true to enable, false to disable \"  (optional)

try:
    # Toggle automatic flagging signed by friends identity as contact
    api_response = api_instance.rs_identity_set_auto_add_friend_ids_as_contact(req_rs_identity_set_auto_add_friend_ids_as_contact=req_rs_identity_set_auto_add_friend_ids_as_contact)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_set_auto_add_friend_ids_as_contact: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_set_auto_add_friend_ids_as_contact** | [**ReqRsIdentitySetAutoAddFriendIdsAsContact**](ReqRsIdentitySetAutoAddFriendIdsAsContact.md)| enabled: \&quot;( boolean)true to enable, false to disable \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_set_delete_banned_nodes_threshold**
> object rs_identity_set_delete_banned_nodes_threshold(req_rs_identity_set_delete_banned_nodes_threshold=req_rs_identity_set_delete_banned_nodes_threshold)

Set number of days after which delete a banned identities

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_set_delete_banned_nodes_threshold = {"days":"( integer)number of days "} # ReqRsIdentitySetDeleteBannedNodesThreshold | days: \"( integer)number of days \"  (optional)

try:
    # Set number of days after which delete a banned identities
    api_response = api_instance.rs_identity_set_delete_banned_nodes_threshold(req_rs_identity_set_delete_banned_nodes_threshold=req_rs_identity_set_delete_banned_nodes_threshold)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_set_delete_banned_nodes_threshold: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_set_delete_banned_nodes_threshold** | [**ReqRsIdentitySetDeleteBannedNodesThreshold**](ReqRsIdentitySetDeleteBannedNodesThreshold.md)| days: \&quot;( integer)number of days \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_identity_update_identity**
> RespRsIdentityUpdateIdentity rs_identity_update_identity(req_rs_identity_update_identity=req_rs_identity_update_identity)

Update identity data (name, avatar...)

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_identity_update_identity = {"identityData":"( RsGxsIdGroup)updated identiy data "} # ReqRsIdentityUpdateIdentity | identityData: \"( RsGxsIdGroup)updated identiy data \"  (optional)

try:
    # Update identity data (name, avatar...)
    api_response = api_instance.rs_identity_update_identity(req_rs_identity_update_identity=req_rs_identity_update_identity)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_identity_update_identity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_identity_update_identity** | [**ReqRsIdentityUpdateIdentity**](ReqRsIdentityUpdateIdentity.md)| identityData: \&quot;( RsGxsIdGroup)updated identiy data \&quot;  | [optional] 

### Return type

[**RespRsIdentityUpdateIdentity**](RespRsIdentityUpdateIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_login_helper_attempt_login**
> RespRsLoginHelperAttemptLogin rs_login_helper_attempt_login(req_rs_login_helper_attempt_login=req_rs_login_helper_attempt_login)

Normal way to attempt login

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_login_helper_attempt_login = {"account":"( RsPeerId)Id of the account to which attempt login ","password":"( string)Password for the given account "} # ReqRsLoginHelperAttemptLogin | account: \"( RsPeerId)Id of the account to which attempt login \"         password: \"( string)Password for the given account \"  (optional)

try:
    # Normal way to attempt login
    api_response = api_instance.rs_login_helper_attempt_login(req_rs_login_helper_attempt_login=req_rs_login_helper_attempt_login)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_login_helper_attempt_login: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_login_helper_attempt_login** | [**ReqRsLoginHelperAttemptLogin**](ReqRsLoginHelperAttemptLogin.md)| account: \&quot;( RsPeerId)Id of the account to which attempt login \&quot;         password: \&quot;( string)Password for the given account \&quot;  | [optional] 

### Return type

[**RespRsLoginHelperAttemptLogin**](RespRsLoginHelperAttemptLogin.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_login_helper_collect_entropy**
> RespRsLoginHelperCollectEntropy rs_login_helper_collect_entropy(req_rs_login_helper_collect_entropy=req_rs_login_helper_collect_entropy)

Feed extra entropy to the crypto libraries

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()
req_rs_login_helper_collect_entropy = {"bytes":"( integer)number to feed to the entropy pool "} # ReqRsLoginHelperCollectEntropy | bytes: \"( integer)number to feed to the entropy pool \"  (optional)

try:
    # Feed extra entropy to the crypto libraries
    api_response = api_instance.rs_login_helper_collect_entropy(req_rs_login_helper_collect_entropy=req_rs_login_helper_collect_entropy)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_login_helper_collect_entropy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_login_helper_collect_entropy** | [**ReqRsLoginHelperCollectEntropy**](ReqRsLoginHelperCollectEntropy.md)| bytes: \&quot;( integer)number to feed to the entropy pool \&quot;  | [optional] 

### Return type

[**RespRsLoginHelperCollectEntropy**](RespRsLoginHelperCollectEntropy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_login_helper_create_location**
> RespRsLoginHelperCreateLocation rs_login_helper_create_location(req_rs_login_helper_create_location=req_rs_login_helper_create_location)

Creates a new RetroShare location, and log in once is created

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_login_helper_create_location = {"location":"( RsLoginHelper_Location)provide input information to generate the location and storage to output the data of the generated location ","password":"( string)to protect and unlock the associated PGP key ","makeHidden":"( boolean)pass true to create an hidden location. UNTESTED! ","makeAutoTor":"( boolean)pass true to create an automatically configured Tor hidden location. UNTESTED! "} # ReqRsLoginHelperCreateLocation | location: \"( RsLoginHelper_Location)provide input information to generate the location and storage to output the data of the generated location \"         password: \"( string)to protect and unlock the associated PGP key \"         makeHidden: \"( boolean)pass true to create an hidden location. UNTESTED! \"         makeAutoTor: \"( boolean)pass true to create an automatically configured Tor hidden location. UNTESTED! \"  (optional)

try:
    # Creates a new RetroShare location, and log in once is created
    api_response = api_instance.rs_login_helper_create_location(req_rs_login_helper_create_location=req_rs_login_helper_create_location)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_login_helper_create_location: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_login_helper_create_location** | [**ReqRsLoginHelperCreateLocation**](ReqRsLoginHelperCreateLocation.md)| location: \&quot;( RsLoginHelper_Location)provide input information to generate the location and storage to output the data of the generated location \&quot;         password: \&quot;( string)to protect and unlock the associated PGP key \&quot;         makeHidden: \&quot;( boolean)pass true to create an hidden location. UNTESTED! \&quot;         makeAutoTor: \&quot;( boolean)pass true to create an automatically configured Tor hidden location. UNTESTED! \&quot;  | [optional] 

### Return type

[**RespRsLoginHelperCreateLocation**](RespRsLoginHelperCreateLocation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if success, false otherwise  [out] errorMessage(string)if some error occurred human readable error message   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_login_helper_get_locations**
> RespRsLoginHelperGetLocations rs_login_helper_get_locations()

Get locations and associated information

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    # Get locations and associated information
    api_response = api_instance.rs_login_helper_get_locations()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_login_helper_get_locations: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsLoginHelperGetLocations**](RespRsLoginHelperGetLocations.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:  [out] locations(vector&lt;RsLoginHelper_Location&gt;)storage for the retrived locations   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_login_helper_is_logged_in**
> RespRsLoginHelperIsLoggedIn rs_login_helper_is_logged_in()

Check if RetroShare is already logged in, this usually return true after a successfull

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    # Check if RetroShare is already logged in, this usually return true after a successfull
    api_response = api_instance.rs_login_helper_is_logged_in()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_login_helper_is_logged_in: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsLoginHelperIsLoggedIn**](RespRsLoginHelperIsLoggedIn.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if already logged in, false otherwise   |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_accept_lobby_invite**
> RespRsMsgsAcceptLobbyInvite rs_msgs_accept_lobby_invite(req_rs_msgs_accept_lobby_invite=req_rs_msgs_accept_lobby_invite)

acceptLobbyInvite accept a chat invite

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_accept_lobby_invite = {"id":"( ChatLobbyId)chat lobby id you were invited into and you want to join ","identity":"( RsGxsId)chat identity to use "} # ReqRsMsgsAcceptLobbyInvite | id: \"( ChatLobbyId)chat lobby id you were invited into and you want to join \"         identity: \"( RsGxsId)chat identity to use \"  (optional)

try:
    # acceptLobbyInvite accept a chat invite
    api_response = api_instance.rs_msgs_accept_lobby_invite(req_rs_msgs_accept_lobby_invite=req_rs_msgs_accept_lobby_invite)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_accept_lobby_invite: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_accept_lobby_invite** | [**ReqRsMsgsAcceptLobbyInvite**](ReqRsMsgsAcceptLobbyInvite.md)| id: \&quot;( ChatLobbyId)chat lobby id you were invited into and you want to join \&quot;         identity: \&quot;( RsGxsId)chat identity to use \&quot;  | [optional] 

### Return type

[**RespRsMsgsAcceptLobbyInvite**](RespRsMsgsAcceptLobbyInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_clear_chat_lobby**
> object rs_msgs_clear_chat_lobby(req_rs_msgs_clear_chat_lobby=req_rs_msgs_clear_chat_lobby)

clearChatLobby clear a chat lobby

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_clear_chat_lobby = {"id":"( ChatId)chat lobby id to clear "} # ReqRsMsgsClearChatLobby | id: \"( ChatId)chat lobby id to clear \"  (optional)

try:
    # clearChatLobby clear a chat lobby
    api_response = api_instance.rs_msgs_clear_chat_lobby(req_rs_msgs_clear_chat_lobby=req_rs_msgs_clear_chat_lobby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_clear_chat_lobby: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_clear_chat_lobby** | [**ReqRsMsgsClearChatLobby**](ReqRsMsgsClearChatLobby.md)| id: \&quot;( ChatId)chat lobby id to clear \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_create_chat_lobby**
> RespRsMsgsCreateChatLobby rs_msgs_create_chat_lobby(req_rs_msgs_create_chat_lobby=req_rs_msgs_create_chat_lobby)

createChatLobby create a new chat lobby

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_create_chat_lobby = {"lobby_name":"( string)lobby name ","lobby_identity":"( RsGxsId)chat id to use for new lobby ","lobby_topic":"( string)lobby toppic ","invited_friends":"( set<RsPeerId>)list of friends to invite ","lobby_privacy_type":"( ChatLobbyFlags)flag for new chat lobby "} # ReqRsMsgsCreateChatLobby | lobby_name: \"( string)lobby name \"         lobby_identity: \"( RsGxsId)chat id to use for new lobby \"         lobby_topic: \"( string)lobby toppic \"         invited_friends: \"( set<RsPeerId>)list of friends to invite \"         lobby_privacy_type: \"( ChatLobbyFlags)flag for new chat lobby \"  (optional)

try:
    # createChatLobby create a new chat lobby
    api_response = api_instance.rs_msgs_create_chat_lobby(req_rs_msgs_create_chat_lobby=req_rs_msgs_create_chat_lobby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_create_chat_lobby: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_create_chat_lobby** | [**ReqRsMsgsCreateChatLobby**](ReqRsMsgsCreateChatLobby.md)| lobby_name: \&quot;( string)lobby name \&quot;         lobby_identity: \&quot;( RsGxsId)chat id to use for new lobby \&quot;         lobby_topic: \&quot;( string)lobby toppic \&quot;         invited_friends: \&quot;( set&lt;RsPeerId&gt;)list of friends to invite \&quot;         lobby_privacy_type: \&quot;( ChatLobbyFlags)flag for new chat lobby \&quot;  | [optional] 

### Return type

[**RespRsMsgsCreateChatLobby**](RespRsMsgsCreateChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: chat id of new lobby   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_deny_lobby_invite**
> object rs_msgs_deny_lobby_invite(req_rs_msgs_deny_lobby_invite=req_rs_msgs_deny_lobby_invite)

denyLobbyInvite deny a chat lobby invite

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_deny_lobby_invite = {"id":"( ChatLobbyId)chat lobby id you were invited into "} # ReqRsMsgsDenyLobbyInvite | id: \"( ChatLobbyId)chat lobby id you were invited into \"  (optional)

try:
    # denyLobbyInvite deny a chat lobby invite
    api_response = api_instance.rs_msgs_deny_lobby_invite(req_rs_msgs_deny_lobby_invite=req_rs_msgs_deny_lobby_invite)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_deny_lobby_invite: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_deny_lobby_invite** | [**ReqRsMsgsDenyLobbyInvite**](ReqRsMsgsDenyLobbyInvite.md)| id: \&quot;( ChatLobbyId)chat lobby id you were invited into \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_chat_lobby_info**
> RespRsMsgsGetChatLobbyInfo rs_msgs_get_chat_lobby_info(req_rs_msgs_get_chat_lobby_info=req_rs_msgs_get_chat_lobby_info)

getChatLobbyInfo get lobby info of a subscribed chat lobby. Returns true if lobby id is valid.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_get_chat_lobby_info = {"id":"( ChatLobbyId)id to get infos from "} # ReqRsMsgsGetChatLobbyInfo | id: \"( ChatLobbyId)id to get infos from \"  (optional)

try:
    # getChatLobbyInfo get lobby info of a subscribed chat lobby. Returns true if lobby id is valid.
    api_response = api_instance.rs_msgs_get_chat_lobby_info(req_rs_msgs_get_chat_lobby_info=req_rs_msgs_get_chat_lobby_info)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_chat_lobby_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_get_chat_lobby_info** | [**ReqRsMsgsGetChatLobbyInfo**](ReqRsMsgsGetChatLobbyInfo.md)| id: \&quot;( ChatLobbyId)id to get infos from \&quot;  | [optional] 

### Return type

[**RespRsMsgsGetChatLobbyInfo**](RespRsMsgsGetChatLobbyInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success  [out] info(ChatLobbyInfo)lobby infos   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_chat_lobby_list**
> RespRsMsgsGetChatLobbyList rs_msgs_get_chat_lobby_list()

getChatLobbyList get ids of subscribed lobbies

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getChatLobbyList get ids of subscribed lobbies
    api_response = api_instance.rs_msgs_get_chat_lobby_list()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_chat_lobby_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetChatLobbyList**](RespRsMsgsGetChatLobbyList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:  [out] cl_list(list&lt;ChatLobbyId&gt;)lobby list   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_custom_state_string**
> RespRsMsgsGetCustomStateString rs_msgs_get_custom_state_string(req_rs_msgs_get_custom_state_string=req_rs_msgs_get_custom_state_string)

getCustomStateString get the custom status message from a peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_get_custom_state_string = {"peer_id":"( RsPeerId)peer id to the peer you want to get the status message from "} # ReqRsMsgsGetCustomStateString | peer_id: \"( RsPeerId)peer id to the peer you want to get the status message from \"  (optional)

try:
    # getCustomStateString get the custom status message from a peer
    api_response = api_instance.rs_msgs_get_custom_state_string(req_rs_msgs_get_custom_state_string=req_rs_msgs_get_custom_state_string)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_custom_state_string: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_get_custom_state_string** | [**ReqRsMsgsGetCustomStateString**](ReqRsMsgsGetCustomStateString.md)| peer_id: \&quot;( RsPeerId)peer id to the peer you want to get the status message from \&quot;  | [optional] 

### Return type

[**RespRsMsgsGetCustomStateString**](RespRsMsgsGetCustomStateString.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: status message   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_default_identity_for_chat_lobby**
> RespRsMsgsGetDefaultIdentityForChatLobby rs_msgs_get_default_identity_for_chat_lobby()

getDefaultIdentityForChatLobby get the default identity used for chat lobbies

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getDefaultIdentityForChatLobby get the default identity used for chat lobbies
    api_response = api_instance.rs_msgs_get_default_identity_for_chat_lobby()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_default_identity_for_chat_lobby: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetDefaultIdentityForChatLobby**](RespRsMsgsGetDefaultIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:  [out] id(RsGxsId)chat identitiy to use   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_identity_for_chat_lobby**
> RespRsMsgsGetIdentityForChatLobby rs_msgs_get_identity_for_chat_lobby(req_rs_msgs_get_identity_for_chat_lobby=req_rs_msgs_get_identity_for_chat_lobby)

getIdentityForChatLobby

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_get_identity_for_chat_lobby = {"lobby_id":"( ChatLobbyId)lobby to get the chat id from "} # ReqRsMsgsGetIdentityForChatLobby | lobby_id: \"( ChatLobbyId)lobby to get the chat id from \"  (optional)

try:
    # getIdentityForChatLobby
    api_response = api_instance.rs_msgs_get_identity_for_chat_lobby(req_rs_msgs_get_identity_for_chat_lobby=req_rs_msgs_get_identity_for_chat_lobby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_identity_for_chat_lobby: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_get_identity_for_chat_lobby** | [**ReqRsMsgsGetIdentityForChatLobby**](ReqRsMsgsGetIdentityForChatLobby.md)| lobby_id: \&quot;( ChatLobbyId)lobby to get the chat id from \&quot;  | [optional] 

### Return type

[**RespRsMsgsGetIdentityForChatLobby**](RespRsMsgsGetIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success  [out] nick(RsGxsId)chat identity   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_list_of_nearby_chat_lobbies**
> RespRsMsgsGetListOfNearbyChatLobbies rs_msgs_get_list_of_nearby_chat_lobbies()

getListOfNearbyChatLobbies get info about all lobbies, subscribed and unsubscribed

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getListOfNearbyChatLobbies get info about all lobbies, subscribed and unsubscribed
    api_response = api_instance.rs_msgs_get_list_of_nearby_chat_lobbies()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_list_of_nearby_chat_lobbies: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetListOfNearbyChatLobbies**](RespRsMsgsGetListOfNearbyChatLobbies.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:  [out] public_lobbies(vector&lt;VisibleChatLobbyRecord&gt;)list of all visible lobbies   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_lobby_auto_subscribe**
> RespRsMsgsGetLobbyAutoSubscribe rs_msgs_get_lobby_auto_subscribe(req_rs_msgs_get_lobby_auto_subscribe=req_rs_msgs_get_lobby_auto_subscribe)

getLobbyAutoSubscribe get current value of auto subscribe

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_get_lobby_auto_subscribe = {"lobby_id":"( ChatLobbyId)lobby to get value from "} # ReqRsMsgsGetLobbyAutoSubscribe | lobby_id: \"( ChatLobbyId)lobby to get value from \"  (optional)

try:
    # getLobbyAutoSubscribe get current value of auto subscribe
    api_response = api_instance.rs_msgs_get_lobby_auto_subscribe(req_rs_msgs_get_lobby_auto_subscribe=req_rs_msgs_get_lobby_auto_subscribe)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_lobby_auto_subscribe: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_get_lobby_auto_subscribe** | [**ReqRsMsgsGetLobbyAutoSubscribe**](ReqRsMsgsGetLobbyAutoSubscribe.md)| lobby_id: \&quot;( ChatLobbyId)lobby to get value from \&quot;  | [optional] 

### Return type

[**RespRsMsgsGetLobbyAutoSubscribe**](RespRsMsgsGetLobbyAutoSubscribe.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: wether lobby has auto subscribe enabled or disabled   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_max_message_security_size**
> RespRsMsgsGetMaxMessageSecuritySize rs_msgs_get_max_message_security_size(req_rs_msgs_get_max_message_security_size=req_rs_msgs_get_max_message_security_size)

getMaxMessageSecuritySize get the maximum size of a chta message

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_get_max_message_security_size = {"type":"( integer)chat type "} # ReqRsMsgsGetMaxMessageSecuritySize | type: \"( integer)chat type \"  (optional)

try:
    # getMaxMessageSecuritySize get the maximum size of a chta message
    api_response = api_instance.rs_msgs_get_max_message_security_size(req_rs_msgs_get_max_message_security_size=req_rs_msgs_get_max_message_security_size)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_max_message_security_size: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_get_max_message_security_size** | [**ReqRsMsgsGetMaxMessageSecuritySize**](ReqRsMsgsGetMaxMessageSecuritySize.md)| type: \&quot;( integer)chat type \&quot;  | [optional] 

### Return type

[**RespRsMsgsGetMaxMessageSecuritySize**](RespRsMsgsGetMaxMessageSecuritySize.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: maximum size or zero for infinite   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_message**
> RespRsMsgsGetMessage rs_msgs_get_message(req_rs_msgs_get_message=req_rs_msgs_get_message)

getMessage

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_get_message = {"msgId":"( string)message ID to lookup "} # ReqRsMsgsGetMessage | msgId: \"( string)message ID to lookup \"  (optional)

try:
    # getMessage
    api_response = api_instance.rs_msgs_get_message(req_rs_msgs_get_message=req_rs_msgs_get_message)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_message: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_get_message** | [**ReqRsMsgsGetMessage**](ReqRsMsgsGetMessage.md)| msgId: \&quot;( string)message ID to lookup \&quot;  | [optional] 

### Return type

[**RespRsMsgsGetMessage**](RespRsMsgsGetMessage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success  [out] msg(Rs_Msgs_MessageInfo)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_message_count**
> RespRsMsgsGetMessageCount rs_msgs_get_message_count()

getMessageCount

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getMessageCount
    api_response = api_instance.rs_msgs_get_message_count()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_message_count: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetMessageCount**](RespRsMsgsGetMessageCount.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:  [out] nInbox(integer)None [out] nInboxNew(integer)None [out] nOutbox(integer)None [out] nDraftbox(integer)None [out] nSentbox(integer)None [out] nTrashbox(integer)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_message_summaries**
> RespRsMsgsGetMessageSummaries rs_msgs_get_message_summaries()

getMessageSummaries

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getMessageSummaries
    api_response = api_instance.rs_msgs_get_message_summaries()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_message_summaries: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetMessageSummaries**](RespRsMsgsGetMessageSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: always true  [out] msgList(list&lt;Rs_Msgs_MsgInfoSummary&gt;)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_message_tag**
> RespRsMsgsGetMessageTag rs_msgs_get_message_tag(req_rs_msgs_get_message_tag=req_rs_msgs_get_message_tag)

getMessageTag

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_get_message_tag = {"msgId":"( string)None"} # ReqRsMsgsGetMessageTag | msgId: \"( string)None\"  (optional)

try:
    # getMessageTag
    api_response = api_instance.rs_msgs_get_message_tag(req_rs_msgs_get_message_tag=req_rs_msgs_get_message_tag)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_message_tag: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_get_message_tag** | [**ReqRsMsgsGetMessageTag**](ReqRsMsgsGetMessageTag.md)| msgId: \&quot;( string)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsGetMessageTag**](RespRsMsgsGetMessageTag.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success  [out] info(Rs_Msgs_MsgTagInfo)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_message_tag_types**
> RespRsMsgsGetMessageTagTypes rs_msgs_get_message_tag_types()

getMessageTagTypes

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getMessageTagTypes
    api_response = api_instance.rs_msgs_get_message_tag_types()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_message_tag_types: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetMessageTagTypes**](RespRsMsgsGetMessageTagTypes.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: always true  [out] tags(Rs_Msgs_MsgTagType)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_msg_parent_id**
> RespRsMsgsGetMsgParentId rs_msgs_get_msg_parent_id(req_rs_msgs_get_msg_parent_id=req_rs_msgs_get_msg_parent_id)

getMsgParentId

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_get_msg_parent_id = {"msgId":"( string)None"} # ReqRsMsgsGetMsgParentId | msgId: \"( string)None\"  (optional)

try:
    # getMsgParentId
    api_response = api_instance.rs_msgs_get_msg_parent_id(req_rs_msgs_get_msg_parent_id=req_rs_msgs_get_msg_parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_msg_parent_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_get_msg_parent_id** | [**ReqRsMsgsGetMsgParentId**](ReqRsMsgsGetMsgParentId.md)| msgId: \&quot;( string)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsGetMsgParentId**](RespRsMsgsGetMsgParentId.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success  [out] msgParentId(string)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_get_pending_chat_lobby_invites**
> RespRsMsgsGetPendingChatLobbyInvites rs_msgs_get_pending_chat_lobby_invites()

getPendingChatLobbyInvites get a list of all pending chat lobby invites

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getPendingChatLobbyInvites get a list of all pending chat lobby invites
    api_response = api_instance.rs_msgs_get_pending_chat_lobby_invites()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_get_pending_chat_lobby_invites: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetPendingChatLobbyInvites**](RespRsMsgsGetPendingChatLobbyInvites.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:  [out] invites(list&lt;ChatLobbyInvite&gt;)list of all pending chat lobby invites   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_invite_peer_to_lobby**
> object rs_msgs_invite_peer_to_lobby(req_rs_msgs_invite_peer_to_lobby=req_rs_msgs_invite_peer_to_lobby)

invitePeerToLobby invite a peer to join a lobby

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_invite_peer_to_lobby = {"lobby_id":"( ChatLobbyId)lobby it to invite into ","peer_id":"( RsPeerId)peer to invite "} # ReqRsMsgsInvitePeerToLobby | lobby_id: \"( ChatLobbyId)lobby it to invite into \"         peer_id: \"( RsPeerId)peer to invite \"  (optional)

try:
    # invitePeerToLobby invite a peer to join a lobby
    api_response = api_instance.rs_msgs_invite_peer_to_lobby(req_rs_msgs_invite_peer_to_lobby=req_rs_msgs_invite_peer_to_lobby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_invite_peer_to_lobby: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_invite_peer_to_lobby** | [**ReqRsMsgsInvitePeerToLobby**](ReqRsMsgsInvitePeerToLobby.md)| lobby_id: \&quot;( ChatLobbyId)lobby it to invite into \&quot;         peer_id: \&quot;( RsPeerId)peer to invite \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_join_visible_chat_lobby**
> RespRsMsgsJoinVisibleChatLobby rs_msgs_join_visible_chat_lobby(req_rs_msgs_join_visible_chat_lobby=req_rs_msgs_join_visible_chat_lobby)

joinVisibleChatLobby join a lobby that is visible

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_join_visible_chat_lobby = {"lobby_id":"( ChatLobbyId)lobby to join to ","own_id":"( RsGxsId)chat id to use "} # ReqRsMsgsJoinVisibleChatLobby | lobby_id: \"( ChatLobbyId)lobby to join to \"         own_id: \"( RsGxsId)chat id to use \"  (optional)

try:
    # joinVisibleChatLobby join a lobby that is visible
    api_response = api_instance.rs_msgs_join_visible_chat_lobby(req_rs_msgs_join_visible_chat_lobby=req_rs_msgs_join_visible_chat_lobby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_join_visible_chat_lobby: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_join_visible_chat_lobby** | [**ReqRsMsgsJoinVisibleChatLobby**](ReqRsMsgsJoinVisibleChatLobby.md)| lobby_id: \&quot;( ChatLobbyId)lobby to join to \&quot;         own_id: \&quot;( RsGxsId)chat id to use \&quot;  | [optional] 

### Return type

[**RespRsMsgsJoinVisibleChatLobby**](RespRsMsgsJoinVisibleChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_message_delete**
> RespRsMsgsMessageDelete rs_msgs_message_delete(req_rs_msgs_message_delete=req_rs_msgs_message_delete)

MessageDelete

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_message_delete = {"msgId":"( string)None"} # ReqRsMsgsMessageDelete | msgId: \"( string)None\"  (optional)

try:
    # MessageDelete
    api_response = api_instance.rs_msgs_message_delete(req_rs_msgs_message_delete=req_rs_msgs_message_delete)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_message_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_message_delete** | [**ReqRsMsgsMessageDelete**](ReqRsMsgsMessageDelete.md)| msgId: \&quot;( string)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsMessageDelete**](RespRsMsgsMessageDelete.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_message_forwarded**
> RespRsMsgsMessageForwarded rs_msgs_message_forwarded(req_rs_msgs_message_forwarded=req_rs_msgs_message_forwarded)

MessageForwarded

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_message_forwarded = {"msgId":"( string)None","forwarded":"( boolean)None"} # ReqRsMsgsMessageForwarded | msgId: \"( string)None\"         forwarded: \"( boolean)None\"  (optional)

try:
    # MessageForwarded
    api_response = api_instance.rs_msgs_message_forwarded(req_rs_msgs_message_forwarded=req_rs_msgs_message_forwarded)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_message_forwarded: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_message_forwarded** | [**ReqRsMsgsMessageForwarded**](ReqRsMsgsMessageForwarded.md)| msgId: \&quot;( string)None\&quot;         forwarded: \&quot;( boolean)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsMessageForwarded**](RespRsMsgsMessageForwarded.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_message_load_embedded_images**
> RespRsMsgsMessageLoadEmbeddedImages rs_msgs_message_load_embedded_images(req_rs_msgs_message_load_embedded_images=req_rs_msgs_message_load_embedded_images)

MessageLoadEmbeddedImages

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_message_load_embedded_images = {"msgId":"( string)None","load":"( boolean)None"} # ReqRsMsgsMessageLoadEmbeddedImages | msgId: \"( string)None\"         load: \"( boolean)None\"  (optional)

try:
    # MessageLoadEmbeddedImages
    api_response = api_instance.rs_msgs_message_load_embedded_images(req_rs_msgs_message_load_embedded_images=req_rs_msgs_message_load_embedded_images)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_message_load_embedded_images: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_message_load_embedded_images** | [**ReqRsMsgsMessageLoadEmbeddedImages**](ReqRsMsgsMessageLoadEmbeddedImages.md)| msgId: \&quot;( string)None\&quot;         load: \&quot;( boolean)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsMessageLoadEmbeddedImages**](RespRsMsgsMessageLoadEmbeddedImages.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_message_read**
> RespRsMsgsMessageRead rs_msgs_message_read(req_rs_msgs_message_read=req_rs_msgs_message_read)

MessageRead

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_message_read = {"msgId":"( string)None","unreadByUser":"( boolean)None"} # ReqRsMsgsMessageRead | msgId: \"( string)None\"         unreadByUser: \"( boolean)None\"  (optional)

try:
    # MessageRead
    api_response = api_instance.rs_msgs_message_read(req_rs_msgs_message_read=req_rs_msgs_message_read)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_message_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_message_read** | [**ReqRsMsgsMessageRead**](ReqRsMsgsMessageRead.md)| msgId: \&quot;( string)None\&quot;         unreadByUser: \&quot;( boolean)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsMessageRead**](RespRsMsgsMessageRead.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_message_replied**
> RespRsMsgsMessageReplied rs_msgs_message_replied(req_rs_msgs_message_replied=req_rs_msgs_message_replied)

MessageReplied

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_message_replied = {"msgId":"( string)None","replied":"( boolean)None"} # ReqRsMsgsMessageReplied | msgId: \"( string)None\"         replied: \"( boolean)None\"  (optional)

try:
    # MessageReplied
    api_response = api_instance.rs_msgs_message_replied(req_rs_msgs_message_replied=req_rs_msgs_message_replied)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_message_replied: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_message_replied** | [**ReqRsMsgsMessageReplied**](ReqRsMsgsMessageReplied.md)| msgId: \&quot;( string)None\&quot;         replied: \&quot;( boolean)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsMessageReplied**](RespRsMsgsMessageReplied.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_message_send**
> RespRsMsgsMessageSend rs_msgs_message_send(req_rs_msgs_message_send=req_rs_msgs_message_send)

MessageSend

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_message_send = {"info":"( Rs_Msgs_MessageInfo)None"} # ReqRsMsgsMessageSend | info: \"( Rs_Msgs_MessageInfo)None\"  (optional)

try:
    # MessageSend
    api_response = api_instance.rs_msgs_message_send(req_rs_msgs_message_send=req_rs_msgs_message_send)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_message_send: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_message_send** | [**ReqRsMsgsMessageSend**](ReqRsMsgsMessageSend.md)| info: \&quot;( Rs_Msgs_MessageInfo)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsMessageSend**](RespRsMsgsMessageSend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: always true   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_message_star**
> RespRsMsgsMessageStar rs_msgs_message_star(req_rs_msgs_message_star=req_rs_msgs_message_star)

MessageStar

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_message_star = {"msgId":"( string)None","mark":"( boolean)None"} # ReqRsMsgsMessageStar | msgId: \"( string)None\"         mark: \"( boolean)None\"  (optional)

try:
    # MessageStar
    api_response = api_instance.rs_msgs_message_star(req_rs_msgs_message_star=req_rs_msgs_message_star)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_message_star: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_message_star** | [**ReqRsMsgsMessageStar**](ReqRsMsgsMessageStar.md)| msgId: \&quot;( string)None\&quot;         mark: \&quot;( boolean)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsMessageStar**](RespRsMsgsMessageStar.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_message_to_draft**
> RespRsMsgsMessageToDraft rs_msgs_message_to_draft(req_rs_msgs_message_to_draft=req_rs_msgs_message_to_draft)

MessageToDraft

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_message_to_draft = {"info":"( Rs_Msgs_MessageInfo)None","msgParentId":"( string)None"} # ReqRsMsgsMessageToDraft | info: \"( Rs_Msgs_MessageInfo)None\"         msgParentId: \"( string)None\"  (optional)

try:
    # MessageToDraft
    api_response = api_instance.rs_msgs_message_to_draft(req_rs_msgs_message_to_draft=req_rs_msgs_message_to_draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_message_to_draft: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_message_to_draft** | [**ReqRsMsgsMessageToDraft**](ReqRsMsgsMessageToDraft.md)| info: \&quot;( Rs_Msgs_MessageInfo)None\&quot;         msgParentId: \&quot;( string)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsMessageToDraft**](RespRsMsgsMessageToDraft.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_message_to_trash**
> RespRsMsgsMessageToTrash rs_msgs_message_to_trash(req_rs_msgs_message_to_trash=req_rs_msgs_message_to_trash)

MessageToTrash

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_message_to_trash = {"msgId":"( string)Id of the message to mode to trash box ","bTrash":"( boolean)Move to trash if true, otherwise remove from trash "} # ReqRsMsgsMessageToTrash | msgId: \"( string)Id of the message to mode to trash box \"         bTrash: \"( boolean)Move to trash if true, otherwise remove from trash \"  (optional)

try:
    # MessageToTrash
    api_response = api_instance.rs_msgs_message_to_trash(req_rs_msgs_message_to_trash=req_rs_msgs_message_to_trash)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_message_to_trash: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_message_to_trash** | [**ReqRsMsgsMessageToTrash**](ReqRsMsgsMessageToTrash.md)| msgId: \&quot;( string)Id of the message to mode to trash box \&quot;         bTrash: \&quot;( boolean)Move to trash if true, otherwise remove from trash \&quot;  | [optional] 

### Return type

[**RespRsMsgsMessageToTrash**](RespRsMsgsMessageToTrash.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_remove_message_tag_type**
> RespRsMsgsRemoveMessageTagType rs_msgs_remove_message_tag_type(req_rs_msgs_remove_message_tag_type=req_rs_msgs_remove_message_tag_type)

removeMessageTagType

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_remove_message_tag_type = {"tagId":"( integer)None"} # ReqRsMsgsRemoveMessageTagType | tagId: \"( integer)None\"  (optional)

try:
    # removeMessageTagType
    api_response = api_instance.rs_msgs_remove_message_tag_type(req_rs_msgs_remove_message_tag_type=req_rs_msgs_remove_message_tag_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_remove_message_tag_type: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_remove_message_tag_type** | [**ReqRsMsgsRemoveMessageTagType**](ReqRsMsgsRemoveMessageTagType.md)| tagId: \&quot;( integer)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsRemoveMessageTagType**](RespRsMsgsRemoveMessageTagType.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_reset_message_standard_tag_types**
> RespRsMsgsResetMessageStandardTagTypes rs_msgs_reset_message_standard_tag_types()

resetMessageStandardTagTypes

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # resetMessageStandardTagTypes
    api_response = api_instance.rs_msgs_reset_message_standard_tag_types()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_reset_message_standard_tag_types: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsMsgsResetMessageStandardTagTypes**](RespRsMsgsResetMessageStandardTagTypes.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: always true  [out] tags(Rs_Msgs_MsgTagType)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_send_chat**
> RespRsMsgsSendChat rs_msgs_send_chat(req_rs_msgs_send_chat=req_rs_msgs_send_chat)

sendChat send a chat message to a given id

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_send_chat = {"id":"( ChatId)id to send the message ","msg":"( string)message to send "} # ReqRsMsgsSendChat | id: \"( ChatId)id to send the message \"         msg: \"( string)message to send \"  (optional)

try:
    # sendChat send a chat message to a given id
    api_response = api_instance.rs_msgs_send_chat(req_rs_msgs_send_chat=req_rs_msgs_send_chat)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_send_chat: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_send_chat** | [**ReqRsMsgsSendChat**](ReqRsMsgsSendChat.md)| id: \&quot;( ChatId)id to send the message \&quot;         msg: \&quot;( string)message to send \&quot;  | [optional] 

### Return type

[**RespRsMsgsSendChat**](RespRsMsgsSendChat.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_send_lobby_status_peer_leaving**
> object rs_msgs_send_lobby_status_peer_leaving(req_rs_msgs_send_lobby_status_peer_leaving=req_rs_msgs_send_lobby_status_peer_leaving)

sendLobbyStatusPeerLeaving notify friend nodes that we're leaving a subscribed lobby

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_send_lobby_status_peer_leaving = {"lobby_id":"( ChatLobbyId)lobby to leave "} # ReqRsMsgsSendLobbyStatusPeerLeaving | lobby_id: \"( ChatLobbyId)lobby to leave \"  (optional)

try:
    # sendLobbyStatusPeerLeaving notify friend nodes that we're leaving a subscribed lobby
    api_response = api_instance.rs_msgs_send_lobby_status_peer_leaving(req_rs_msgs_send_lobby_status_peer_leaving=req_rs_msgs_send_lobby_status_peer_leaving)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_send_lobby_status_peer_leaving: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_send_lobby_status_peer_leaving** | [**ReqRsMsgsSendLobbyStatusPeerLeaving**](ReqRsMsgsSendLobbyStatusPeerLeaving.md)| lobby_id: \&quot;( ChatLobbyId)lobby to leave \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_send_status_string**
> object rs_msgs_send_status_string(req_rs_msgs_send_status_string=req_rs_msgs_send_status_string)

sendStatusString send a status string

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_send_status_string = {"id":"( ChatId)chat id to send the status string to ","status_string":"( string)status string "} # ReqRsMsgsSendStatusString | id: \"( ChatId)chat id to send the status string to \"         status_string: \"( string)status string \"  (optional)

try:
    # sendStatusString send a status string
    api_response = api_instance.rs_msgs_send_status_string(req_rs_msgs_send_status_string=req_rs_msgs_send_status_string)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_send_status_string: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_send_status_string** | [**ReqRsMsgsSendStatusString**](ReqRsMsgsSendStatusString.md)| id: \&quot;( ChatId)chat id to send the status string to \&quot;         status_string: \&quot;( string)status string \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_set_custom_state_string**
> object rs_msgs_set_custom_state_string(req_rs_msgs_set_custom_state_string=req_rs_msgs_set_custom_state_string)

setCustomStateString set your custom status message

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_set_custom_state_string = {"status_string":"( string)status message "} # ReqRsMsgsSetCustomStateString | status_string: \"( string)status message \"  (optional)

try:
    # setCustomStateString set your custom status message
    api_response = api_instance.rs_msgs_set_custom_state_string(req_rs_msgs_set_custom_state_string=req_rs_msgs_set_custom_state_string)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_set_custom_state_string: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_set_custom_state_string** | [**ReqRsMsgsSetCustomStateString**](ReqRsMsgsSetCustomStateString.md)| status_string: \&quot;( string)status message \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_set_default_identity_for_chat_lobby**
> RespRsMsgsSetDefaultIdentityForChatLobby rs_msgs_set_default_identity_for_chat_lobby(req_rs_msgs_set_default_identity_for_chat_lobby=req_rs_msgs_set_default_identity_for_chat_lobby)

setDefaultIdentityForChatLobby set the default identity used for chat lobbies

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_set_default_identity_for_chat_lobby = {"nick":"( RsGxsId)chat identitiy to use "} # ReqRsMsgsSetDefaultIdentityForChatLobby | nick: \"( RsGxsId)chat identitiy to use \"  (optional)

try:
    # setDefaultIdentityForChatLobby set the default identity used for chat lobbies
    api_response = api_instance.rs_msgs_set_default_identity_for_chat_lobby(req_rs_msgs_set_default_identity_for_chat_lobby=req_rs_msgs_set_default_identity_for_chat_lobby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_set_default_identity_for_chat_lobby: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_set_default_identity_for_chat_lobby** | [**ReqRsMsgsSetDefaultIdentityForChatLobby**](ReqRsMsgsSetDefaultIdentityForChatLobby.md)| nick: \&quot;( RsGxsId)chat identitiy to use \&quot;  | [optional] 

### Return type

[**RespRsMsgsSetDefaultIdentityForChatLobby**](RespRsMsgsSetDefaultIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_set_identity_for_chat_lobby**
> RespRsMsgsSetIdentityForChatLobby rs_msgs_set_identity_for_chat_lobby(req_rs_msgs_set_identity_for_chat_lobby=req_rs_msgs_set_identity_for_chat_lobby)

setIdentityForChatLobby set the chat identit

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_set_identity_for_chat_lobby = {"lobby_id":"( ChatLobbyId)lobby to change the chat idnetity for ","nick":"( RsGxsId)new chat identity "} # ReqRsMsgsSetIdentityForChatLobby | lobby_id: \"( ChatLobbyId)lobby to change the chat idnetity for \"         nick: \"( RsGxsId)new chat identity \"  (optional)

try:
    # setIdentityForChatLobby set the chat identit
    api_response = api_instance.rs_msgs_set_identity_for_chat_lobby(req_rs_msgs_set_identity_for_chat_lobby=req_rs_msgs_set_identity_for_chat_lobby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_set_identity_for_chat_lobby: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_set_identity_for_chat_lobby** | [**ReqRsMsgsSetIdentityForChatLobby**](ReqRsMsgsSetIdentityForChatLobby.md)| lobby_id: \&quot;( ChatLobbyId)lobby to change the chat idnetity for \&quot;         nick: \&quot;( RsGxsId)new chat identity \&quot;  | [optional] 

### Return type

[**RespRsMsgsSetIdentityForChatLobby**](RespRsMsgsSetIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_set_lobby_auto_subscribe**
> object rs_msgs_set_lobby_auto_subscribe(req_rs_msgs_set_lobby_auto_subscribe=req_rs_msgs_set_lobby_auto_subscribe)

setLobbyAutoSubscribe enable or disable auto subscribe for a chat lobby

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_set_lobby_auto_subscribe = {"lobby_id":"( ChatLobbyId)lobby to auto (un)subscribe ","autoSubscribe":"( boolean)set value for auto subscribe "} # ReqRsMsgsSetLobbyAutoSubscribe | lobby_id: \"( ChatLobbyId)lobby to auto (un)subscribe \"         autoSubscribe: \"( boolean)set value for auto subscribe \"  (optional)

try:
    # setLobbyAutoSubscribe enable or disable auto subscribe for a chat lobby
    api_response = api_instance.rs_msgs_set_lobby_auto_subscribe(req_rs_msgs_set_lobby_auto_subscribe=req_rs_msgs_set_lobby_auto_subscribe)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_set_lobby_auto_subscribe: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_set_lobby_auto_subscribe** | [**ReqRsMsgsSetLobbyAutoSubscribe**](ReqRsMsgsSetLobbyAutoSubscribe.md)| lobby_id: \&quot;( ChatLobbyId)lobby to auto (un)subscribe \&quot;         autoSubscribe: \&quot;( boolean)set value for auto subscribe \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_set_message_tag**
> RespRsMsgsSetMessageTag rs_msgs_set_message_tag(req_rs_msgs_set_message_tag=req_rs_msgs_set_message_tag)

setMessageTag set == false && tagId == 0

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_set_message_tag = {"msgId":"( string)None","tagId":"( integer)None","set":"( boolean)None"} # ReqRsMsgsSetMessageTag | msgId: \"( string)None\"         tagId: \"( integer)None\"         set: \"( boolean)None\"  (optional)

try:
    # setMessageTag set == false && tagId == 0
    api_response = api_instance.rs_msgs_set_message_tag(req_rs_msgs_set_message_tag=req_rs_msgs_set_message_tag)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_set_message_tag: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_set_message_tag** | [**ReqRsMsgsSetMessageTag**](ReqRsMsgsSetMessageTag.md)| msgId: \&quot;( string)None\&quot;         tagId: \&quot;( integer)None\&quot;         set: \&quot;( boolean)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsSetMessageTag**](RespRsMsgsSetMessageTag.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_set_message_tag_type**
> RespRsMsgsSetMessageTagType rs_msgs_set_message_tag_type(req_rs_msgs_set_message_tag_type=req_rs_msgs_set_message_tag_type)

setMessageTagType

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_set_message_tag_type = {"tagId":"( integer)None","text":"( string)None","rgb_color":"( integer)None"} # ReqRsMsgsSetMessageTagType | tagId: \"( integer)None\"         text: \"( string)None\"         rgb_color: \"( integer)None\"  (optional)

try:
    # setMessageTagType
    api_response = api_instance.rs_msgs_set_message_tag_type(req_rs_msgs_set_message_tag_type=req_rs_msgs_set_message_tag_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_set_message_tag_type: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_set_message_tag_type** | [**ReqRsMsgsSetMessageTagType**](ReqRsMsgsSetMessageTagType.md)| tagId: \&quot;( integer)None\&quot;         text: \&quot;( string)None\&quot;         rgb_color: \&quot;( integer)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsSetMessageTagType**](RespRsMsgsSetMessageTagType.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_system_message**
> RespRsMsgsSystemMessage rs_msgs_system_message(req_rs_msgs_system_message=req_rs_msgs_system_message)

SystemMessage

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_system_message = {"title":"( string)None","message":"( string)None","systemFlag":"( integer)None"} # ReqRsMsgsSystemMessage | title: \"( string)None\"         message: \"( string)None\"         systemFlag: \"( integer)None\"  (optional)

try:
    # SystemMessage
    api_response = api_instance.rs_msgs_system_message(req_rs_msgs_system_message=req_rs_msgs_system_message)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_system_message: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_system_message** | [**ReqRsMsgsSystemMessage**](ReqRsMsgsSystemMessage.md)| title: \&quot;( string)None\&quot;         message: \&quot;( string)None\&quot;         systemFlag: \&quot;( integer)None\&quot;  | [optional] 

### Return type

[**RespRsMsgsSystemMessage**](RespRsMsgsSystemMessage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_msgs_unsubscribe_chat_lobby**
> object rs_msgs_unsubscribe_chat_lobby(req_rs_msgs_unsubscribe_chat_lobby=req_rs_msgs_unsubscribe_chat_lobby)

unsubscribeChatLobby leave a chat lobby

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_msgs_unsubscribe_chat_lobby = {"lobby_id":"( ChatLobbyId)lobby to leave "} # ReqRsMsgsUnsubscribeChatLobby | lobby_id: \"( ChatLobbyId)lobby to leave \"  (optional)

try:
    # unsubscribeChatLobby leave a chat lobby
    api_response = api_instance.rs_msgs_unsubscribe_chat_lobby(req_rs_msgs_unsubscribe_chat_lobby=req_rs_msgs_unsubscribe_chat_lobby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_msgs_unsubscribe_chat_lobby: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_msgs_unsubscribe_chat_lobby** | [**ReqRsMsgsUnsubscribeChatLobby**](ReqRsMsgsUnsubscribeChatLobby.md)| lobby_id: \&quot;( ChatLobbyId)lobby to leave \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_accept_invite**
> RespRsPeersAcceptInvite rs_peers_accept_invite(req_rs_peers_accept_invite=req_rs_peers_accept_invite)

Add trusted node from invite

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_accept_invite = {"invite":"( string)invite string being it in cert or URL format ","flags":"( ServicePermissionFlags)service permissions flag "} # ReqRsPeersAcceptInvite | invite: \"( string)invite string being it in cert or URL format \"         flags: \"( ServicePermissionFlags)service permissions flag \"  (optional)

try:
    # Add trusted node from invite
    api_response = api_instance.rs_peers_accept_invite(req_rs_peers_accept_invite=req_rs_peers_accept_invite)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_accept_invite: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_accept_invite** | [**ReqRsPeersAcceptInvite**](ReqRsPeersAcceptInvite.md)| invite: \&quot;( string)invite string being it in cert or URL format \&quot;         flags: \&quot;( ServicePermissionFlags)service permissions flag \&quot;  | [optional] 

### Return type

[**RespRsPeersAcceptInvite**](RespRsPeersAcceptInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_add_friend**
> RespRsPeersAddFriend rs_peers_add_friend(req_rs_peers_add_friend=req_rs_peers_add_friend)

Add trusted node

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_add_friend = {"sslId":"( RsPeerId)SSL id of the node to add ","gpgId":"( RsPgpId)PGP id of the node to add ","flags":"( ServicePermissionFlags)service permissions flag "} # ReqRsPeersAddFriend | sslId: \"( RsPeerId)SSL id of the node to add \"         gpgId: \"( RsPgpId)PGP id of the node to add \"         flags: \"( ServicePermissionFlags)service permissions flag \"  (optional)

try:
    # Add trusted node
    api_response = api_instance.rs_peers_add_friend(req_rs_peers_add_friend=req_rs_peers_add_friend)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_add_friend: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_add_friend** | [**ReqRsPeersAddFriend**](ReqRsPeersAddFriend.md)| sslId: \&quot;( RsPeerId)SSL id of the node to add \&quot;         gpgId: \&quot;( RsPgpId)PGP id of the node to add \&quot;         flags: \&quot;( ServicePermissionFlags)service permissions flag \&quot;  | [optional] 

### Return type

[**RespRsPeersAddFriend**](RespRsPeersAddFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_add_group**
> RespRsPeersAddGroup rs_peers_add_group(req_rs_peers_add_group=req_rs_peers_add_group)

addGroup create a new group

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_add_group = {"groupInfo":"( RsGroupInfo)None"} # ReqRsPeersAddGroup | groupInfo: \"( RsGroupInfo)None\"  (optional)

try:
    # addGroup create a new group
    api_response = api_instance.rs_peers_add_group(req_rs_peers_add_group=req_rs_peers_add_group)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_add_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_add_group** | [**ReqRsPeersAddGroup**](ReqRsPeersAddGroup.md)| groupInfo: \&quot;( RsGroupInfo)None\&quot;  | [optional] 

### Return type

[**RespRsPeersAddGroup**](RespRsPeersAddGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_add_peer_locator**
> RespRsPeersAddPeerLocator rs_peers_add_peer_locator(req_rs_peers_add_peer_locator=req_rs_peers_add_peer_locator)

Add URL locator for given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_add_peer_locator = {"sslId":"( RsPeerId)SSL id of the peer, own id is accepted too ","locator":"( RsUrl)peer url locator "} # ReqRsPeersAddPeerLocator | sslId: \"( RsPeerId)SSL id of the peer, own id is accepted too \"         locator: \"( RsUrl)peer url locator \"  (optional)

try:
    # Add URL locator for given peer
    api_response = api_instance.rs_peers_add_peer_locator(req_rs_peers_add_peer_locator=req_rs_peers_add_peer_locator)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_add_peer_locator: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_add_peer_locator** | [**ReqRsPeersAddPeerLocator**](ReqRsPeersAddPeerLocator.md)| sslId: \&quot;( RsPeerId)SSL id of the peer, own id is accepted too \&quot;         locator: \&quot;( RsUrl)peer url locator \&quot;  | [optional] 

### Return type

[**RespRsPeersAddPeerLocator**](RespRsPeersAddPeerLocator.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_assign_peer_to_group**
> RespRsPeersAssignPeerToGroup rs_peers_assign_peer_to_group(req_rs_peers_assign_peer_to_group=req_rs_peers_assign_peer_to_group)

assignPeerToGroup add a peer to a group

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_assign_peer_to_group = {"groupId":"( RsNodeGroupId)None","peerId":"( RsPgpId)None","assign":"( boolean)true to assign a peer, false to remove a peer "} # ReqRsPeersAssignPeerToGroup | groupId: \"( RsNodeGroupId)None\"         peerId: \"( RsPgpId)None\"         assign: \"( boolean)true to assign a peer, false to remove a peer \"  (optional)

try:
    # assignPeerToGroup add a peer to a group
    api_response = api_instance.rs_peers_assign_peer_to_group(req_rs_peers_assign_peer_to_group=req_rs_peers_assign_peer_to_group)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_assign_peer_to_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_assign_peer_to_group** | [**ReqRsPeersAssignPeerToGroup**](ReqRsPeersAssignPeerToGroup.md)| groupId: \&quot;( RsNodeGroupId)None\&quot;         peerId: \&quot;( RsPgpId)None\&quot;         assign: \&quot;( boolean)true to assign a peer, false to remove a peer \&quot;  | [optional] 

### Return type

[**RespRsPeersAssignPeerToGroup**](RespRsPeersAssignPeerToGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_assign_peers_to_group**
> RespRsPeersAssignPeersToGroup rs_peers_assign_peers_to_group(req_rs_peers_assign_peers_to_group=req_rs_peers_assign_peers_to_group)

assignPeersToGroup add a list of peers to a group

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_assign_peers_to_group = {"groupId":"( RsNodeGroupId)None","peerIds":"( list<RsPgpId>)None","assign":"( boolean)true to assign a peer, false to remove a peer "} # ReqRsPeersAssignPeersToGroup | groupId: \"( RsNodeGroupId)None\"         peerIds: \"( list<RsPgpId>)None\"         assign: \"( boolean)true to assign a peer, false to remove a peer \"  (optional)

try:
    # assignPeersToGroup add a list of peers to a group
    api_response = api_instance.rs_peers_assign_peers_to_group(req_rs_peers_assign_peers_to_group=req_rs_peers_assign_peers_to_group)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_assign_peers_to_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_assign_peers_to_group** | [**ReqRsPeersAssignPeersToGroup**](ReqRsPeersAssignPeersToGroup.md)| groupId: \&quot;( RsNodeGroupId)None\&quot;         peerIds: \&quot;( list&lt;RsPgpId&gt;)None\&quot;         assign: \&quot;( boolean)true to assign a peer, false to remove a peer \&quot;  | [optional] 

### Return type

[**RespRsPeersAssignPeersToGroup**](RespRsPeersAssignPeersToGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_connect_attempt**
> RespRsPeersConnectAttempt rs_peers_connect_attempt(req_rs_peers_connect_attempt=req_rs_peers_connect_attempt)

Trigger connection attempt to given node

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_connect_attempt = {"sslId":"( RsPeerId)SSL id of the node to connect "} # ReqRsPeersConnectAttempt | sslId: \"( RsPeerId)SSL id of the node to connect \"  (optional)

try:
    # Trigger connection attempt to given node
    api_response = api_instance.rs_peers_connect_attempt(req_rs_peers_connect_attempt=req_rs_peers_connect_attempt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_connect_attempt: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_connect_attempt** | [**ReqRsPeersConnectAttempt**](ReqRsPeersConnectAttempt.md)| sslId: \&quot;( RsPeerId)SSL id of the node to connect \&quot;  | [optional] 

### Return type

[**RespRsPeersConnectAttempt**](RespRsPeersConnectAttempt.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_edit_group**
> RespRsPeersEditGroup rs_peers_edit_group(req_rs_peers_edit_group=req_rs_peers_edit_group)

editGroup edit an existing group

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_edit_group = {"groupId":"( RsNodeGroupId)None","groupInfo":"( RsGroupInfo)None"} # ReqRsPeersEditGroup | groupId: \"( RsNodeGroupId)None\"         groupInfo: \"( RsGroupInfo)None\"  (optional)

try:
    # editGroup edit an existing group
    api_response = api_instance.rs_peers_edit_group(req_rs_peers_edit_group=req_rs_peers_edit_group)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_edit_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_edit_group** | [**ReqRsPeersEditGroup**](ReqRsPeersEditGroup.md)| groupId: \&quot;( RsNodeGroupId)None\&quot;         groupInfo: \&quot;( RsGroupInfo)None\&quot;  | [optional] 

### Return type

[**RespRsPeersEditGroup**](RespRsPeersEditGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_get_friend_list**
> RespRsPeersGetFriendList rs_peers_get_friend_list()

Get trusted peers list

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get trusted peers list
    api_response = api_instance.rs_peers_get_friend_list()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_get_friend_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsPeersGetFriendList**](RespRsPeersGetFriendList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise  [out] sslIds(list&lt;RsPeerId&gt;)storage for the trusted peers   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_get_gpg_id**
> RespRsPeersGetGPGId rs_peers_get_gpg_id(req_rs_peers_get_gpg_id=req_rs_peers_get_gpg_id)

Get PGP id for the given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_get_gpg_id = {"sslId":"( RsPeerId)SSL id of the peer "} # ReqRsPeersGetGPGId | sslId: \"( RsPeerId)SSL id of the peer \"  (optional)

try:
    # Get PGP id for the given peer
    api_response = api_instance.rs_peers_get_gpg_id(req_rs_peers_get_gpg_id=req_rs_peers_get_gpg_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_get_gpg_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_get_gpg_id** | [**ReqRsPeersGetGPGId**](ReqRsPeersGetGPGId.md)| sslId: \&quot;( RsPeerId)SSL id of the peer \&quot;  | [optional] 

### Return type

[**RespRsPeersGetGPGId**](RespRsPeersGetGPGId.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: PGP id of the peer   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_get_group_info**
> RespRsPeersGetGroupInfo rs_peers_get_group_info(req_rs_peers_get_group_info=req_rs_peers_get_group_info)

getGroupInfo get group information to one group

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_get_group_info = {"groupId":"( RsNodeGroupId)None"} # ReqRsPeersGetGroupInfo | groupId: \"( RsNodeGroupId)None\"  (optional)

try:
    # getGroupInfo get group information to one group
    api_response = api_instance.rs_peers_get_group_info(req_rs_peers_get_group_info=req_rs_peers_get_group_info)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_get_group_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_get_group_info** | [**ReqRsPeersGetGroupInfo**](ReqRsPeersGetGroupInfo.md)| groupId: \&quot;( RsNodeGroupId)None\&quot;  | [optional] 

### Return type

[**RespRsPeersGetGroupInfo**](RespRsPeersGetGroupInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: None [out] groupInfo(RsGroupInfo)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_get_group_info_by_name**
> RespRsPeersGetGroupInfoByName rs_peers_get_group_info_by_name(req_rs_peers_get_group_info_by_name=req_rs_peers_get_group_info_by_name)

getGroupInfoByName get group information by group name

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_get_group_info_by_name = {"groupName":"( string)None"} # ReqRsPeersGetGroupInfoByName | groupName: \"( string)None\"  (optional)

try:
    # getGroupInfoByName get group information by group name
    api_response = api_instance.rs_peers_get_group_info_by_name(req_rs_peers_get_group_info_by_name=req_rs_peers_get_group_info_by_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_get_group_info_by_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_get_group_info_by_name** | [**ReqRsPeersGetGroupInfoByName**](ReqRsPeersGetGroupInfoByName.md)| groupName: \&quot;( string)None\&quot;  | [optional] 

### Return type

[**RespRsPeersGetGroupInfoByName**](RespRsPeersGetGroupInfoByName.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: None [out] groupInfo(RsGroupInfo)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_get_group_info_list**
> RespRsPeersGetGroupInfoList rs_peers_get_group_info_list()

getGroupInfoList get list of all groups

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # getGroupInfoList get list of all groups
    api_response = api_instance.rs_peers_get_group_info_list()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_get_group_info_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsPeersGetGroupInfoList**](RespRsPeersGetGroupInfoList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: None [out] groupInfoList(list&lt;RsGroupInfo&gt;)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_get_online_list**
> RespRsPeersGetOnlineList rs_peers_get_online_list()

Get connected peers list

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get connected peers list
    api_response = api_instance.rs_peers_get_online_list()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_get_online_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsPeersGetOnlineList**](RespRsPeersGetOnlineList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise  [out] sslIds(list&lt;RsPeerId&gt;)storage for the peers   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_get_peer_details**
> RespRsPeersGetPeerDetails rs_peers_get_peer_details(req_rs_peers_get_peer_details=req_rs_peers_get_peer_details)

Get details details of the given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_get_peer_details = {"sslId":"( RsPeerId)id of the peer "} # ReqRsPeersGetPeerDetails | sslId: \"( RsPeerId)id of the peer \"  (optional)

try:
    # Get details details of the given peer
    api_response = api_instance.rs_peers_get_peer_details(req_rs_peers_get_peer_details=req_rs_peers_get_peer_details)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_get_peer_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_get_peer_details** | [**ReqRsPeersGetPeerDetails**](ReqRsPeersGetPeerDetails.md)| sslId: \&quot;( RsPeerId)id of the peer \&quot;  | [optional] 

### Return type

[**RespRsPeersGetPeerDetails**](RespRsPeersGetPeerDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise  [out] det(RsPeerDetails)storage for the details of the peer   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_get_peers_count**
> RespRsPeersGetPeersCount rs_peers_get_peers_count(req_rs_peers_get_peers_count=req_rs_peers_get_peers_count)

Get peers count

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_get_peers_count = {"countLocations":"( boolean)true to count multiple locations of same owner "} # ReqRsPeersGetPeersCount | countLocations: \"( boolean)true to count multiple locations of same owner \"  (optional)

try:
    # Get peers count
    api_response = api_instance.rs_peers_get_peers_count(req_rs_peers_get_peers_count=req_rs_peers_get_peers_count)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_get_peers_count: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_get_peers_count** | [**ReqRsPeersGetPeersCount**](ReqRsPeersGetPeersCount.md)| countLocations: \&quot;( boolean)true to count multiple locations of same owner \&quot;  | [optional] 

### Return type

[**RespRsPeersGetPeersCount**](RespRsPeersGetPeersCount.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise  [out] peersCount(integer)storage for trusted peers count  [out] onlinePeersCount(integer)storage for online peers count   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_get_retroshare_invite**
> RespRsPeersGetRetroshareInvite rs_peers_get_retroshare_invite(req_rs_peers_get_retroshare_invite=req_rs_peers_get_retroshare_invite)

Get RetroShare invite of the given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_get_retroshare_invite = {"sslId":"( RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned. ","includeSignatures":"( boolean)true to add key signatures to the invite ","includeExtraLocators":"( boolean)false to avoid to add extra locators "} # ReqRsPeersGetRetroshareInvite | sslId: \"( RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned. \"         includeSignatures: \"( boolean)true to add key signatures to the invite \"         includeExtraLocators: \"( boolean)false to avoid to add extra locators \"  (optional)

try:
    # Get RetroShare invite of the given peer
    api_response = api_instance.rs_peers_get_retroshare_invite(req_rs_peers_get_retroshare_invite=req_rs_peers_get_retroshare_invite)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_get_retroshare_invite: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_get_retroshare_invite** | [**ReqRsPeersGetRetroshareInvite**](ReqRsPeersGetRetroshareInvite.md)| sslId: \&quot;( RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned. \&quot;         includeSignatures: \&quot;( boolean)true to add key signatures to the invite \&quot;         includeExtraLocators: \&quot;( boolean)false to avoid to add extra locators \&quot;  | [optional] 

### Return type

[**RespRsPeersGetRetroshareInvite**](RespRsPeersGetRetroshareInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: invite string   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_is_friend**
> RespRsPeersIsFriend rs_peers_is_friend(req_rs_peers_is_friend=req_rs_peers_is_friend)

Check if given peer is a trusted node

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_is_friend = {"sslId":"( RsPeerId)id of the peer to check "} # ReqRsPeersIsFriend | sslId: \"( RsPeerId)id of the peer to check \"  (optional)

try:
    # Check if given peer is a trusted node
    api_response = api_instance.rs_peers_is_friend(req_rs_peers_is_friend=req_rs_peers_is_friend)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_is_friend: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_is_friend** | [**ReqRsPeersIsFriend**](ReqRsPeersIsFriend.md)| sslId: \&quot;( RsPeerId)id of the peer to check \&quot;  | [optional] 

### Return type

[**RespRsPeersIsFriend**](RespRsPeersIsFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if the node is trusted, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_is_online**
> RespRsPeersIsOnline rs_peers_is_online(req_rs_peers_is_online=req_rs_peers_is_online)

Check if there is an established connection to the given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_is_online = {"sslId":"( RsPeerId)id of the peer to check "} # ReqRsPeersIsOnline | sslId: \"( RsPeerId)id of the peer to check \"  (optional)

try:
    # Check if there is an established connection to the given peer
    api_response = api_instance.rs_peers_is_online(req_rs_peers_is_online=req_rs_peers_is_online)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_is_online: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_is_online** | [**ReqRsPeersIsOnline**](ReqRsPeersIsOnline.md)| sslId: \&quot;( RsPeerId)id of the peer to check \&quot;  | [optional] 

### Return type

[**RespRsPeersIsOnline**](RespRsPeersIsOnline.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if the connection is establisced, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_is_pgp_friend**
> RespRsPeersIsPgpFriend rs_peers_is_pgp_friend(req_rs_peers_is_pgp_friend=req_rs_peers_is_pgp_friend)

Check if given PGP id is trusted

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_is_pgp_friend = {"pgpId":"( RsPgpId)PGP id to check "} # ReqRsPeersIsPgpFriend | pgpId: \"( RsPgpId)PGP id to check \"  (optional)

try:
    # Check if given PGP id is trusted
    api_response = api_instance.rs_peers_is_pgp_friend(req_rs_peers_is_pgp_friend=req_rs_peers_is_pgp_friend)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_is_pgp_friend: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_is_pgp_friend** | [**ReqRsPeersIsPgpFriend**](ReqRsPeersIsPgpFriend.md)| pgpId: \&quot;( RsPgpId)PGP id to check \&quot;  | [optional] 

### Return type

[**RespRsPeersIsPgpFriend**](RespRsPeersIsPgpFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if the PGP id is trusted, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_load_certificate_from_string**
> RespRsPeersLoadCertificateFromString rs_peers_load_certificate_from_string(req_rs_peers_load_certificate_from_string=req_rs_peers_load_certificate_from_string)

Import certificate into the keyring

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_load_certificate_from_string = {"cert":"( string)string representation of the certificate "} # ReqRsPeersLoadCertificateFromString | cert: \"( string)string representation of the certificate \"  (optional)

try:
    # Import certificate into the keyring
    api_response = api_instance.rs_peers_load_certificate_from_string(req_rs_peers_load_certificate_from_string=req_rs_peers_load_certificate_from_string)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_load_certificate_from_string: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_load_certificate_from_string** | [**ReqRsPeersLoadCertificateFromString**](ReqRsPeersLoadCertificateFromString.md)| cert: \&quot;( string)string representation of the certificate \&quot;  | [optional] 

### Return type

[**RespRsPeersLoadCertificateFromString**](RespRsPeersLoadCertificateFromString.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise  [out] sslId(RsPeerId)storage for the SSL id of the certificate  [out] pgpId(RsPgpId)storage for the PGP id of the certificate  [out] errorString(string)storage for the possible error string   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_load_details_from_string_cert**
> RespRsPeersLoadDetailsFromStringCert rs_peers_load_details_from_string_cert(req_rs_peers_load_details_from_string_cert=req_rs_peers_load_details_from_string_cert)

Examine certificate and get details without importing into the keyring

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_load_details_from_string_cert = {"cert":"( string)string representation of the certificate "} # ReqRsPeersLoadDetailsFromStringCert | cert: \"( string)string representation of the certificate \"  (optional)

try:
    # Examine certificate and get details without importing into the keyring
    api_response = api_instance.rs_peers_load_details_from_string_cert(req_rs_peers_load_details_from_string_cert=req_rs_peers_load_details_from_string_cert)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_load_details_from_string_cert: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_load_details_from_string_cert** | [**ReqRsPeersLoadDetailsFromStringCert**](ReqRsPeersLoadDetailsFromStringCert.md)| cert: \&quot;( string)string representation of the certificate \&quot;  | [optional] 

### Return type

[**RespRsPeersLoadDetailsFromStringCert**](RespRsPeersLoadDetailsFromStringCert.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise  [out] certDetails(RsPeerDetails)storage for the certificate details  [out] errorCode(integer)storage for possible error number   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_remove_friend**
> RespRsPeersRemoveFriend rs_peers_remove_friend(req_rs_peers_remove_friend=req_rs_peers_remove_friend)

Revoke connection trust from to node

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_remove_friend = {"pgpId":"( RsPgpId)PGP id of the node "} # ReqRsPeersRemoveFriend | pgpId: \"( RsPgpId)PGP id of the node \"  (optional)

try:
    # Revoke connection trust from to node
    api_response = api_instance.rs_peers_remove_friend(req_rs_peers_remove_friend=req_rs_peers_remove_friend)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_remove_friend: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_remove_friend** | [**ReqRsPeersRemoveFriend**](ReqRsPeersRemoveFriend.md)| pgpId: \&quot;( RsPgpId)PGP id of the node \&quot;  | [optional] 

### Return type

[**RespRsPeersRemoveFriend**](RespRsPeersRemoveFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_remove_friend_location**
> RespRsPeersRemoveFriendLocation rs_peers_remove_friend_location(req_rs_peers_remove_friend_location=req_rs_peers_remove_friend_location)

Remove location of a trusted node, useful to prune old unused locations of a trusted peer without revoking trust

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_remove_friend_location = {"sslId":"( RsPeerId)SSL id of the location to remove "} # ReqRsPeersRemoveFriendLocation | sslId: \"( RsPeerId)SSL id of the location to remove \"  (optional)

try:
    # Remove location of a trusted node, useful to prune old unused locations of a trusted peer without revoking trust
    api_response = api_instance.rs_peers_remove_friend_location(req_rs_peers_remove_friend_location=req_rs_peers_remove_friend_location)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_remove_friend_location: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_remove_friend_location** | [**ReqRsPeersRemoveFriendLocation**](ReqRsPeersRemoveFriendLocation.md)| sslId: \&quot;( RsPeerId)SSL id of the location to remove \&quot;  | [optional] 

### Return type

[**RespRsPeersRemoveFriendLocation**](RespRsPeersRemoveFriendLocation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_remove_group**
> RespRsPeersRemoveGroup rs_peers_remove_group(req_rs_peers_remove_group=req_rs_peers_remove_group)

removeGroup remove a group

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_remove_group = {"groupId":"( RsNodeGroupId)None"} # ReqRsPeersRemoveGroup | groupId: \"( RsNodeGroupId)None\"  (optional)

try:
    # removeGroup remove a group
    api_response = api_instance.rs_peers_remove_group(req_rs_peers_remove_group=req_rs_peers_remove_group)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_remove_group: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_remove_group** | [**ReqRsPeersRemoveGroup**](ReqRsPeersRemoveGroup.md)| groupId: \&quot;( RsNodeGroupId)None\&quot;  | [optional] 

### Return type

[**RespRsPeersRemoveGroup**](RespRsPeersRemoveGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_set_dyn_dns**
> RespRsPeersSetDynDNS rs_peers_set_dyn_dns(req_rs_peers_set_dyn_dns=req_rs_peers_set_dyn_dns)

Set (dynamical) domain name associated to the given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_set_dyn_dns = {"sslId":"( RsPeerId)SSL id of the peer, own id is accepted too ","addr":"( string)domain name string representation "} # ReqRsPeersSetDynDNS | sslId: \"( RsPeerId)SSL id of the peer, own id is accepted too \"         addr: \"( string)domain name string representation \"  (optional)

try:
    # Set (dynamical) domain name associated to the given peer
    api_response = api_instance.rs_peers_set_dyn_dns(req_rs_peers_set_dyn_dns=req_rs_peers_set_dyn_dns)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_set_dyn_dns: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_set_dyn_dns** | [**ReqRsPeersSetDynDNS**](ReqRsPeersSetDynDNS.md)| sslId: \&quot;( RsPeerId)SSL id of the peer, own id is accepted too \&quot;         addr: \&quot;( string)domain name string representation \&quot;  | [optional] 

### Return type

[**RespRsPeersSetDynDNS**](RespRsPeersSetDynDNS.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_set_ext_address**
> RespRsPeersSetExtAddress rs_peers_set_ext_address(req_rs_peers_set_ext_address=req_rs_peers_set_ext_address)

Set external IPv4 address for given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_set_ext_address = {"sslId":"( RsPeerId)SSL id of the peer, own id is accepted too ","addr":"( string)string representation of the external IPv4 address ","port":"( integer)external listening port "} # ReqRsPeersSetExtAddress | sslId: \"( RsPeerId)SSL id of the peer, own id is accepted too \"         addr: \"( string)string representation of the external IPv4 address \"         port: \"( integer)external listening port \"  (optional)

try:
    # Set external IPv4 address for given peer
    api_response = api_instance.rs_peers_set_ext_address(req_rs_peers_set_ext_address=req_rs_peers_set_ext_address)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_set_ext_address: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_set_ext_address** | [**ReqRsPeersSetExtAddress**](ReqRsPeersSetExtAddress.md)| sslId: \&quot;( RsPeerId)SSL id of the peer, own id is accepted too \&quot;         addr: \&quot;( string)string representation of the external IPv4 address \&quot;         port: \&quot;( integer)external listening port \&quot;  | [optional] 

### Return type

[**RespRsPeersSetExtAddress**](RespRsPeersSetExtAddress.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_set_local_address**
> RespRsPeersSetLocalAddress rs_peers_set_local_address(req_rs_peers_set_local_address=req_rs_peers_set_local_address)

Set local IPv4 address for the given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_set_local_address = {"sslId":"( RsPeerId)SSL id of the peer, own id is accepted too ","addr":"( string)string representation of the local IPv4 address ","port":"( integer)local listening port "} # ReqRsPeersSetLocalAddress | sslId: \"( RsPeerId)SSL id of the peer, own id is accepted too \"         addr: \"( string)string representation of the local IPv4 address \"         port: \"( integer)local listening port \"  (optional)

try:
    # Set local IPv4 address for the given peer
    api_response = api_instance.rs_peers_set_local_address(req_rs_peers_set_local_address=req_rs_peers_set_local_address)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_set_local_address: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_set_local_address** | [**ReqRsPeersSetLocalAddress**](ReqRsPeersSetLocalAddress.md)| sslId: \&quot;( RsPeerId)SSL id of the peer, own id is accepted too \&quot;         addr: \&quot;( string)string representation of the local IPv4 address \&quot;         port: \&quot;( integer)local listening port \&quot;  | [optional] 

### Return type

[**RespRsPeersSetLocalAddress**](RespRsPeersSetLocalAddress.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_set_network_mode**
> RespRsPeersSetNetworkMode rs_peers_set_network_mode(req_rs_peers_set_network_mode=req_rs_peers_set_network_mode)

Set network mode of the given peer

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_set_network_mode = {"sslId":"( RsPeerId)SSL id of the peer, own id is accepted too ","netMode":"( integer)one of RS_NETMODE_* "} # ReqRsPeersSetNetworkMode | sslId: \"( RsPeerId)SSL id of the peer, own id is accepted too \"         netMode: \"( integer)one of RS_NETMODE_* \"  (optional)

try:
    # Set network mode of the given peer
    api_response = api_instance.rs_peers_set_network_mode(req_rs_peers_set_network_mode=req_rs_peers_set_network_mode)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_set_network_mode: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_set_network_mode** | [**ReqRsPeersSetNetworkMode**](ReqRsPeersSetNetworkMode.md)| sslId: \&quot;( RsPeerId)SSL id of the peer, own id is accepted too \&quot;         netMode: \&quot;( integer)one of RS_NETMODE_* \&quot;  | [optional] 

### Return type

[**RespRsPeersSetNetworkMode**](RespRsPeersSetNetworkMode.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_peers_set_vis_state**
> RespRsPeersSetVisState rs_peers_set_vis_state(req_rs_peers_set_vis_state=req_rs_peers_set_vis_state)

set DHT and discovery modes

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_peers_set_vis_state = {"sslId":"( RsPeerId)SSL id of the peer, own id is accepted too ","vsDisc":"( integer)one of RS_VS_DISC_* ","vsDht":"( integer)one of RS_VS_DHT_* "} # ReqRsPeersSetVisState | sslId: \"( RsPeerId)SSL id of the peer, own id is accepted too \"         vsDisc: \"( integer)one of RS_VS_DISC_* \"         vsDht: \"( integer)one of RS_VS_DHT_* \"  (optional)

try:
    # set DHT and discovery modes
    api_response = api_instance.rs_peers_set_vis_state(req_rs_peers_set_vis_state=req_rs_peers_set_vis_state)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_peers_set_vis_state: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_peers_set_vis_state** | [**ReqRsPeersSetVisState**](ReqRsPeersSetVisState.md)| sslId: \&quot;( RsPeerId)SSL id of the peer, own id is accepted too \&quot;         vsDisc: \&quot;( integer)one of RS_VS_DISC_* \&quot;         vsDht: \&quot;( integer)one of RS_VS_DHT_* \&quot;  | [optional] 

### Return type

[**RespRsPeersSetVisState**](RespRsPeersSetVisState.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false if error occurred, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_auto_positive_opinion_for_contacts**
> RespRsReputationsAutoPositiveOpinionForContacts rs_reputations_auto_positive_opinion_for_contacts()

check if giving automatic positive opinion when flagging as contact is enbaled

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # check if giving automatic positive opinion when flagging as contact is enbaled
    api_response = api_instance.rs_reputations_auto_positive_opinion_for_contacts()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_auto_positive_opinion_for_contacts: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsReputationsAutoPositiveOpinionForContacts**](RespRsReputationsAutoPositiveOpinionForContacts.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if enabled, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_ban_node**
> object rs_reputations_ban_node(req_rs_reputations_ban_node=req_rs_reputations_ban_node)

Enable automatic banning of all identities signed by the given node

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_ban_node = {"id":"( RsPgpId)PGP id of the node ","b":"( boolean)true to enable, false to disable "} # ReqRsReputationsBanNode | id: \"( RsPgpId)PGP id of the node \"         b: \"( boolean)true to enable, false to disable \"  (optional)

try:
    # Enable automatic banning of all identities signed by the given node
    api_response = api_instance.rs_reputations_ban_node(req_rs_reputations_ban_node=req_rs_reputations_ban_node)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_ban_node: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_ban_node** | [**ReqRsReputationsBanNode**](ReqRsReputationsBanNode.md)| id: \&quot;( RsPgpId)PGP id of the node \&quot;         b: \&quot;( boolean)true to enable, false to disable \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_get_own_opinion**
> RespRsReputationsGetOwnOpinion rs_reputations_get_own_opinion(req_rs_reputations_get_own_opinion=req_rs_reputations_get_own_opinion)

Get own opition about the given identity

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_get_own_opinion = {"id":"( RsGxsId)Id of the identity "} # ReqRsReputationsGetOwnOpinion | id: \"( RsGxsId)Id of the identity \"  (optional)

try:
    # Get own opition about the given identity
    api_response = api_instance.rs_reputations_get_own_opinion(req_rs_reputations_get_own_opinion=req_rs_reputations_get_own_opinion)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_get_own_opinion: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_get_own_opinion** | [**ReqRsReputationsGetOwnOpinion**](ReqRsReputationsGetOwnOpinion.md)| id: \&quot;( RsGxsId)Id of the identity \&quot;  | [optional] 

### Return type

[**RespRsReputationsGetOwnOpinion**](RespRsReputationsGetOwnOpinion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] op(RsOpinion)Own opinion   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_get_reputation_info**
> RespRsReputationsGetReputationInfo rs_reputations_get_reputation_info(req_rs_reputations_get_reputation_info=req_rs_reputations_get_reputation_info)

Get reputation data of given identity

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_get_reputation_info = {"id":"( RsGxsId)Id of the identity ","ownerNode":"( RsPgpId)Optiona PGP id of the signed identity, accept a null (all zero/noninitialized) PGP id ","stamp":"( boolean)if true, timestamo the information "} # ReqRsReputationsGetReputationInfo | id: \"( RsGxsId)Id of the identity \"         ownerNode: \"( RsPgpId)Optiona PGP id of the signed identity, accept a null (all zero/noninitialized) PGP id \"         stamp: \"( boolean)if true, timestamo the information \"  (optional)

try:
    # Get reputation data of given identity
    api_response = api_instance.rs_reputations_get_reputation_info(req_rs_reputations_get_reputation_info=req_rs_reputations_get_reputation_info)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_get_reputation_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_get_reputation_info** | [**ReqRsReputationsGetReputationInfo**](ReqRsReputationsGetReputationInfo.md)| id: \&quot;( RsGxsId)Id of the identity \&quot;         ownerNode: \&quot;( RsPgpId)Optiona PGP id of the signed identity, accept a null (all zero/noninitialized) PGP id \&quot;         stamp: \&quot;( boolean)if true, timestamo the information \&quot;  | [optional] 

### Return type

[**RespRsReputationsGetReputationInfo**](RespRsReputationsGetReputationInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise  [out] info(RsReputationInfo)storage for the information   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_is_identity_banned**
> RespRsReputationsIsIdentityBanned rs_reputations_is_identity_banned(req_rs_reputations_is_identity_banned=req_rs_reputations_is_identity_banned)

This method allow fast checking if a GXS identity is banned.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_is_identity_banned = {"id":"( RsGxsId)Id of the identity to check "} # ReqRsReputationsIsIdentityBanned | id: \"( RsGxsId)Id of the identity to check \"  (optional)

try:
    # This method allow fast checking if a GXS identity is banned.
    api_response = api_instance.rs_reputations_is_identity_banned(req_rs_reputations_is_identity_banned=req_rs_reputations_is_identity_banned)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_is_identity_banned: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_is_identity_banned** | [**ReqRsReputationsIsIdentityBanned**](ReqRsReputationsIsIdentityBanned.md)| id: \&quot;( RsGxsId)Id of the identity to check \&quot;  | [optional] 

### Return type

[**RespRsReputationsIsIdentityBanned**](RespRsReputationsIsIdentityBanned.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if identity is banned, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_is_node_banned**
> RespRsReputationsIsNodeBanned rs_reputations_is_node_banned(req_rs_reputations_is_node_banned=req_rs_reputations_is_node_banned)

Check if automatic banning of all identities signed by the given node is enabled

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_is_node_banned = {"id":"( RsPgpId)PGP id of the node "} # ReqRsReputationsIsNodeBanned | id: \"( RsPgpId)PGP id of the node \"  (optional)

try:
    # Check if automatic banning of all identities signed by the given node is enabled
    api_response = api_instance.rs_reputations_is_node_banned(req_rs_reputations_is_node_banned=req_rs_reputations_is_node_banned)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_is_node_banned: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_is_node_banned** | [**ReqRsReputationsIsNodeBanned**](ReqRsReputationsIsNodeBanned.md)| id: \&quot;( RsPgpId)PGP id of the node \&quot;  | [optional] 

### Return type

[**RespRsReputationsIsNodeBanned**](RespRsReputationsIsNodeBanned.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true if enabled, false otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_overall_reputation_level**
> RespRsReputationsOverallReputationLevel rs_reputations_overall_reputation_level(req_rs_reputations_overall_reputation_level=req_rs_reputations_overall_reputation_level)

Get overall reputation level of given identity

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_overall_reputation_level = {"id":"( RsGxsId)Id of the identity "} # ReqRsReputationsOverallReputationLevel | id: \"( RsGxsId)Id of the identity \"  (optional)

try:
    # Get overall reputation level of given identity
    api_response = api_instance.rs_reputations_overall_reputation_level(req_rs_reputations_overall_reputation_level=req_rs_reputations_overall_reputation_level)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_overall_reputation_level: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_overall_reputation_level** | [**ReqRsReputationsOverallReputationLevel**](ReqRsReputationsOverallReputationLevel.md)| id: \&quot;( RsGxsId)Id of the identity \&quot;  | [optional] 

### Return type

[**RespRsReputationsOverallReputationLevel**](RespRsReputationsOverallReputationLevel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: the calculated reputation level based on available information   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_remember_banned_id_threshold**
> RespRsReputationsRememberBannedIdThreshold rs_reputations_remember_banned_id_threshold()

Get number of days to wait before deleting a banned identity from local storage

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get number of days to wait before deleting a banned identity from local storage
    api_response = api_instance.rs_reputations_remember_banned_id_threshold()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_remember_banned_id_threshold: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsReputationsRememberBannedIdThreshold**](RespRsReputationsRememberBannedIdThreshold.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: number of days to wait, 0 means never delete   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_set_auto_positive_opinion_for_contacts**
> object rs_reputations_set_auto_positive_opinion_for_contacts(req_rs_reputations_set_auto_positive_opinion_for_contacts=req_rs_reputations_set_auto_positive_opinion_for_contacts)

Enable giving automatic positive opinion when flagging as contact

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_set_auto_positive_opinion_for_contacts = {"b":"( boolean)true to enable, false to disable "} # ReqRsReputationsSetAutoPositiveOpinionForContacts | b: \"( boolean)true to enable, false to disable \"  (optional)

try:
    # Enable giving automatic positive opinion when flagging as contact
    api_response = api_instance.rs_reputations_set_auto_positive_opinion_for_contacts(req_rs_reputations_set_auto_positive_opinion_for_contacts=req_rs_reputations_set_auto_positive_opinion_for_contacts)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_set_auto_positive_opinion_for_contacts: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_set_auto_positive_opinion_for_contacts** | [**ReqRsReputationsSetAutoPositiveOpinionForContacts**](ReqRsReputationsSetAutoPositiveOpinionForContacts.md)| b: \&quot;( boolean)true to enable, false to disable \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_set_own_opinion**
> RespRsReputationsSetOwnOpinion rs_reputations_set_own_opinion(req_rs_reputations_set_own_opinion=req_rs_reputations_set_own_opinion)

Set own opinion about the given identity

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_set_own_opinion = {"id":"( RsGxsId)Id of the identity ","op":"( RsOpinion)Own opinion "} # ReqRsReputationsSetOwnOpinion | id: \"( RsGxsId)Id of the identity \"         op: \"( RsOpinion)Own opinion \"  (optional)

try:
    # Set own opinion about the given identity
    api_response = api_instance.rs_reputations_set_own_opinion(req_rs_reputations_set_own_opinion=req_rs_reputations_set_own_opinion)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_set_own_opinion: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_set_own_opinion** | [**ReqRsReputationsSetOwnOpinion**](ReqRsReputationsSetOwnOpinion.md)| id: \&quot;( RsGxsId)Id of the identity \&quot;         op: \&quot;( RsOpinion)Own opinion \&quot;  | [optional] 

### Return type

[**RespRsReputationsSetOwnOpinion**](RespRsReputationsSetOwnOpinion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: false on error, true otherwise   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_set_remember_banned_id_threshold**
> object rs_reputations_set_remember_banned_id_threshold(req_rs_reputations_set_remember_banned_id_threshold=req_rs_reputations_set_remember_banned_id_threshold)

Set number of days to wait before deleting a banned identity from local storage

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_set_remember_banned_id_threshold = {"days":"( integer)number of days to wait, 0 means never delete "} # ReqRsReputationsSetRememberBannedIdThreshold | days: \"( integer)number of days to wait, 0 means never delete \"  (optional)

try:
    # Set number of days to wait before deleting a banned identity from local storage
    api_response = api_instance.rs_reputations_set_remember_banned_id_threshold(req_rs_reputations_set_remember_banned_id_threshold=req_rs_reputations_set_remember_banned_id_threshold)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_set_remember_banned_id_threshold: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_set_remember_banned_id_threshold** | [**ReqRsReputationsSetRememberBannedIdThreshold**](ReqRsReputationsSetRememberBannedIdThreshold.md)| days: \&quot;( integer)number of days to wait, 0 means never delete \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_set_threshold_for_remotely_negative_reputation**
> object rs_reputations_set_threshold_for_remotely_negative_reputation(req_rs_reputations_set_threshold_for_remotely_negative_reputation=req_rs_reputations_set_threshold_for_remotely_negative_reputation)

Set threshold on remote reputation to consider it remotely negative

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_set_threshold_for_remotely_negative_reputation = {"thresh":"( integer)Threshold value "} # ReqRsReputationsSetThresholdForRemotelyNegativeReputation | thresh: \"( integer)Threshold value \"  (optional)

try:
    # Set threshold on remote reputation to consider it remotely negative
    api_response = api_instance.rs_reputations_set_threshold_for_remotely_negative_reputation(req_rs_reputations_set_threshold_for_remotely_negative_reputation=req_rs_reputations_set_threshold_for_remotely_negative_reputation)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_set_threshold_for_remotely_negative_reputation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_set_threshold_for_remotely_negative_reputation** | [**ReqRsReputationsSetThresholdForRemotelyNegativeReputation**](ReqRsReputationsSetThresholdForRemotelyNegativeReputation.md)| thresh: \&quot;( integer)Threshold value \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_set_threshold_for_remotely_positive_reputation**
> object rs_reputations_set_threshold_for_remotely_positive_reputation(req_rs_reputations_set_threshold_for_remotely_positive_reputation=req_rs_reputations_set_threshold_for_remotely_positive_reputation)

Set threshold on remote reputation to consider it remotely positive

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_reputations_set_threshold_for_remotely_positive_reputation = {"thresh":"( integer)Threshold value "} # ReqRsReputationsSetThresholdForRemotelyPositiveReputation | thresh: \"( integer)Threshold value \"  (optional)

try:
    # Set threshold on remote reputation to consider it remotely positive
    api_response = api_instance.rs_reputations_set_threshold_for_remotely_positive_reputation(req_rs_reputations_set_threshold_for_remotely_positive_reputation=req_rs_reputations_set_threshold_for_remotely_positive_reputation)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_set_threshold_for_remotely_positive_reputation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_reputations_set_threshold_for_remotely_positive_reputation** | [**ReqRsReputationsSetThresholdForRemotelyPositiveReputation**](ReqRsReputationsSetThresholdForRemotelyPositiveReputation.md)| thresh: \&quot;( integer)Threshold value \&quot;  | [optional] 

### Return type

**object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_threshold_for_remotely_negative_reputation**
> RespRsReputationsThresholdForRemotelyNegativeReputation rs_reputations_threshold_for_remotely_negative_reputation()

Get threshold on remote reputation to consider it remotely negative

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get threshold on remote reputation to consider it remotely negative
    api_response = api_instance.rs_reputations_threshold_for_remotely_negative_reputation()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_threshold_for_remotely_negative_reputation: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsReputationsThresholdForRemotelyNegativeReputation**](RespRsReputationsThresholdForRemotelyNegativeReputation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: Threshold value   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_reputations_threshold_for_remotely_positive_reputation**
> RespRsReputationsThresholdForRemotelyPositiveReputation rs_reputations_threshold_for_remotely_positive_reputation()

Get threshold on remote reputation to consider it remotely negative

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # Get threshold on remote reputation to consider it remotely negative
    api_response = api_instance.rs_reputations_threshold_for_remotely_positive_reputation()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_reputations_threshold_for_remotely_positive_reputation: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsReputationsThresholdForRemotelyPositiveReputation**](RespRsReputationsThresholdForRemotelyPositiveReputation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: Threshold value   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_service_control_get_own_services**
> RespRsServiceControlGetOwnServices rs_service_control_get_own_services()

get a map off all services.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))

try:
    # get a map off all services.
    api_response = api_instance.rs_service_control_get_own_services()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_service_control_get_own_services: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RespRsServiceControlGetOwnServices**](RespRsServiceControlGetOwnServices.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: always true  [out] info(RsPeerServiceInfo)storage for service information   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_service_control_get_peers_connected**
> RespRsServiceControlGetPeersConnected rs_service_control_get_peers_connected(req_rs_service_control_get_peers_connected=req_rs_service_control_get_peers_connected)

getPeersConnected return peers using a service.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_service_control_get_peers_connected = {"serviceId":"( integer)service to look up. "} # ReqRsServiceControlGetPeersConnected | serviceId: \"( integer)service to look up. \"  (optional)

try:
    # getPeersConnected return peers using a service.
    api_response = api_instance.rs_service_control_get_peers_connected(req_rs_service_control_get_peers_connected=req_rs_service_control_get_peers_connected)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_service_control_get_peers_connected: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_service_control_get_peers_connected** | [**ReqRsServiceControlGetPeersConnected**](ReqRsServiceControlGetPeersConnected.md)| serviceId: \&quot;( integer)service to look up. \&quot;  | [optional] 

### Return type

[**RespRsServiceControlGetPeersConnected**](RespRsServiceControlGetPeersConnected.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return:  [out] peerSet(set&lt;RsPeerId&gt;)set of peers using this service.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_service_control_get_service_item_names**
> RespRsServiceControlGetServiceItemNames rs_service_control_get_service_item_names(req_rs_service_control_get_service_item_names=req_rs_service_control_get_service_item_names)

getServiceItemNames return a map of service item names.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_service_control_get_service_item_names = {"serviceId":"( integer)service to look up "} # ReqRsServiceControlGetServiceItemNames | serviceId: \"( integer)service to look up \"  (optional)

try:
    # getServiceItemNames return a map of service item names.
    api_response = api_instance.rs_service_control_get_service_item_names(req_rs_service_control_get_service_item_names=req_rs_service_control_get_service_item_names)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_service_control_get_service_item_names: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_service_control_get_service_item_names** | [**ReqRsServiceControlGetServiceItemNames**](ReqRsServiceControlGetServiceItemNames.md)| serviceId: \&quot;( integer)service to look up \&quot;  | [optional] 

### Return type

[**RespRsServiceControlGetServiceItemNames**](RespRsServiceControlGetServiceItemNames.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success false otherwise  [out] names(map&lt;uint8_t,std_string&gt;)names of items   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_service_control_get_service_name**
> RespRsServiceControlGetServiceName rs_service_control_get_service_name(req_rs_service_control_get_service_name=req_rs_service_control_get_service_name)

getServiceName lookup the name of a service.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_service_control_get_service_name = {"serviceId":"( integer)service to look up "} # ReqRsServiceControlGetServiceName | serviceId: \"( integer)service to look up \"  (optional)

try:
    # getServiceName lookup the name of a service.
    api_response = api_instance.rs_service_control_get_service_name(req_rs_service_control_get_service_name=req_rs_service_control_get_service_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_service_control_get_service_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_service_control_get_service_name** | [**ReqRsServiceControlGetServiceName**](ReqRsServiceControlGetServiceName.md)| serviceId: \&quot;( integer)service to look up \&quot;  | [optional] 

### Return type

[**RespRsServiceControlGetServiceName**](RespRsServiceControlGetServiceName.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: name of service   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_service_control_get_service_permissions**
> RespRsServiceControlGetServicePermissions rs_service_control_get_service_permissions(req_rs_service_control_get_service_permissions=req_rs_service_control_get_service_permissions)

getServicePermissions return permissions of one service.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_service_control_get_service_permissions = {"serviceId":"( integer)service id to look up "} # ReqRsServiceControlGetServicePermissions | serviceId: \"( integer)service id to look up \"  (optional)

try:
    # getServicePermissions return permissions of one service.
    api_response = api_instance.rs_service_control_get_service_permissions(req_rs_service_control_get_service_permissions=req_rs_service_control_get_service_permissions)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_service_control_get_service_permissions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_service_control_get_service_permissions** | [**ReqRsServiceControlGetServicePermissions**](ReqRsServiceControlGetServicePermissions.md)| serviceId: \&quot;( integer)service id to look up \&quot;  | [optional] 

### Return type

[**RespRsServiceControlGetServicePermissions**](RespRsServiceControlGetServicePermissions.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success false otherwise.  [out] permissions(RsServicePermissions)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_service_control_get_services_allowed**
> RespRsServiceControlGetServicesAllowed rs_service_control_get_services_allowed(req_rs_service_control_get_services_allowed=req_rs_service_control_get_services_allowed)

getServicesAllowed return a mpa with allowed service information.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_service_control_get_services_allowed = {"peerId":"( RsPeerId)peer to look up "} # ReqRsServiceControlGetServicesAllowed | peerId: \"( RsPeerId)peer to look up \"  (optional)

try:
    # getServicesAllowed return a mpa with allowed service information.
    api_response = api_instance.rs_service_control_get_services_allowed(req_rs_service_control_get_services_allowed=req_rs_service_control_get_services_allowed)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_service_control_get_services_allowed: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_service_control_get_services_allowed** | [**ReqRsServiceControlGetServicesAllowed**](ReqRsServiceControlGetServicesAllowed.md)| peerId: \&quot;( RsPeerId)peer to look up \&quot;  | [optional] 

### Return type

[**RespRsServiceControlGetServicesAllowed**](RespRsServiceControlGetServicesAllowed.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: always true  [out] info(RsPeerServiceInfo)map with infomration   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_service_control_get_services_provided**
> RespRsServiceControlGetServicesProvided rs_service_control_get_services_provided(req_rs_service_control_get_services_provided=req_rs_service_control_get_services_provided)

getServicesProvided return services provided by a peer.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_service_control_get_services_provided = {"peerId":"( RsPeerId)peer to look up "} # ReqRsServiceControlGetServicesProvided | peerId: \"( RsPeerId)peer to look up \"  (optional)

try:
    # getServicesProvided return services provided by a peer.
    api_response = api_instance.rs_service_control_get_services_provided(req_rs_service_control_get_services_provided=req_rs_service_control_get_services_provided)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_service_control_get_services_provided: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_service_control_get_services_provided** | [**ReqRsServiceControlGetServicesProvided**](ReqRsServiceControlGetServicesProvided.md)| peerId: \&quot;( RsPeerId)peer to look up \&quot;  | [optional] 

### Return type

[**RespRsServiceControlGetServicesProvided**](RespRsServiceControlGetServicesProvided.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success false otherwise.  [out] info(RsPeerServiceInfo)None  |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rs_service_control_update_service_permissions**
> RespRsServiceControlUpdateServicePermissions rs_service_control_update_service_permissions(req_rs_service_control_update_service_permissions=req_rs_service_control_update_service_permissions)

updateServicePermissions update service permissions of one service.

### Example

* Basic Authentication (BasicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://127.0.0.1:9092
configuration.host = "http://127.0.0.1:9092"
# Create an instance of the API class
api_instance = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))
req_rs_service_control_update_service_permissions = {"serviceId":"( integer)service to update ","permissions":"( RsServicePermissions)new permissions "} # ReqRsServiceControlUpdateServicePermissions | serviceId: \"( integer)service to update \"         permissions: \"( RsServicePermissions)new permissions \"  (optional)

try:
    # updateServicePermissions update service permissions of one service.
    api_response = api_instance.rs_service_control_update_service_permissions(req_rs_service_control_update_service_permissions=req_rs_service_control_update_service_permissions)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rs_service_control_update_service_permissions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **req_rs_service_control_update_service_permissions** | [**ReqRsServiceControlUpdateServicePermissions**](ReqRsServiceControlUpdateServicePermissions.md)| serviceId: \&quot;( integer)service to update \&quot;         permissions: \&quot;( RsServicePermissions)new permissions \&quot;  | [optional] 

### Return type

[**RespRsServiceControlUpdateServicePermissions**](RespRsServiceControlUpdateServicePermissions.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | return: true on success false otherwise.   |  -  |
**401** | Authentication information is missing or invalid |  * WWW_Authenticate -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

