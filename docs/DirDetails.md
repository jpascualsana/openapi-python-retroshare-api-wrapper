# DirDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | **int** |  | [optional] 
**prow** | **int** |  | [optional] 
**ref** | **int** |  | [optional] 
**type** | **int** |  | [optional] 
**id** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**hash** | **str** |  | [optional] 
**path** | **str** |  | [optional] 
**count** | **int** |  | [optional] 
**mtime** | **int** |  | [optional] 
**flags** | **int** |  | [optional] 
**max_mtime** | **int** |  | [optional] 
**children** | [**list[DirStub]**](DirStub.md) |  | [optional] 
**parent_groups** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


