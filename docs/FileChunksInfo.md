# FileChunksInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_size** | **int** |  | [optional] 
**chunk_size** | **int** |  | [optional] 
**strategy** | [**FileChunksInfoChunkStrategy**](FileChunksInfoChunkStrategy.md) |  | [optional] 
**chunks** | [**list[ChunkState]**](ChunkState.md) |  | [optional] 
**compressed_peer_availability_maps** | [**list[FileChunksInfoCompressedPeerAvailabilityMaps]**](FileChunksInfoCompressedPeerAvailabilityMaps.md) |  | [optional] 
**active_chunks** | [**list[FileChunksInfoActiveChunks]**](FileChunksInfoActiveChunks.md) |  | [optional] 
**pending_slices** | [**list[FileChunksInfoPendingSlices]**](FileChunksInfoPendingSlices.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


