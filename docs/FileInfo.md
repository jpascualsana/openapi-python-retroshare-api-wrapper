# FileInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**storage_permission_flags** | **int** |  | [optional] 
**transfer_info_flags** | **int** |  | [optional] 
**m_id** | **int** |  | [optional] 
**search_id** | **int** |  | [optional] 
**path** | **str** |  | [optional] 
**fname** | **str** |  | [optional] 
**hash** | **str** |  | [optional] 
**ext** | **str** |  | [optional] 
**size** | **int** |  | [optional] 
**avail** | **int** |  | [optional] 
**rank** | **float** |  | [optional] 
**age** | **int** |  | [optional] 
**queue_position** | **int** |  | [optional] 
**transfered** | **int** |  | [optional] 
**tf_rate** | **float** |  | [optional] 
**download_status** | **int** |  | [optional] 
**peers** | [**list[TransferInfo]**](TransferInfo.md) |  | [optional] 
**priority** | [**DwlSpeed**](DwlSpeed.md) |  | [optional] 
**last_ts** | **int** |  | [optional] 
**parent_groups** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


