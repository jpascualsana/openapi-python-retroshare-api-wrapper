# GxsReputation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_overall_score** | **int** |  | [optional] 
**m_id_score** | **int** |  | [optional] 
**m_own_opinion** | **int** |  | [optional] 
**m_peer_opinion** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


