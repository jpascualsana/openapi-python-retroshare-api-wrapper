# RSTrafficClue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ts** | **int** |  | [optional] 
**size** | **int** |  | [optional] 
**priority** | **int** |  | [optional] 
**service_id** | **int** |  | [optional] 
**service_sub_id** | **int** |  | [optional] 
**peer_id** | **str** |  | [optional] 
**count** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


