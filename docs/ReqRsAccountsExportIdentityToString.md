# ReqRsAccountsExportIdentityToString

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pgp_id** | **str** |  | [optional] 
**include_signatures** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


