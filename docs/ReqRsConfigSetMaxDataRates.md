# ReqRsConfigSetMaxDataRates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**down_kb** | **int** |  | [optional] 
**up_kb** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


