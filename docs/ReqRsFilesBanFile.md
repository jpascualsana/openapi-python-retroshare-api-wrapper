# ReqRsFilesBanFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**real_file_hash** | **str** |  | [optional] 
**filename** | **str** |  | [optional] 
**file_size** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


