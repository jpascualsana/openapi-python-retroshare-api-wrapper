# ReqRsFilesExtraFileHash

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**localpath** | **str** |  | [optional] 
**period** | **int** |  | [optional] 
**flags** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


