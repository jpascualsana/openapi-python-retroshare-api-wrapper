# ReqRsFilesFileRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_name** | **str** |  | [optional] 
**hash** | **str** |  | [optional] 
**size** | **int** |  | [optional] 
**dest_path** | **str** |  | [optional] 
**flags** | **int** |  | [optional] 
**src_ids** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


