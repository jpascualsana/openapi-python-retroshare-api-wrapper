# ReqRsFilesSetSharedDirectories

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dirs** | [**list[SharedDirInfo]**](SharedDirInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


