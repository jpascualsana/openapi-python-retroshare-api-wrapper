# ReqRsFilesTurtleSearchRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**match_string** | **str** |  | [optional] 
**max_wait** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


