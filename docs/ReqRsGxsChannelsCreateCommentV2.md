# ReqRsGxsChannelsCreateCommentV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **str** |  | [optional] 
**thread_id** | **str** |  | [optional] 
**comment** | **str** |  | [optional] 
**author_id** | **str** |  | [optional] 
**parent_id** | **str** |  | [optional] 
**orig_comment_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


