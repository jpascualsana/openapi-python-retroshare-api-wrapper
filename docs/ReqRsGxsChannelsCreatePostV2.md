# ReqRsGxsChannelsCreatePostV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **str** |  | [optional] 
**title** | **str** |  | [optional] 
**m_body** | **str** |  | [optional] 
**files** | [**list[RsGxsFile]**](RsGxsFile.md) |  | [optional] 
**thumbnail** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 
**orig_post_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


