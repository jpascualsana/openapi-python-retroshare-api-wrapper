# ReqRsGxsChannelsCreateVoteV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **str** |  | [optional] 
**post_id** | **str** |  | [optional] 
**comment_id** | **str** |  | [optional] 
**author_id** | **str** |  | [optional] 
**vote** | [**RsGxsVoteType**](RsGxsVoteType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


