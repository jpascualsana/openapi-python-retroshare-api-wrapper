# ReqRsGxsCirclesInviteIdsToCircle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identities** | **list[str]** |  | [optional] 
**circle_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


