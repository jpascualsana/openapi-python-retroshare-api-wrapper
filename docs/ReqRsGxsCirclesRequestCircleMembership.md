# ReqRsGxsCirclesRequestCircleMembership

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**own_gxs_id** | **str** |  | [optional] 
**circle_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


