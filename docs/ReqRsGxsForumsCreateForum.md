# ReqRsGxsForumsCreateForum

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forum** | [**RsGxsForumGroup**](RsGxsForumGroup.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


