# ReqRsGxsForumsCreateForumV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**author_id** | **str** |  | [optional] 
**moderators_ids** | **list[str]** |  | [optional] 
**circle_type** | [**RsGxsCircleType**](RsGxsCircleType.md) |  | [optional] 
**circle_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


