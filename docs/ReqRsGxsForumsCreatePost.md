# ReqRsGxsForumsCreatePost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forum_id** | **str** |  | [optional] 
**title** | **str** |  | [optional] 
**m_body** | **str** |  | [optional] 
**author_id** | **str** |  | [optional] 
**parent_id** | **str** |  | [optional] 
**orig_post_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


