# ReqRsGxsForumsGetForumContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forum_id** | **str** |  | [optional] 
**msgs_ids** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


