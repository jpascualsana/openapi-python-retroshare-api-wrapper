# ReqRsGxsForumsSubscribeToForum

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forum_id** | **str** |  | [optional] 
**subscribe** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


