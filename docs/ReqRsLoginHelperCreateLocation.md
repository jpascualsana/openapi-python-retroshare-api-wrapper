# ReqRsLoginHelperCreateLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | [**RsLoginHelperLocation**](RsLoginHelperLocation.md) |  | [optional] 
**password** | **str** |  | [optional] 
**make_hidden** | **bool** |  | [optional] 
**make_auto_tor** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


