# ReqRsMsgsCreateChatLobby

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobby_name** | **str** |  | [optional] 
**lobby_identity** | **str** |  | [optional] 
**lobby_topic** | **str** |  | [optional] 
**invited_friends** | **list[str]** |  | [optional] 
**lobby_privacy_type** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


