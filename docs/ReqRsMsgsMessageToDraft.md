# ReqRsMsgsMessageToDraft

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | [**RsMsgsMessageInfo**](RsMsgsMessageInfo.md) |  | [optional] 
**msg_parent_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


