# ReqRsMsgsSetIdentityForChatLobby

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobby_id** | **int** |  | [optional] 
**nick** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


