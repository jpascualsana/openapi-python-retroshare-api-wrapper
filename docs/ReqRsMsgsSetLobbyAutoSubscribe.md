# ReqRsMsgsSetLobbyAutoSubscribe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobby_id** | **int** |  | [optional] 
**auto_subscribe** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


