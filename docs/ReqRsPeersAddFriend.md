# ReqRsPeersAddFriend

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ssl_id** | **str** |  | [optional] 
**gpg_id** | **str** |  | [optional] 
**flags** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


