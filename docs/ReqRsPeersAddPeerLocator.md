# ReqRsPeersAddPeerLocator

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ssl_id** | **str** |  | [optional] 
**locator** | [**RsUrl**](RsUrl.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


