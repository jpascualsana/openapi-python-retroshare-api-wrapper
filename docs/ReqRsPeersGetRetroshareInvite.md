# ReqRsPeersGetRetroshareInvite

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ssl_id** | **str** |  | [optional] 
**include_signatures** | **bool** |  | [optional] 
**include_extra_locators** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


