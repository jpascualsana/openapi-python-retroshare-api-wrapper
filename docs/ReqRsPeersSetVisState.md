# ReqRsPeersSetVisState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ssl_id** | **str** |  | [optional] 
**vs_disc** | **int** |  | [optional] 
**vs_dht** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


