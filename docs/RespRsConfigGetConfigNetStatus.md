# RespRsConfigGetConfigNetStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **int** |  | [optional] 
**status** | [**RsConfigNetStatus**](RsConfigNetStatus.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


