# RespRsConfigGetCurrentDataRates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **int** |  | [optional] 
**in_kb** | **float** |  | [optional] 
**out_kb** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


