# RespRsConfigGetMaxDataRates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **int** |  | [optional] 
**in_kb** | **int** |  | [optional] 
**out_kb** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


