# RespRsConfigGetTrafficInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **int** |  | [optional] 
**out_lst** | [**list[RSTrafficClue]**](RSTrafficClue.md) |  | [optional] 
**in_lst** | [**list[RSTrafficClue]**](RSTrafficClue.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


