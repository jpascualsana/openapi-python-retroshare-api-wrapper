# RespRsGossipDiscoveryGetDiscPgpFriends

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **bool** |  | [optional] 
**gpg_friends** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


