# RespRsGxsChannelsGetChannelContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **bool** |  | [optional] 
**posts** | [**list[RsGxsChannelPost]**](RsGxsChannelPost.md) |  | [optional] 
**comments** | [**list[RsGxsComment]**](RsGxsComment.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


