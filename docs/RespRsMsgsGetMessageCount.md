# RespRsMsgsGetMessageCount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**n_inbox** | **int** |  | [optional] 
**n_inbox_new** | **int** |  | [optional] 
**n_outbox** | **int** |  | [optional] 
**n_draftbox** | **int** |  | [optional] 
**n_sentbox** | **int** |  | [optional] 
**n_trashbox** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


