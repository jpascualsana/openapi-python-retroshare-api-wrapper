# RespRsMsgsGetMessageSummaries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **bool** |  | [optional] 
**msg_list** | [**list[RsMsgsMsgInfoSummary]**](RsMsgsMsgInfoSummary.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


