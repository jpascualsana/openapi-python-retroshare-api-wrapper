# RespRsMsgsGetPendingChatLobbyInvites

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invites** | [**list[ChatLobbyInvite]**](ChatLobbyInvite.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


