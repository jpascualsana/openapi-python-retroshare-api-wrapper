# RespRsPeersLoadCertificateFromString

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **bool** |  | [optional] 
**ssl_id** | **str** |  | [optional] 
**pgp_id** | **str** |  | [optional] 
**error_string** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


