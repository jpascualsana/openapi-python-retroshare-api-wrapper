# RespRsPeersLoadDetailsFromStringCert

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **bool** |  | [optional] 
**cert_details** | [**RsPeerDetails**](RsPeerDetails.md) |  | [optional] 
**error_code** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


