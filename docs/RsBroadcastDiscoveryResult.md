# RsBroadcastDiscoveryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_ssl_id** | **str** |  | [optional] 
**m_profile_name** | **str** |  | [optional] 
**m_locator** | [**RsUrl**](RsUrl.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


