# RsConfigDataRates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_rate_in** | **float** |  | [optional] 
**m_rate_max_in** | **float** |  | [optional] 
**m_alloc_in** | **float** |  | [optional] 
**m_alloc_ts** | **int** |  | [optional] 
**m_rate_out** | **float** |  | [optional] 
**m_rate_max_out** | **float** |  | [optional] 
**m_allowed_out** | **float** |  | [optional] 
**m_allowed_ts** | **int** |  | [optional] 
**m_queue_in** | **int** |  | [optional] 
**m_queue_out** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


