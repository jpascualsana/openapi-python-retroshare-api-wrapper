# RsConfigNetStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**own_id** | **str** |  | [optional] 
**own_name** | **str** |  | [optional] 
**local_addr** | **str** |  | [optional] 
**local_port** | **int** |  | [optional] 
**ext_addr** | **str** |  | [optional] 
**ext_port** | **int** |  | [optional] 
**ext_dyn_dns** | **str** |  | [optional] 
**firewalled** | **bool** |  | [optional] 
**forward_port** | **bool** |  | [optional] 
**dht_active** | **bool** |  | [optional] 
**u_pn_p_active** | **bool** |  | [optional] 
**u_pn_p_state** | **int** |  | [optional] 
**net_local_ok** | **bool** |  | [optional] 
**net_upnp_ok** | **bool** |  | [optional] 
**net_dht_ok** | **bool** |  | [optional] 
**net_stun_ok** | **bool** |  | [optional] 
**net_ext_address_ok** | **bool** |  | [optional] 
**net_dht_net_size** | **int** |  | [optional] 
**net_dht_rs_net_size** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


