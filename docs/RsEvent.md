# RsEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_type** | [**RsEventType**](RsEventType.md) |  | [optional] 
**m_time_point** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


