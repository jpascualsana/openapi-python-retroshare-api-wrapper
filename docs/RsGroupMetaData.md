# RsGroupMetaData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_group_id** | **str** |  | [optional] 
**m_group_name** | **str** |  | [optional] 
**m_group_flags** | **int** |  | [optional] 
**m_sign_flags** | **int** |  | [optional] 
**m_publish_ts** | **int** |  | [optional] 
**m_author_id** | **str** |  | [optional] 
**m_circle_id** | **str** |  | [optional] 
**m_circle_type** | **int** |  | [optional] 
**m_authen_flags** | **int** |  | [optional] 
**m_parent_grp_id** | **str** |  | [optional] 
**m_subscribe_flags** | **int** |  | [optional] 
**m_pop** | **int** |  | [optional] 
**m_visible_msg_count** | **int** |  | [optional] 
**m_last_post** | **int** |  | [optional] 
**m_group_status** | **int** |  | [optional] 
**m_service_string** | **str** |  | [optional] 
**m_originator** | **str** |  | [optional] 
**m_internal_circle** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


