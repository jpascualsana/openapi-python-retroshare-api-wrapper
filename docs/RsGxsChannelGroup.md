# RsGxsChannelGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_meta** | [**RsGroupMetaData**](RsGroupMetaData.md) |  | [optional] 
**m_description** | **str** |  | [optional] 
**m_image** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 
**m_auto_download** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


