# RsGxsChannelPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_meta** | [**RsMsgMetaData**](RsMsgMetaData.md) |  | [optional] 
**m_older_versions** | **list[str]** |  | [optional] 
**m_msg** | **str** |  | [optional] 
**m_files** | [**list[RsGxsFile]**](RsGxsFile.md) |  | [optional] 
**m_count** | **int** |  | [optional] 
**m_size** | **int** |  | [optional] 
**m_thumbnail** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


