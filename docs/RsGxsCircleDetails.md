# RsGxsCircleDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_circle_id** | **str** |  | [optional] 
**m_circle_name** | **str** |  | [optional] 
**m_circle_type** | **int** |  | [optional] 
**m_restricted_circle_id** | **str** |  | [optional] 
**m_am_i_allowed** | **bool** |  | [optional] 
**m_allowed_gxs_ids** | **list[str]** |  | [optional] 
**m_allowed_nodes** | **list[str]** |  | [optional] 
**m_subscription_flags** | [**list[RsGxsCircleDetailsMSubscriptionFlags]**](RsGxsCircleDetailsMSubscriptionFlags.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


