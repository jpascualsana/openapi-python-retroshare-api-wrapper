# RsGxsCircleGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_meta** | [**RsGroupMetaData**](RsGroupMetaData.md) |  | [optional] 
**m_local_friends** | **list[str]** |  | [optional] 
**m_invited_members** | **list[str]** |  | [optional] 
**m_sub_circles** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


