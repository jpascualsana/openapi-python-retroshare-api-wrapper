# RsGxsCircleMsg

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_meta** | [**RsMsgMetaData**](RsMsgMetaData.md) |  | [optional] 
**stuff** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


