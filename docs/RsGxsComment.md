# RsGxsComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_meta** | [**RsMsgMetaData**](RsMsgMetaData.md) |  | [optional] 
**m_comment** | **str** |  | [optional] 
**m_up_votes** | **int** |  | [optional] 
**m_down_votes** | **int** |  | [optional] 
**m_score** | **float** |  | [optional] 
**m_own_vote** | **int** |  | [optional] 
**m_votes** | [**list[RsGxsVote]**](RsGxsVote.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


