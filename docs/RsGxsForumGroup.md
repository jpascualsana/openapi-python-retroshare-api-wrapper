# RsGxsForumGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_meta** | [**RsGroupMetaData**](RsGroupMetaData.md) |  | [optional] 
**m_description** | **str** |  | [optional] 
**m_admin_list** | **str** |  | [optional] 
**m_pinned_posts** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


