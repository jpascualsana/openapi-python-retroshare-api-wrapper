# RsGxsGroupSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_group_id** | **str** |  | [optional] 
**m_group_name** | **str** |  | [optional] 
**m_author_id** | **str** |  | [optional] 
**m_publish_ts** | **int** |  | [optional] 
**m_number_of_messages** | **int** |  | [optional] 
**m_last_message_ts** | **int** |  | [optional] 
**m_sign_flags** | **int** |  | [optional] 
**m_popularity** | **int** |  | [optional] 
**m_search_context** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


