# RsGxsIdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_meta** | [**RsGroupMetaData**](RsGroupMetaData.md) |  | [optional] 
**m_pgp_id_hash** | **str** |  | [optional] 
**m_pgp_id_sign** | **str** |  | [optional] 
**m_recogn_tags** | **list[str]** |  | [optional] 
**m_image** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 
**m_last_usage_ts** | **int** |  | [optional] 
**m_pgp_known** | **bool** |  | [optional] 
**m_is_a_contact** | **bool** |  | [optional] 
**m_pgp_id** | **str** |  | [optional] 
**m_reputation** | [**GxsReputation**](GxsReputation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


