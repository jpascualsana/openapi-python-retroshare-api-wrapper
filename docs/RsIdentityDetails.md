# RsIdentityDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_id** | **str** |  | [optional] 
**m_nickname** | **str** |  | [optional] 
**m_flags** | **int** |  | [optional] 
**m_pgp_id** | **str** |  | [optional] 
**m_recogn_tags** | [**list[RsRecognTag]**](RsRecognTag.md) |  | [optional] 
**m_reputation** | [**RsReputationInfo**](RsReputationInfo.md) |  | [optional] 
**m_avatar** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 
**m_publish_ts** | **int** |  | [optional] 
**m_last_usage_ts** | **int** |  | [optional] 
**m_use_cases** | [**list[RsIdentityDetailsMUseCases]**](RsIdentityDetailsMUseCases.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


