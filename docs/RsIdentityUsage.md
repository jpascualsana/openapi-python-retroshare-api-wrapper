# RsIdentityUsage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_service_id** | [**RsServiceType**](RsServiceType.md) |  | [optional] 
**m_usage_code** | [**UsageCode**](UsageCode.md) |  | [optional] 
**m_grp_id** | **str** |  | [optional] 
**m_msg_id** | **str** |  | [optional] 
**m_additional_id** | **int** |  | [optional] 
**m_comment** | **str** |  | [optional] 
**m_hash** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


