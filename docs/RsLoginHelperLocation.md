# RsLoginHelperLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_location_id** | **str** |  | [optional] 
**m_pgp_id** | **str** |  | [optional] 
**m_location_name** | **str** |  | [optional] 
**m_ppg_name** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


