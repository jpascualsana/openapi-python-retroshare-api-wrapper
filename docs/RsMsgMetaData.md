# RsMsgMetaData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_group_id** | **str** |  | [optional] 
**m_msg_id** | **str** |  | [optional] 
**m_thread_id** | **str** |  | [optional] 
**m_parent_id** | **str** |  | [optional] 
**m_orig_msg_id** | **str** |  | [optional] 
**m_author_id** | **str** |  | [optional] 
**m_msg_name** | **str** |  | [optional] 
**m_publish_ts** | **int** |  | [optional] 
**m_msg_flags** | **int** |  | [optional] 
**m_msg_status** | **int** |  | [optional] 
**m_child_ts** | **int** |  | [optional] 
**m_service_string** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


