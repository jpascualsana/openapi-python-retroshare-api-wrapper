# RsMsgsMessageInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msg_id** | **str** |  | [optional] 
**rspeerid_src_id** | **str** |  | [optional] 
**rsgxsid_src_id** | **str** |  | [optional] 
**msgflags** | **int** |  | [optional] 
**rspeerid_msgto** | **list[str]** |  | [optional] 
**rspeerid_msgcc** | **list[str]** |  | [optional] 
**rspeerid_msgbcc** | **list[str]** |  | [optional] 
**rsgxsid_msgto** | **list[str]** |  | [optional] 
**rsgxsid_msgcc** | **list[str]** |  | [optional] 
**rsgxsid_msgbcc** | **list[str]** |  | [optional] 
**title** | **str** |  | [optional] 
**msg** | **str** |  | [optional] 
**attach_title** | **str** |  | [optional] 
**attach_comment** | **str** |  | [optional] 
**files** | [**list[FileInfo]**](FileInfo.md) |  | [optional] 
**size** | **int** |  | [optional] 
**count** | **int** |  | [optional] 
**ts** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


