# RsMsgsMsgInfoSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msg_id** | **str** |  | [optional] 
**src_id** | **str** |  | [optional] 
**msgflags** | **int** |  | [optional] 
**msgtags** | **list[int]** |  | [optional] 
**title** | **str** |  | [optional] 
**count** | **int** |  | [optional] 
**ts** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


