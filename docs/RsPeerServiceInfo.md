# RsPeerServiceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_peer_id** | **str** |  | [optional] 
**m_service_list** | [**list[RsPeerServiceInfoMServiceList]**](RsPeerServiceInfoMServiceList.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


