# RsRecognTag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag_class** | **int** |  | [optional] 
**tag_type** | **int** |  | [optional] 
**valid** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


