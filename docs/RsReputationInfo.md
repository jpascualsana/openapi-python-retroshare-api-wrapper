# RsReputationInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_own_opinion** | [**RsOpinion**](RsOpinion.md) |  | [optional] 
**m_friends_positive_votes** | **int** |  | [optional] 
**m_friends_negative_votes** | **int** |  | [optional] 
**m_friend_average_score** | **float** |  | [optional] 
**m_overall_reputation_level** | [**RsReputationLevel**](RsReputationLevel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


