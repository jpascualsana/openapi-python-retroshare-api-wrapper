# RsServiceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_service_name** | **str** |  | [optional] 
**m_service_type** | **int** |  | [optional] 
**m_version_major** | **int** |  | [optional] 
**m_version_minor** | **int** |  | [optional] 
**m_min_version_major** | **int** |  | [optional] 
**m_min_version_minor** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


