# RsServicePermissions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**m_service_id** | **int** |  | [optional] 
**m_service_name** | **str** |  | [optional] 
**m_default_allowed** | **bool** |  | [optional] 
**m_peers_allowed** | **list[str]** |  | [optional] 
**m_peers_denied** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


