# RsUrl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scheme_separator** | **str** |  | [optional] 
**ipv6_wrap_open** | **str** |  | [optional] 
**ipv6_separator** | **str** |  | [optional] 
**ipv6_wrap_close** | **str** |  | [optional] 
**port_separator** | **str** |  | [optional] 
**path_separator** | **str** |  | [optional] 
**query_separator** | **str** |  | [optional] 
**query_assign** | **str** |  | [optional] 
**query_field_sep** | **str** |  | [optional] 
**fragment_separator** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


