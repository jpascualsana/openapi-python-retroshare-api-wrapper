# TransferInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**peer_id** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**tf_rate** | **float** |  | [optional] 
**status** | **int** |  | [optional] 
**transfered** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


