# VisibleChatLobbyRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobby_id** | **int** |  | [optional] 
**lobby_name** | **str** |  | [optional] 
**lobby_topic** | **str** |  | [optional] 
**participating_friends** | **list[str]** |  | [optional] 
**total_number_of_peers** | **int** |  | [optional] 
**last_report_time** | **int** |  | [optional] 
**lobby_flags** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


