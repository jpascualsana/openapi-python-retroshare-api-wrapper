# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class ReqRsFilesSetSharedDirectories(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'dirs': 'list[SharedDirInfo]'
    }

    attribute_map = {
        'dirs': 'dirs'
    }

    def __init__(self, dirs=None):  # noqa: E501
        """ReqRsFilesSetSharedDirectories - a model defined in OpenAPI"""  # noqa: E501

        self._dirs = None
        self.discriminator = None

        if dirs is not None:
            self.dirs = dirs

    @property
    def dirs(self):
        """Gets the dirs of this ReqRsFilesSetSharedDirectories.  # noqa: E501


        :return: The dirs of this ReqRsFilesSetSharedDirectories.  # noqa: E501
        :rtype: list[SharedDirInfo]
        """
        return self._dirs

    @dirs.setter
    def dirs(self, dirs):
        """Sets the dirs of this ReqRsFilesSetSharedDirectories.


        :param dirs: The dirs of this ReqRsFilesSetSharedDirectories.  # noqa: E501
        :type: list[SharedDirInfo]
        """

        self._dirs = dirs

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ReqRsFilesSetSharedDirectories):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
