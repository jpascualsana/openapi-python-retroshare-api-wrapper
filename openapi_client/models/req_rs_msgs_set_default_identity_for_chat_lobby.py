# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class ReqRsMsgsSetDefaultIdentityForChatLobby(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'nick': 'str'
    }

    attribute_map = {
        'nick': 'nick'
    }

    def __init__(self, nick=None):  # noqa: E501
        """ReqRsMsgsSetDefaultIdentityForChatLobby - a model defined in OpenAPI"""  # noqa: E501

        self._nick = None
        self.discriminator = None

        if nick is not None:
            self.nick = nick

    @property
    def nick(self):
        """Gets the nick of this ReqRsMsgsSetDefaultIdentityForChatLobby.  # noqa: E501


        :return: The nick of this ReqRsMsgsSetDefaultIdentityForChatLobby.  # noqa: E501
        :rtype: str
        """
        return self._nick

    @nick.setter
    def nick(self, nick):
        """Sets the nick of this ReqRsMsgsSetDefaultIdentityForChatLobby.


        :param nick: The nick of this ReqRsMsgsSetDefaultIdentityForChatLobby.  # noqa: E501
        :type: str
        """

        self._nick = nick

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ReqRsMsgsSetDefaultIdentityForChatLobby):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
