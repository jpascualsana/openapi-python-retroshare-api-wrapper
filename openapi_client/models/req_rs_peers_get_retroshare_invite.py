# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class ReqRsPeersGetRetroshareInvite(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'ssl_id': 'str',
        'include_signatures': 'bool',
        'include_extra_locators': 'bool'
    }

    attribute_map = {
        'ssl_id': 'sslId',
        'include_signatures': 'includeSignatures',
        'include_extra_locators': 'includeExtraLocators'
    }

    def __init__(self, ssl_id=None, include_signatures=None, include_extra_locators=None):  # noqa: E501
        """ReqRsPeersGetRetroshareInvite - a model defined in OpenAPI"""  # noqa: E501

        self._ssl_id = None
        self._include_signatures = None
        self._include_extra_locators = None
        self.discriminator = None

        if ssl_id is not None:
            self.ssl_id = ssl_id
        if include_signatures is not None:
            self.include_signatures = include_signatures
        if include_extra_locators is not None:
            self.include_extra_locators = include_extra_locators

    @property
    def ssl_id(self):
        """Gets the ssl_id of this ReqRsPeersGetRetroshareInvite.  # noqa: E501


        :return: The ssl_id of this ReqRsPeersGetRetroshareInvite.  # noqa: E501
        :rtype: str
        """
        return self._ssl_id

    @ssl_id.setter
    def ssl_id(self, ssl_id):
        """Sets the ssl_id of this ReqRsPeersGetRetroshareInvite.


        :param ssl_id: The ssl_id of this ReqRsPeersGetRetroshareInvite.  # noqa: E501
        :type: str
        """

        self._ssl_id = ssl_id

    @property
    def include_signatures(self):
        """Gets the include_signatures of this ReqRsPeersGetRetroshareInvite.  # noqa: E501


        :return: The include_signatures of this ReqRsPeersGetRetroshareInvite.  # noqa: E501
        :rtype: bool
        """
        return self._include_signatures

    @include_signatures.setter
    def include_signatures(self, include_signatures):
        """Sets the include_signatures of this ReqRsPeersGetRetroshareInvite.


        :param include_signatures: The include_signatures of this ReqRsPeersGetRetroshareInvite.  # noqa: E501
        :type: bool
        """

        self._include_signatures = include_signatures

    @property
    def include_extra_locators(self):
        """Gets the include_extra_locators of this ReqRsPeersGetRetroshareInvite.  # noqa: E501


        :return: The include_extra_locators of this ReqRsPeersGetRetroshareInvite.  # noqa: E501
        :rtype: bool
        """
        return self._include_extra_locators

    @include_extra_locators.setter
    def include_extra_locators(self, include_extra_locators):
        """Sets the include_extra_locators of this ReqRsPeersGetRetroshareInvite.


        :param include_extra_locators: The include_extra_locators of this ReqRsPeersGetRetroshareInvite.  # noqa: E501
        :type: bool
        """

        self._include_extra_locators = include_extra_locators

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ReqRsPeersGetRetroshareInvite):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
