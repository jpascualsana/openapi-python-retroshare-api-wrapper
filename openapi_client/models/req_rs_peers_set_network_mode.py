# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class ReqRsPeersSetNetworkMode(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'ssl_id': 'str',
        'net_mode': 'int'
    }

    attribute_map = {
        'ssl_id': 'sslId',
        'net_mode': 'netMode'
    }

    def __init__(self, ssl_id=None, net_mode=None):  # noqa: E501
        """ReqRsPeersSetNetworkMode - a model defined in OpenAPI"""  # noqa: E501

        self._ssl_id = None
        self._net_mode = None
        self.discriminator = None

        if ssl_id is not None:
            self.ssl_id = ssl_id
        if net_mode is not None:
            self.net_mode = net_mode

    @property
    def ssl_id(self):
        """Gets the ssl_id of this ReqRsPeersSetNetworkMode.  # noqa: E501


        :return: The ssl_id of this ReqRsPeersSetNetworkMode.  # noqa: E501
        :rtype: str
        """
        return self._ssl_id

    @ssl_id.setter
    def ssl_id(self, ssl_id):
        """Sets the ssl_id of this ReqRsPeersSetNetworkMode.


        :param ssl_id: The ssl_id of this ReqRsPeersSetNetworkMode.  # noqa: E501
        :type: str
        """

        self._ssl_id = ssl_id

    @property
    def net_mode(self):
        """Gets the net_mode of this ReqRsPeersSetNetworkMode.  # noqa: E501


        :return: The net_mode of this ReqRsPeersSetNetworkMode.  # noqa: E501
        :rtype: int
        """
        return self._net_mode

    @net_mode.setter
    def net_mode(self, net_mode):
        """Sets the net_mode of this ReqRsPeersSetNetworkMode.


        :param net_mode: The net_mode of this ReqRsPeersSetNetworkMode.  # noqa: E501
        :type: int
        """

        self._net_mode = net_mode

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ReqRsPeersSetNetworkMode):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
