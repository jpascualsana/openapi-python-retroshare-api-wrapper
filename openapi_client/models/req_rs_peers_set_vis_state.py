# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class ReqRsPeersSetVisState(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'ssl_id': 'str',
        'vs_disc': 'int',
        'vs_dht': 'int'
    }

    attribute_map = {
        'ssl_id': 'sslId',
        'vs_disc': 'vsDisc',
        'vs_dht': 'vsDht'
    }

    def __init__(self, ssl_id=None, vs_disc=None, vs_dht=None):  # noqa: E501
        """ReqRsPeersSetVisState - a model defined in OpenAPI"""  # noqa: E501

        self._ssl_id = None
        self._vs_disc = None
        self._vs_dht = None
        self.discriminator = None

        if ssl_id is not None:
            self.ssl_id = ssl_id
        if vs_disc is not None:
            self.vs_disc = vs_disc
        if vs_dht is not None:
            self.vs_dht = vs_dht

    @property
    def ssl_id(self):
        """Gets the ssl_id of this ReqRsPeersSetVisState.  # noqa: E501


        :return: The ssl_id of this ReqRsPeersSetVisState.  # noqa: E501
        :rtype: str
        """
        return self._ssl_id

    @ssl_id.setter
    def ssl_id(self, ssl_id):
        """Sets the ssl_id of this ReqRsPeersSetVisState.


        :param ssl_id: The ssl_id of this ReqRsPeersSetVisState.  # noqa: E501
        :type: str
        """

        self._ssl_id = ssl_id

    @property
    def vs_disc(self):
        """Gets the vs_disc of this ReqRsPeersSetVisState.  # noqa: E501


        :return: The vs_disc of this ReqRsPeersSetVisState.  # noqa: E501
        :rtype: int
        """
        return self._vs_disc

    @vs_disc.setter
    def vs_disc(self, vs_disc):
        """Sets the vs_disc of this ReqRsPeersSetVisState.


        :param vs_disc: The vs_disc of this ReqRsPeersSetVisState.  # noqa: E501
        :type: int
        """

        self._vs_disc = vs_disc

    @property
    def vs_dht(self):
        """Gets the vs_dht of this ReqRsPeersSetVisState.  # noqa: E501


        :return: The vs_dht of this ReqRsPeersSetVisState.  # noqa: E501
        :rtype: int
        """
        return self._vs_dht

    @vs_dht.setter
    def vs_dht(self, vs_dht):
        """Sets the vs_dht of this ReqRsPeersSetVisState.


        :param vs_dht: The vs_dht of this ReqRsPeersSetVisState.  # noqa: E501
        :type: int
        """

        self._vs_dht = vs_dht

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ReqRsPeersSetVisState):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
