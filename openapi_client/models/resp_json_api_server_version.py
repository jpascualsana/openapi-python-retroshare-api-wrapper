# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class RespJsonApiServerVersion(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'major': 'int',
        'minor': 'int',
        'mini': 'int',
        'extra': 'str',
        'human': 'str'
    }

    attribute_map = {
        'major': 'major',
        'minor': 'minor',
        'mini': 'mini',
        'extra': 'extra',
        'human': 'human'
    }

    def __init__(self, major=None, minor=None, mini=None, extra=None, human=None):  # noqa: E501
        """RespJsonApiServerVersion - a model defined in OpenAPI"""  # noqa: E501

        self._major = None
        self._minor = None
        self._mini = None
        self._extra = None
        self._human = None
        self.discriminator = None

        if major is not None:
            self.major = major
        if minor is not None:
            self.minor = minor
        if mini is not None:
            self.mini = mini
        if extra is not None:
            self.extra = extra
        if human is not None:
            self.human = human

    @property
    def major(self):
        """Gets the major of this RespJsonApiServerVersion.  # noqa: E501


        :return: The major of this RespJsonApiServerVersion.  # noqa: E501
        :rtype: int
        """
        return self._major

    @major.setter
    def major(self, major):
        """Sets the major of this RespJsonApiServerVersion.


        :param major: The major of this RespJsonApiServerVersion.  # noqa: E501
        :type: int
        """

        self._major = major

    @property
    def minor(self):
        """Gets the minor of this RespJsonApiServerVersion.  # noqa: E501


        :return: The minor of this RespJsonApiServerVersion.  # noqa: E501
        :rtype: int
        """
        return self._minor

    @minor.setter
    def minor(self, minor):
        """Sets the minor of this RespJsonApiServerVersion.


        :param minor: The minor of this RespJsonApiServerVersion.  # noqa: E501
        :type: int
        """

        self._minor = minor

    @property
    def mini(self):
        """Gets the mini of this RespJsonApiServerVersion.  # noqa: E501


        :return: The mini of this RespJsonApiServerVersion.  # noqa: E501
        :rtype: int
        """
        return self._mini

    @mini.setter
    def mini(self, mini):
        """Sets the mini of this RespJsonApiServerVersion.


        :param mini: The mini of this RespJsonApiServerVersion.  # noqa: E501
        :type: int
        """

        self._mini = mini

    @property
    def extra(self):
        """Gets the extra of this RespJsonApiServerVersion.  # noqa: E501


        :return: The extra of this RespJsonApiServerVersion.  # noqa: E501
        :rtype: str
        """
        return self._extra

    @extra.setter
    def extra(self, extra):
        """Sets the extra of this RespJsonApiServerVersion.


        :param extra: The extra of this RespJsonApiServerVersion.  # noqa: E501
        :type: str
        """

        self._extra = extra

    @property
    def human(self):
        """Gets the human of this RespJsonApiServerVersion.  # noqa: E501


        :return: The human of this RespJsonApiServerVersion.  # noqa: E501
        :rtype: str
        """
        return self._human

    @human.setter
    def human(self, human):
        """Sets the human of this RespJsonApiServerVersion.


        :param human: The human of this RespJsonApiServerVersion.  # noqa: E501
        :type: str
        """

        self._human = human

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, RespJsonApiServerVersion):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
