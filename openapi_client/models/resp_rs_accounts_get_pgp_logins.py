# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class RespRsAccountsGetPGPLogins(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'retval': 'int',
        'pgp_ids': 'list[str]'
    }

    attribute_map = {
        'retval': 'retval',
        'pgp_ids': 'pgpIds'
    }

    def __init__(self, retval=None, pgp_ids=None):  # noqa: E501
        """RespRsAccountsGetPGPLogins - a model defined in OpenAPI"""  # noqa: E501

        self._retval = None
        self._pgp_ids = None
        self.discriminator = None

        if retval is not None:
            self.retval = retval
        if pgp_ids is not None:
            self.pgp_ids = pgp_ids

    @property
    def retval(self):
        """Gets the retval of this RespRsAccountsGetPGPLogins.  # noqa: E501


        :return: The retval of this RespRsAccountsGetPGPLogins.  # noqa: E501
        :rtype: int
        """
        return self._retval

    @retval.setter
    def retval(self, retval):
        """Sets the retval of this RespRsAccountsGetPGPLogins.


        :param retval: The retval of this RespRsAccountsGetPGPLogins.  # noqa: E501
        :type: int
        """

        self._retval = retval

    @property
    def pgp_ids(self):
        """Gets the pgp_ids of this RespRsAccountsGetPGPLogins.  # noqa: E501


        :return: The pgp_ids of this RespRsAccountsGetPGPLogins.  # noqa: E501
        :rtype: list[str]
        """
        return self._pgp_ids

    @pgp_ids.setter
    def pgp_ids(self, pgp_ids):
        """Sets the pgp_ids of this RespRsAccountsGetPGPLogins.


        :param pgp_ids: The pgp_ids of this RespRsAccountsGetPGPLogins.  # noqa: E501
        :type: list[str]
        """

        self._pgp_ids = pgp_ids

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, RespRsAccountsGetPGPLogins):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
