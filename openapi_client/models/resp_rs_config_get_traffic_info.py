# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class RespRsConfigGetTrafficInfo(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'retval': 'int',
        'out_lst': 'list[RSTrafficClue]',
        'in_lst': 'list[RSTrafficClue]'
    }

    attribute_map = {
        'retval': 'retval',
        'out_lst': 'out_lst',
        'in_lst': 'in_lst'
    }

    def __init__(self, retval=None, out_lst=None, in_lst=None):  # noqa: E501
        """RespRsConfigGetTrafficInfo - a model defined in OpenAPI"""  # noqa: E501

        self._retval = None
        self._out_lst = None
        self._in_lst = None
        self.discriminator = None

        if retval is not None:
            self.retval = retval
        if out_lst is not None:
            self.out_lst = out_lst
        if in_lst is not None:
            self.in_lst = in_lst

    @property
    def retval(self):
        """Gets the retval of this RespRsConfigGetTrafficInfo.  # noqa: E501


        :return: The retval of this RespRsConfigGetTrafficInfo.  # noqa: E501
        :rtype: int
        """
        return self._retval

    @retval.setter
    def retval(self, retval):
        """Sets the retval of this RespRsConfigGetTrafficInfo.


        :param retval: The retval of this RespRsConfigGetTrafficInfo.  # noqa: E501
        :type: int
        """

        self._retval = retval

    @property
    def out_lst(self):
        """Gets the out_lst of this RespRsConfigGetTrafficInfo.  # noqa: E501


        :return: The out_lst of this RespRsConfigGetTrafficInfo.  # noqa: E501
        :rtype: list[RSTrafficClue]
        """
        return self._out_lst

    @out_lst.setter
    def out_lst(self, out_lst):
        """Sets the out_lst of this RespRsConfigGetTrafficInfo.


        :param out_lst: The out_lst of this RespRsConfigGetTrafficInfo.  # noqa: E501
        :type: list[RSTrafficClue]
        """

        self._out_lst = out_lst

    @property
    def in_lst(self):
        """Gets the in_lst of this RespRsConfigGetTrafficInfo.  # noqa: E501


        :return: The in_lst of this RespRsConfigGetTrafficInfo.  # noqa: E501
        :rtype: list[RSTrafficClue]
        """
        return self._in_lst

    @in_lst.setter
    def in_lst(self, in_lst):
        """Sets the in_lst of this RespRsConfigGetTrafficInfo.


        :param in_lst: The in_lst of this RespRsConfigGetTrafficInfo.  # noqa: E501
        :type: list[RSTrafficClue]
        """

        self._in_lst = in_lst

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, RespRsConfigGetTrafficInfo):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
