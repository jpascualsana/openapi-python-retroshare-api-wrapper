# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class RsMsgsMsgInfoSummary(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'msg_id': 'str',
        'src_id': 'str',
        'msgflags': 'int',
        'msgtags': 'list[int]',
        'title': 'str',
        'count': 'int',
        'ts': 'int'
    }

    attribute_map = {
        'msg_id': 'msgId',
        'src_id': 'srcId',
        'msgflags': 'msgflags',
        'msgtags': 'msgtags',
        'title': 'title',
        'count': 'count',
        'ts': 'ts'
    }

    def __init__(self, msg_id=None, src_id=None, msgflags=None, msgtags=None, title=None, count=None, ts=None):  # noqa: E501
        """RsMsgsMsgInfoSummary - a model defined in OpenAPI"""  # noqa: E501

        self._msg_id = None
        self._src_id = None
        self._msgflags = None
        self._msgtags = None
        self._title = None
        self._count = None
        self._ts = None
        self.discriminator = None

        if msg_id is not None:
            self.msg_id = msg_id
        if src_id is not None:
            self.src_id = src_id
        if msgflags is not None:
            self.msgflags = msgflags
        if msgtags is not None:
            self.msgtags = msgtags
        if title is not None:
            self.title = title
        if count is not None:
            self.count = count
        if ts is not None:
            self.ts = ts

    @property
    def msg_id(self):
        """Gets the msg_id of this RsMsgsMsgInfoSummary.  # noqa: E501


        :return: The msg_id of this RsMsgsMsgInfoSummary.  # noqa: E501
        :rtype: str
        """
        return self._msg_id

    @msg_id.setter
    def msg_id(self, msg_id):
        """Sets the msg_id of this RsMsgsMsgInfoSummary.


        :param msg_id: The msg_id of this RsMsgsMsgInfoSummary.  # noqa: E501
        :type: str
        """

        self._msg_id = msg_id

    @property
    def src_id(self):
        """Gets the src_id of this RsMsgsMsgInfoSummary.  # noqa: E501


        :return: The src_id of this RsMsgsMsgInfoSummary.  # noqa: E501
        :rtype: str
        """
        return self._src_id

    @src_id.setter
    def src_id(self, src_id):
        """Sets the src_id of this RsMsgsMsgInfoSummary.


        :param src_id: The src_id of this RsMsgsMsgInfoSummary.  # noqa: E501
        :type: str
        """

        self._src_id = src_id

    @property
    def msgflags(self):
        """Gets the msgflags of this RsMsgsMsgInfoSummary.  # noqa: E501


        :return: The msgflags of this RsMsgsMsgInfoSummary.  # noqa: E501
        :rtype: int
        """
        return self._msgflags

    @msgflags.setter
    def msgflags(self, msgflags):
        """Sets the msgflags of this RsMsgsMsgInfoSummary.


        :param msgflags: The msgflags of this RsMsgsMsgInfoSummary.  # noqa: E501
        :type: int
        """

        self._msgflags = msgflags

    @property
    def msgtags(self):
        """Gets the msgtags of this RsMsgsMsgInfoSummary.  # noqa: E501


        :return: The msgtags of this RsMsgsMsgInfoSummary.  # noqa: E501
        :rtype: list[int]
        """
        return self._msgtags

    @msgtags.setter
    def msgtags(self, msgtags):
        """Sets the msgtags of this RsMsgsMsgInfoSummary.


        :param msgtags: The msgtags of this RsMsgsMsgInfoSummary.  # noqa: E501
        :type: list[int]
        """

        self._msgtags = msgtags

    @property
    def title(self):
        """Gets the title of this RsMsgsMsgInfoSummary.  # noqa: E501


        :return: The title of this RsMsgsMsgInfoSummary.  # noqa: E501
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title):
        """Sets the title of this RsMsgsMsgInfoSummary.


        :param title: The title of this RsMsgsMsgInfoSummary.  # noqa: E501
        :type: str
        """

        self._title = title

    @property
    def count(self):
        """Gets the count of this RsMsgsMsgInfoSummary.  # noqa: E501


        :return: The count of this RsMsgsMsgInfoSummary.  # noqa: E501
        :rtype: int
        """
        return self._count

    @count.setter
    def count(self, count):
        """Sets the count of this RsMsgsMsgInfoSummary.


        :param count: The count of this RsMsgsMsgInfoSummary.  # noqa: E501
        :type: int
        """

        self._count = count

    @property
    def ts(self):
        """Gets the ts of this RsMsgsMsgInfoSummary.  # noqa: E501


        :return: The ts of this RsMsgsMsgInfoSummary.  # noqa: E501
        :rtype: int
        """
        return self._ts

    @ts.setter
    def ts(self, ts):
        """Sets the ts of this RsMsgsMsgInfoSummary.


        :param ts: The ts of this RsMsgsMsgInfoSummary.  # noqa: E501
        :type: int
        """

        self._ts = ts

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, RsMsgsMsgInfoSummary):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
