# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class RSTrafficClue(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'ts': 'int',
        'size': 'int',
        'priority': 'int',
        'service_id': 'int',
        'service_sub_id': 'int',
        'peer_id': 'str',
        'count': 'int'
    }

    attribute_map = {
        'ts': 'TS',
        'size': 'size',
        'priority': 'priority',
        'service_id': 'service_id',
        'service_sub_id': 'service_sub_id',
        'peer_id': 'peer_id',
        'count': 'count'
    }

    def __init__(self, ts=None, size=None, priority=None, service_id=None, service_sub_id=None, peer_id=None, count=None):  # noqa: E501
        """RSTrafficClue - a model defined in OpenAPI"""  # noqa: E501

        self._ts = None
        self._size = None
        self._priority = None
        self._service_id = None
        self._service_sub_id = None
        self._peer_id = None
        self._count = None
        self.discriminator = None

        if ts is not None:
            self.ts = ts
        if size is not None:
            self.size = size
        if priority is not None:
            self.priority = priority
        if service_id is not None:
            self.service_id = service_id
        if service_sub_id is not None:
            self.service_sub_id = service_sub_id
        if peer_id is not None:
            self.peer_id = peer_id
        if count is not None:
            self.count = count

    @property
    def ts(self):
        """Gets the ts of this RSTrafficClue.  # noqa: E501


        :return: The ts of this RSTrafficClue.  # noqa: E501
        :rtype: int
        """
        return self._ts

    @ts.setter
    def ts(self, ts):
        """Sets the ts of this RSTrafficClue.


        :param ts: The ts of this RSTrafficClue.  # noqa: E501
        :type: int
        """

        self._ts = ts

    @property
    def size(self):
        """Gets the size of this RSTrafficClue.  # noqa: E501


        :return: The size of this RSTrafficClue.  # noqa: E501
        :rtype: int
        """
        return self._size

    @size.setter
    def size(self, size):
        """Sets the size of this RSTrafficClue.


        :param size: The size of this RSTrafficClue.  # noqa: E501
        :type: int
        """

        self._size = size

    @property
    def priority(self):
        """Gets the priority of this RSTrafficClue.  # noqa: E501


        :return: The priority of this RSTrafficClue.  # noqa: E501
        :rtype: int
        """
        return self._priority

    @priority.setter
    def priority(self, priority):
        """Sets the priority of this RSTrafficClue.


        :param priority: The priority of this RSTrafficClue.  # noqa: E501
        :type: int
        """

        self._priority = priority

    @property
    def service_id(self):
        """Gets the service_id of this RSTrafficClue.  # noqa: E501


        :return: The service_id of this RSTrafficClue.  # noqa: E501
        :rtype: int
        """
        return self._service_id

    @service_id.setter
    def service_id(self, service_id):
        """Sets the service_id of this RSTrafficClue.


        :param service_id: The service_id of this RSTrafficClue.  # noqa: E501
        :type: int
        """

        self._service_id = service_id

    @property
    def service_sub_id(self):
        """Gets the service_sub_id of this RSTrafficClue.  # noqa: E501


        :return: The service_sub_id of this RSTrafficClue.  # noqa: E501
        :rtype: int
        """
        return self._service_sub_id

    @service_sub_id.setter
    def service_sub_id(self, service_sub_id):
        """Sets the service_sub_id of this RSTrafficClue.


        :param service_sub_id: The service_sub_id of this RSTrafficClue.  # noqa: E501
        :type: int
        """

        self._service_sub_id = service_sub_id

    @property
    def peer_id(self):
        """Gets the peer_id of this RSTrafficClue.  # noqa: E501


        :return: The peer_id of this RSTrafficClue.  # noqa: E501
        :rtype: str
        """
        return self._peer_id

    @peer_id.setter
    def peer_id(self, peer_id):
        """Sets the peer_id of this RSTrafficClue.


        :param peer_id: The peer_id of this RSTrafficClue.  # noqa: E501
        :type: str
        """

        self._peer_id = peer_id

    @property
    def count(self):
        """Gets the count of this RSTrafficClue.  # noqa: E501


        :return: The count of this RSTrafficClue.  # noqa: E501
        :rtype: int
        """
        return self._count

    @count.setter
    def count(self, count):
        """Sets the count of this RSTrafficClue.


        :param count: The count of this RSTrafficClue.  # noqa: E501
        :type: int
        """

        self._count = count

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, RSTrafficClue):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
