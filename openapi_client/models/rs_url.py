# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class RsUrl(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'scheme_separator': 'str',
        'ipv6_wrap_open': 'str',
        'ipv6_separator': 'str',
        'ipv6_wrap_close': 'str',
        'port_separator': 'str',
        'path_separator': 'str',
        'query_separator': 'str',
        'query_assign': 'str',
        'query_field_sep': 'str',
        'fragment_separator': 'str'
    }

    attribute_map = {
        'scheme_separator': 'schemeSeparator',
        'ipv6_wrap_open': 'ipv6WrapOpen',
        'ipv6_separator': 'ipv6Separator',
        'ipv6_wrap_close': 'ipv6WrapClose',
        'port_separator': 'portSeparator',
        'path_separator': 'pathSeparator',
        'query_separator': 'querySeparator',
        'query_assign': 'queryAssign',
        'query_field_sep': 'queryFieldSep',
        'fragment_separator': 'fragmentSeparator'
    }

    def __init__(self, scheme_separator=None, ipv6_wrap_open=None, ipv6_separator=None, ipv6_wrap_close=None, port_separator=None, path_separator=None, query_separator=None, query_assign=None, query_field_sep=None, fragment_separator=None):  # noqa: E501
        """RsUrl - a model defined in OpenAPI"""  # noqa: E501

        self._scheme_separator = None
        self._ipv6_wrap_open = None
        self._ipv6_separator = None
        self._ipv6_wrap_close = None
        self._port_separator = None
        self._path_separator = None
        self._query_separator = None
        self._query_assign = None
        self._query_field_sep = None
        self._fragment_separator = None
        self.discriminator = None

        if scheme_separator is not None:
            self.scheme_separator = scheme_separator
        if ipv6_wrap_open is not None:
            self.ipv6_wrap_open = ipv6_wrap_open
        if ipv6_separator is not None:
            self.ipv6_separator = ipv6_separator
        if ipv6_wrap_close is not None:
            self.ipv6_wrap_close = ipv6_wrap_close
        if port_separator is not None:
            self.port_separator = port_separator
        if path_separator is not None:
            self.path_separator = path_separator
        if query_separator is not None:
            self.query_separator = query_separator
        if query_assign is not None:
            self.query_assign = query_assign
        if query_field_sep is not None:
            self.query_field_sep = query_field_sep
        if fragment_separator is not None:
            self.fragment_separator = fragment_separator

    @property
    def scheme_separator(self):
        """Gets the scheme_separator of this RsUrl.  # noqa: E501


        :return: The scheme_separator of this RsUrl.  # noqa: E501
        :rtype: str
        """
        return self._scheme_separator

    @scheme_separator.setter
    def scheme_separator(self, scheme_separator):
        """Sets the scheme_separator of this RsUrl.


        :param scheme_separator: The scheme_separator of this RsUrl.  # noqa: E501
        :type: str
        """

        self._scheme_separator = scheme_separator

    @property
    def ipv6_wrap_open(self):
        """Gets the ipv6_wrap_open of this RsUrl.  # noqa: E501


        :return: The ipv6_wrap_open of this RsUrl.  # noqa: E501
        :rtype: str
        """
        return self._ipv6_wrap_open

    @ipv6_wrap_open.setter
    def ipv6_wrap_open(self, ipv6_wrap_open):
        """Sets the ipv6_wrap_open of this RsUrl.


        :param ipv6_wrap_open: The ipv6_wrap_open of this RsUrl.  # noqa: E501
        :type: str
        """

        self._ipv6_wrap_open = ipv6_wrap_open

    @property
    def ipv6_separator(self):
        """Gets the ipv6_separator of this RsUrl.  # noqa: E501


        :return: The ipv6_separator of this RsUrl.  # noqa: E501
        :rtype: str
        """
        return self._ipv6_separator

    @ipv6_separator.setter
    def ipv6_separator(self, ipv6_separator):
        """Sets the ipv6_separator of this RsUrl.


        :param ipv6_separator: The ipv6_separator of this RsUrl.  # noqa: E501
        :type: str
        """

        self._ipv6_separator = ipv6_separator

    @property
    def ipv6_wrap_close(self):
        """Gets the ipv6_wrap_close of this RsUrl.  # noqa: E501


        :return: The ipv6_wrap_close of this RsUrl.  # noqa: E501
        :rtype: str
        """
        return self._ipv6_wrap_close

    @ipv6_wrap_close.setter
    def ipv6_wrap_close(self, ipv6_wrap_close):
        """Sets the ipv6_wrap_close of this RsUrl.


        :param ipv6_wrap_close: The ipv6_wrap_close of this RsUrl.  # noqa: E501
        :type: str
        """

        self._ipv6_wrap_close = ipv6_wrap_close

    @property
    def port_separator(self):
        """Gets the port_separator of this RsUrl.  # noqa: E501


        :return: The port_separator of this RsUrl.  # noqa: E501
        :rtype: str
        """
        return self._port_separator

    @port_separator.setter
    def port_separator(self, port_separator):
        """Sets the port_separator of this RsUrl.


        :param port_separator: The port_separator of this RsUrl.  # noqa: E501
        :type: str
        """

        self._port_separator = port_separator

    @property
    def path_separator(self):
        """Gets the path_separator of this RsUrl.  # noqa: E501


        :return: The path_separator of this RsUrl.  # noqa: E501
        :rtype: str
        """
        return self._path_separator

    @path_separator.setter
    def path_separator(self, path_separator):
        """Sets the path_separator of this RsUrl.


        :param path_separator: The path_separator of this RsUrl.  # noqa: E501
        :type: str
        """

        self._path_separator = path_separator

    @property
    def query_separator(self):
        """Gets the query_separator of this RsUrl.  # noqa: E501


        :return: The query_separator of this RsUrl.  # noqa: E501
        :rtype: str
        """
        return self._query_separator

    @query_separator.setter
    def query_separator(self, query_separator):
        """Sets the query_separator of this RsUrl.


        :param query_separator: The query_separator of this RsUrl.  # noqa: E501
        :type: str
        """

        self._query_separator = query_separator

    @property
    def query_assign(self):
        """Gets the query_assign of this RsUrl.  # noqa: E501


        :return: The query_assign of this RsUrl.  # noqa: E501
        :rtype: str
        """
        return self._query_assign

    @query_assign.setter
    def query_assign(self, query_assign):
        """Sets the query_assign of this RsUrl.


        :param query_assign: The query_assign of this RsUrl.  # noqa: E501
        :type: str
        """

        self._query_assign = query_assign

    @property
    def query_field_sep(self):
        """Gets the query_field_sep of this RsUrl.  # noqa: E501


        :return: The query_field_sep of this RsUrl.  # noqa: E501
        :rtype: str
        """
        return self._query_field_sep

    @query_field_sep.setter
    def query_field_sep(self, query_field_sep):
        """Sets the query_field_sep of this RsUrl.


        :param query_field_sep: The query_field_sep of this RsUrl.  # noqa: E501
        :type: str
        """

        self._query_field_sep = query_field_sep

    @property
    def fragment_separator(self):
        """Gets the fragment_separator of this RsUrl.  # noqa: E501


        :return: The fragment_separator of this RsUrl.  # noqa: E501
        :rtype: str
        """
        return self._fragment_separator

    @fragment_separator.setter
    def fragment_separator(self, fragment_separator):
        """Sets the fragment_separator of this RsUrl.


        :param fragment_separator: The fragment_separator of this RsUrl.  # noqa: E501
        :type: str
        """

        self._fragment_separator = fragment_separator

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, RsUrl):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
