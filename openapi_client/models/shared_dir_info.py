# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class SharedDirInfo(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'filename': 'str',
        'virtualname': 'str',
        'shareflags': 'int',
        'parent_groups': 'list[str]'
    }

    attribute_map = {
        'filename': 'filename',
        'virtualname': 'virtualname',
        'shareflags': 'shareflags',
        'parent_groups': 'parent_groups'
    }

    def __init__(self, filename=None, virtualname=None, shareflags=None, parent_groups=None):  # noqa: E501
        """SharedDirInfo - a model defined in OpenAPI"""  # noqa: E501

        self._filename = None
        self._virtualname = None
        self._shareflags = None
        self._parent_groups = None
        self.discriminator = None

        if filename is not None:
            self.filename = filename
        if virtualname is not None:
            self.virtualname = virtualname
        if shareflags is not None:
            self.shareflags = shareflags
        if parent_groups is not None:
            self.parent_groups = parent_groups

    @property
    def filename(self):
        """Gets the filename of this SharedDirInfo.  # noqa: E501


        :return: The filename of this SharedDirInfo.  # noqa: E501
        :rtype: str
        """
        return self._filename

    @filename.setter
    def filename(self, filename):
        """Sets the filename of this SharedDirInfo.


        :param filename: The filename of this SharedDirInfo.  # noqa: E501
        :type: str
        """

        self._filename = filename

    @property
    def virtualname(self):
        """Gets the virtualname of this SharedDirInfo.  # noqa: E501


        :return: The virtualname of this SharedDirInfo.  # noqa: E501
        :rtype: str
        """
        return self._virtualname

    @virtualname.setter
    def virtualname(self, virtualname):
        """Sets the virtualname of this SharedDirInfo.


        :param virtualname: The virtualname of this SharedDirInfo.  # noqa: E501
        :type: str
        """

        self._virtualname = virtualname

    @property
    def shareflags(self):
        """Gets the shareflags of this SharedDirInfo.  # noqa: E501


        :return: The shareflags of this SharedDirInfo.  # noqa: E501
        :rtype: int
        """
        return self._shareflags

    @shareflags.setter
    def shareflags(self, shareflags):
        """Sets the shareflags of this SharedDirInfo.


        :param shareflags: The shareflags of this SharedDirInfo.  # noqa: E501
        :type: int
        """

        self._shareflags = shareflags

    @property
    def parent_groups(self):
        """Gets the parent_groups of this SharedDirInfo.  # noqa: E501


        :return: The parent_groups of this SharedDirInfo.  # noqa: E501
        :rtype: list[str]
        """
        return self._parent_groups

    @parent_groups.setter
    def parent_groups(self, parent_groups):
        """Sets the parent_groups of this SharedDirInfo.


        :param parent_groups: The parent_groups of this SharedDirInfo.  # noqa: E501
        :type: list[str]
        """

        self._parent_groups = parent_groups

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, SharedDirInfo):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
