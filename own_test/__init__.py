import  openapi_client

configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: BasicAuth
configuration.username = 'test'
configuration.password = 'test'

# Create an instance of the API class
API_INSTANCE = openapi_client.DefaultApi(openapi_client.ApiClient(configuration))