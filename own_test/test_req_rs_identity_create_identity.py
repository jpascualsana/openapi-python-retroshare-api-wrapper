# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.models.req_rs_identity_create_identity import ReqRsIdentityCreateIdentity  # noqa: E501
from openapi_client.rest import ApiException
from own_test import API_INSTANCE

class TestReqRsIdentityCreateIdentity(unittest.TestCase):
    """ReqRsIdentityCreateIdentity unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testReqRsIdentityCreateIdentity(self):
        """Test ReqRsIdentityCreateIdentity"""

        api_instance = API_INSTANCE


        req_rs_identity_create_identity = {"name": "AnonymousIdTest1",
                                           # "avatar": "( RsGxsImage)Image associated to the identity",
                                           "pseudonimous": True,
                                           # "pgpPassword": "( string)password to unlock PGP to sign identity, not implemented yet"
                                           }  # ReqRsIdentityCreateIdentity | name: ( string)Name of the identity          avatar: ( RsGxsImage)Image associated to the identity          pseudonimous: ( boolean)true for unsigned identity, false otherwise          pgpPassword: ( string)password to unlock PGP to sign identity, not implemented yet   (optional)

        try:
            # Create a new identity
            api_response = api_instance.rs_identity_create_identity(
                req_rs_identity_create_identity=req_rs_identity_create_identity)
            print(api_response)
        except ApiException as e:
            print("Exception when calling DefaultApi->rs_identity_create_identity: %s\n" % e)
        pass


if __name__ == '__main__':
    unittest.main()
