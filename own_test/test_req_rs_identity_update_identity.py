# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.models.req_rs_identity_update_identity import ReqRsIdentityUpdateIdentity  # noqa: E501
from openapi_client.rest import ApiException

# Used to update the identity
from openapi_client.models.rs_gxs_id_group import RsGxsIdGroup
from openapi_client.models.rs_group_meta_data import RsGroupMetaData

# Used to get identity info
from openapi_client.models.req_rs_identity_get_identities_info import ReqRsIdentityGetIdentitiesInfo

from own_test import API_INSTANCE

class TestReqRsIdentityUpdateIdentity(unittest.TestCase):
    """ReqRsIdentityUpdateIdentity unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testReqRsIdentityUpdateIdentity(self):
        """Test ReqRsIdentityUpdateIdentity"""
        api_instance = API_INSTANCE

        # Get all identities info for debugging
        # try:
            # Get identities summaries list.
            # api_response = api_instance.rs_identity_get_own_signed_ids()
            # Could be also
            # api_response = api_instance.rs_identity_get_identities_summaries()
            # print(api_response)
        # except ApiException as e:
        #     print("Exception when calling DefaultApi->rs_identity_get_identities_summaries: %s\n" % e)

        # Get the RsGxsIdGroup data for the wanted identity
        reqId = 'a96d9b1a009bb96ea6fdd0be053e44c4'

        req_rs_identity_get_identities_info = {"ids": [reqId]}

        try:
            # Get identities information (name, avatar...). Blocking API.
            api_response = api_instance.rs_identity_get_identities_info(
                req_rs_identity_get_identities_info=req_rs_identity_get_identities_info)
            # print(api_response)
        except ApiException as e:
            print("Exception when calling DefaultApi->rs_identity_get_identities_info: %s\n" % e)

        # Now lets get the RsGxsIdGroup object from the array of returned ids
        for id in api_response.ids_info:
            if id.m_meta.m_group_id == reqId:
                reqId = id

        # Now modify the desired components
        print(reqId)
        reqId.m_meta.m_group_name = 'AnonymousIdTest2'

        # This won't work because the API return the info on camelCase and the object attrs are stored in snake_case
        # Todo: modify the openapi generator python template to be camelCase only (to unify with RS project)
        # rsGxsIdGroup = RsGxsIdGroup(reqId)
        # rsGroupMetaData = RsGroupMetaData(rsGxsIdGroup.m_meta)

        # Send the object
        req_rs_identity_update_identity = {
            "identityData": reqId}  # ReqRsIdentityUpdateIdentity | identityData: ( RsGxsIdGroup)updated identiy data   (optional)

        try:
            # Update identity data (name, avatar...)
            api_response = api_instance.rs_identity_update_identity(
                req_rs_identity_update_identity=req_rs_identity_update_identity)
            print(api_response)
        except ApiException as e:
            print("Exception when calling DefaultApi->rs_identity_update_identity: %s\n" % e)

        pass


if __name__ == '__main__':
    unittest.main()
