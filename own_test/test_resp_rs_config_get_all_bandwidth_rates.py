# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.models.resp_rs_config_get_all_bandwidth_rates import RespRsConfigGetAllBandwidthRates  # noqa: E501
from openapi_client.rest import ApiException
from own_test import API_INSTANCE

class TestRespRsConfigGetAllBandwidthRates(unittest.TestCase):
    """RespRsConfigGetAllBandwidthRates unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testRespRsConfigGetAllBandwidthRates(self):
        """Test RespRsConfigGetAllBandwidthRates"""
        # Create an instance of the API class
        api_instance = API_INSTANCE

        try:
            # getAllBandwidthRates get the bandwidth rates for all peers
            api_response = api_instance.rs_config_get_all_bandwidth_rates()
            print(api_response)
        except ApiException as e:
            print("Exception when calling DefaultApi->rs_config_get_all_bandwidth_rates: %s\n" % e)

        pass


if __name__ == '__main__':
    unittest.main()
