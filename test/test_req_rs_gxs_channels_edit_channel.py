# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.models.req_rs_gxs_channels_edit_channel import ReqRsGxsChannelsEditChannel  # noqa: E501
from openapi_client.rest import ApiException


class TestReqRsGxsChannelsEditChannel(unittest.TestCase):
    """ReqRsGxsChannelsEditChannel unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testReqRsGxsChannelsEditChannel(self):
        """Test ReqRsGxsChannelsEditChannel"""
        # FIXME: construct object with mandatory attributes with example values
        # model = openapi_client.models.req_rs_gxs_channels_edit_channel.ReqRsGxsChannelsEditChannel()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
