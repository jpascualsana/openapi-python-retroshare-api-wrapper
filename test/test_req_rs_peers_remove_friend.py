# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.models.req_rs_peers_remove_friend import ReqRsPeersRemoveFriend  # noqa: E501
from openapi_client.rest import ApiException


class TestReqRsPeersRemoveFriend(unittest.TestCase):
    """ReqRsPeersRemoveFriend unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testReqRsPeersRemoveFriend(self):
        """Test ReqRsPeersRemoveFriend"""
        # FIXME: construct object with mandatory attributes with example values
        # model = openapi_client.models.req_rs_peers_remove_friend.ReqRsPeersRemoveFriend()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
