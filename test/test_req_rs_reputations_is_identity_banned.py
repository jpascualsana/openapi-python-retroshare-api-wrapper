# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.models.req_rs_reputations_is_identity_banned import ReqRsReputationsIsIdentityBanned  # noqa: E501
from openapi_client.rest import ApiException


class TestReqRsReputationsIsIdentityBanned(unittest.TestCase):
    """ReqRsReputationsIsIdentityBanned unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testReqRsReputationsIsIdentityBanned(self):
        """Test ReqRsReputationsIsIdentityBanned"""
        # FIXME: construct object with mandatory attributes with example values
        # model = openapi_client.models.req_rs_reputations_is_identity_banned.ReqRsReputationsIsIdentityBanned()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
