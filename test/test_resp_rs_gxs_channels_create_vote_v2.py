# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.models.resp_rs_gxs_channels_create_vote_v2 import RespRsGxsChannelsCreateVoteV2  # noqa: E501
from openapi_client.rest import ApiException


class TestRespRsGxsChannelsCreateVoteV2(unittest.TestCase):
    """RespRsGxsChannelsCreateVoteV2 unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testRespRsGxsChannelsCreateVoteV2(self):
        """Test RespRsGxsChannelsCreateVoteV2"""
        # FIXME: construct object with mandatory attributes with example values
        # model = openapi_client.models.resp_rs_gxs_channels_create_vote_v2.RespRsGxsChannelsCreateVoteV2()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
