# coding: utf-8

"""
    RetroShare OpenApi wrapper

    RetroShare OpenApi wrapper generated using Doxygen documentation  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import openapi_client
from openapi_client.models.resp_rs_identity_auto_add_friend_ids_as_contact import RespRsIdentityAutoAddFriendIdsAsContact  # noqa: E501
from openapi_client.rest import ApiException


class TestRespRsIdentityAutoAddFriendIdsAsContact(unittest.TestCase):
    """RespRsIdentityAutoAddFriendIdsAsContact unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testRespRsIdentityAutoAddFriendIdsAsContact(self):
        """Test RespRsIdentityAutoAddFriendIdsAsContact"""
        # FIXME: construct object with mandatory attributes with example values
        # model = openapi_client.models.resp_rs_identity_auto_add_friend_ids_as_contact.RespRsIdentityAutoAddFriendIdsAsContact()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
